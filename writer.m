%================================================================================
% Pretty printer for the parse result (for a page). This is for debugging. See module "pretty" for
% details.
%================================================================================

:- module writer.

%================================================================================
% Interface
%================================================================================

:- interface.
:- import_module string.
:- import_module parser.


% Pretty-print a page to a string
:- pred page(page(raw)::in, string::out) is det.


%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module list, int, bool.
:- import_module pretty, moin2tiki, pages.


% Pretty-print the parse result of a page to a string

page(Page,      % Parse result
    Str) :-     % Pretty-printed parse result
    pretty.write(
        page(Page),
        null_m2t,
        [],
        pretty.c_text,
        yes,
        Str,
        _).     % No messages for the pretty printing of the parse result


% Pretty-print a page to text
:- pred page(page(raw)::in, ppstate::in, ppstate::out) is det.

page(Paragraphs, !S) :-
    str("[ ", !S),
    indent(2, !S),
    list(paragraph,
         (pred(A::in, C::out) is det :-
              str0(", ", A, B),
              nl(B, C)),
         Paragraphs,
         !S),
    unindent(2, !S),
    nl(!S),
    str("]", !S).


% Write a data constructor, with indented arguments
:- pred constructor(string,                     % Data constructor name
                    (pred(ppstate, ppstate)),   % Writer for the arguments
                    ppstate, ppstate).

:- mode constructor(in,
                    (pred(in, out) is det),
                    in, out)
   is det.

constructor(Name, Writer, !S) :-
    Len = length(Name),
    str(Name ++ "(", !S),
    indent(Len+1, !S),
    Writer(!S),
    str(")", !S),
    unindent(Len+1, !S).




% Pretty-print a paragraph to text
:- pred paragraph(paragraph(raw)::in, ppstate::in, ppstate::out) is det.

paragraph(Heading@heading(_,_), !S) :-
    str(string.string(Heading), !S).

paragraph(code_block(code_block(Ind, Lines, MaybeHighlighting)), !S) :-
    constructor("code_block",
                (pred(A::in, Z::out) is det :-
                     str(int_to_string(Ind) ++ ", ", A, B),
                     str(string.string(MaybeHighlighting) ++ ", ", B, C),
                     nl(C, D),
                     writer.lines(Lines, D, Z)),
                !S).

paragraph(text(Ind, Inlines), !S) :-
    constructor("text",
                text_args(Ind, Inlines),
                !S).


% Writer for the arguments of a text() constructor
:- pred text_args(int::in, list(inline(raw))::in, ppstate::in, ppstate::out) is det.
text_args(Ind, Inlines, !S) :-
    indentation(Ind, !S),
    hori_list(inline,
              Inlines,
              !S).


paragraph(comment_block(Lines), !S) :-
    constructor("comment_block",
                writer.lines(Lines),
                !S).


paragraph(empty_line, !S) :-
    str("empty_line", !S).


paragraph(list_item(Ind, List_type, Inlines), !S) :-
    constructor("list_item",
                list_item_args(Ind, List_type, Inlines),
                !S).

% Writer for the arguments of a list_item() constructor
:- pred list_item_args(int::in, list_type::in, list(inline(raw))::in, ppstate::in, ppstate::out) is det.
list_item_args(Ind, List_type, Inlines, !S) :-
    indentation(Ind, !S),
    str(string.string(List_type) ++ ",", !S),
    nl(!S),
    hori_list(inline,
              Inlines,
              !S).


paragraph(def_list_item(Ind, Word, Inlines), !S) :-
    constructor("def_list_item",
                def_list_item_args(Ind, Word, Inlines),
                !S).

% Writer for the arguments of a def_list_item() constructor
:- pred def_list_item_args(int::in, string::in, list(inline(raw))::in,
                           ppstate::in, ppstate::out) is det.
def_list_item_args(Ind, Word, Inlines, !S) :-
    indentation(Ind, !S),
    str(string.string(Word) ++ ",", !S),
    nl(!S),
    hori_list(inline,
              Inlines,
              !S).


paragraph(Horizontal_rule@horizontal_rule(_,_), !S) :-
    str(string.string(Horizontal_rule), !S).


paragraph(table(_Ind, LLL), !S) :-
    constructor("table",
                (pred(X::in, Y::out) is det :-
                    vert_list((pred(LL::in, A::in, Z::out) is det :-
                                   hori_list((pred(L::in, P::in, Q::out) is det :-
                                                  hori_list(inline, L, P, Q)
                                             ),
                                             LL,
                                             A, Z)
                              ),
                              LLL,
                              X, Y
                             )
                ),
                !S).


:- pred indentation(int::in, ppstate::in, ppstate::out) is det.

indentation(Ind, !S) :-
    str(int_to_string(Ind) ++ ",", !S),
    nl(!S).




% Writer for an inline.
:- pred inline(inline(raw)::in, ppstate::in, ppstate::out) is det.

inline(Inline, !S) :-
    str(string.string(Inline), !S).



%--------------------------------------------------------------------------------
% Write a list of strings in the form
% [ "str1",
%   "str2",
%   ...
% ]

:- pred lines(list(string)::in,
              ppstate::in, ppstate::out) is det.


lines(Lines, !S) :-
    str("[ ", !S),
    indent(2, !S),
    list((pred(Str::in, A::in, B::out) is det :-
              str(string.string(Str), A, B)),

         (pred(A::in, C::out) is det :-
              str(",", A, B),
              nl(B, C)),

         Lines,

         !S),
    unindent(2, !S),
    str(" ]", !S).



%--------------------------------------------------------------------------------
% Generic list writer predicates

% Write a list of arbitrary type. The values are arranged horizontally.
:- pred hori_list( % Writer for the values
                   (pred(T, ppstate, ppstate)),

                   % List of values to be written
                   list(T),

                   % Pretty-printer state
                   ppstate, ppstate).

:- mode hori_list((pred(in, in, out) is det),
                  in,
                  in, out)
   is det.



% Write a list of arbitrary type. Every value gets its own line.
:- pred vert_list( % Writer for the values
                   (pred(T, ppstate, ppstate)),

                   % List of values to be written
                   list(T),

                   % Pretty-printer state
                   ppstate, ppstate).

:- mode vert_list((pred(in, in, out) is det),
                  in,
                  in, out)
   is det.




% Write a horizontal list. The values are interspersed with ", ".
hori_list(Writer, Values, !S) :-
    str("[ ", !S),
    indent(2, !S),
    list(Writer,
         str(", "),
         Values,
         !S),
    unindent(2, !S),
    str(" ]", !S).


% Write a vertical list. The values are interspersed with "," and each list item is on a separate
% line.
vert_list(Writer, Values, !S) :-
    str("[ ", !S),
    indent(2, !S),
    list(Writer,
         (pred(A::in, C::out) is det :-
              str(", ", A, B),
              nl(B, C)
         ),
         Values,
         !S),
    unindent(2, !S),
    str(" ]", !S).


