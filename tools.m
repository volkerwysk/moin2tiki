%================================================================================
% General purpose tools
%================================================================================

:- module tools.

:- interface.
:- import_module list, io, char, string, bool, pair, map.
:- import_module posix.


% The identity function
:- func id(T::in(Inst)) = (T::out(Inst)) is det.


% An alternative of two values
:- type either(A,B)
   ---> left(A)
   ;    right(B).

:- inst either(I,J) ---> left(I) ; right(J).
:- inst left(I)     ---> left(I).
:- inst right(I)    ---> right(I).


% Apply an IO predicate to all elements of a list, and collect the results.

:- pred map_io((pred(X, Y, io.state, io.state)),        % Predicate
               list(X),                                 % Input arguments
               list(Y),                                 % Output arguments
               io.state, io.state).

:- mode map_io((pred(in, out, di, uo) is det),
               in,
               out,
               di, uo)
   is det.



% Fold an IO predicate over a list of elements. That's like foldl, but with an additional IO
% argument.

:- pred foldl_io(pred(L, A, A, io, io),         % IO predicate to fold
                 list(L),                       % List of input arguments of the predicate
                 A,                             % Start value
                 A,                             % Result value
                 io, io).                       % IO state

:- mode foldl_io(pred(in, in, out, di, uo) is det,
                 in,
                 in,
                 out,
                 di, uo)
   is det.




% Apply an IO predicate to all elements of a list. The predicate doesn't have any output.

:- pred map_io((pred(X, io.state, io.state)),
               list(X),
               io.state, io.state).

:- mode map_io((pred(in, di, uo) is det),
               in,
               di, uo)
   is det.


% Apply an stateful predicate to all elements of a list. The predicate doesn't have any output. It
% only operates on the state.

:- pred map_state((pred(X, State, State)),
                  list(X),
                  State, State).

:- mode map_state((pred(in, in, out) is det),
                  in,
                  in, out)
   is det.

:- mode map_state((pred(in, di, uo) is det),
                  in,
                  di, uo)
   is det.


% Change error reporting behaviour of a POSIX library predicate.
%
% The predicates of the POSIX library return a result of type posix.result or posix.result(Res).
% This either is ok or ok(Res) for successful completion, or posix.error(Err), where Err is of type
% posix.error.
%
% The following function recognizes the error. It makes an error message from the error and throws
% it as a string. Then, in the success case, the result no longer needs to be packaged, so it is
% returned directly.

:- pred posixerr2ex( % File name for error message. Give the empty string, when no file name should
                     % be included in the error message.
                     string::in,
                     % Predicate whose error reporting scheme should be changed
                     (pred(posix.result(T), io.state, io.state))::in(pred(out, di, uo) is det),
                     % Result of the predicate
                     T::out,
                     io::di, io::uo)
is det.

:- pred posixerr2ex( % File name for error message. Give the empty string, when no file name should
                     % be included in the error message.
                     string::in,
                     % Predicate whose error reporting scheme should be changed
                     (pred(posix.result, io, io))::in(pred(out, di, uo) is det),
                     io::di, io::uo)
is det.


% Take up to a number of elements from a list. Also returns the rest.

:- pred take(int::in, list(T)::in,
             list(T)::out,              % first elements
             list(T)::out)              % rest of the list
   is det.


% Insert the specified element between each two list elements

:- func intersperse(T, list(T)) = list(T).


% Read an entire text file. Return the result and errors in an io.res.
:- pred contents(string::in,              % File name
                 io.res(string)::out,     % Result (the contents or an error)
                 io::di, io::uo)
   is det.


% Read an entire text file. Throw an error as a string with the error message.
:- pred contents_ex(string::in,            % File name
                    string::out,           % File contents
                    io::di, io::uo)
   is det.


% Write an entire text file
:- pred write_contents(string::in,            % File name
                       string::in,            % File contents
                       io::di, io::uo)
   is det.


% Append content to a text file
:- pred append_contents(string::in,            % File name
                        string::in,            % File contents
                        io::di, io::uo)
   is det.


% Build a string which consists of the concatenation of a number of identical strings
:- func repeat(string,          % character to repeat
               int)             % number of repetitions
   = string.



% Quote special characters inside a string for the shell
%
% This quotes special characters inside a string, such that it is
% recognized as one string by the shell when enclosed in double quotes.
% Doesn't add the double quotes.
%
% Note: The quoted strings are correctly recognized in shell scripts. But the shell bash has an
% annoying history expansion "feature", which causes it to choke on exclamation marks, when in
% interactive mode, even when quoted with double quotes. You can turn it off with "set +o
% histexpand".
:- func quote0(string) = string.

% Char list version of quote0
:- func quote0_chars(list(char)) = list(char).


% Quote a string for the shell
%
% This encloses a string in double quotes and quotes any special
% characters inside, such that it will be recognized as one string by a
% shell. The double quotes are added even when they aren't needed for this
% purpose.
%
% Note: The quoted strings are correctly recognized in shell scripts. But the shell bash has an
% annoying history expansion \"feature\", which causes it to choke on exclamation marks, when in
% interactive mode, even when quoted with double quotes. You can turn it off with "set +o
% histexpand".
:- func quote(string) = string.

% Char list version of quote
:- func quote_chars(list(char)) = list(char).



% Set a string in quotes and quote all quotation marks inside as '\"'. The quote is the only
% character which is quoted inside.

:- func quote_qm(string) = string.



% Set a string in quotes and quote all quotation marks inside as '""'. The quote is the only
% character which is quoted inside.

:- func quote_qm2(string) = string.



% Quote shell metacharacters.
%
% This function quotes strings, such that they are not misinterpreted by the shell. It tries to be
% friendly to a human reader - when special characters are present, then the string is quoted with
% double quotes. If not, it is left unchanged.
%
% The list of exacly which characters need to be quoted has been taken from the bash source code.
% Bash in turn, implements POSIX 1003.2. So the result produced should be correct. From the bash
% info pages: "... the rules for evaluation and quoting are taken from the POSIX 1003.2
% specification for the `standard' Unix shell."
%
% Note: The quoted strings are correctly recognized in shell scripts. But the shell bash has an
% annoying history expansion "feature", which causes it to choke on exclamation marks, when in
% interactive mode, even when quoted with double quotes. You can turn it off with "set +o
% histexpand".
:- func shell_quote(string) = string.

% Char list version of shell_quote.
:- func shell_quote_chars(list(char)) = list(char).



:- pred mkdir(string::in, io::di, io::uo) is det.
:- pred chdir(string::in, io::di, io::uo) is det.


% Like io.call_system, but throw an exception on failure.
:- pred system(string::in, io::di, io::uo) is det.


% Like posix.realpath.realpath, but throw an exeption on failure.
:- pred realpath(string::in, string::out, io::di, io::uo) is det.


% A version of the head function that is deterministic on nonempty lists.
:- func head1(list(T)) = T.
:- mode head1(in) = out is semidet.
:- mode head1(in(non_empty_list)) = out is det.


% A version of the tail function that is deterministic on nonempty lists.
:- func tail1(list(T)) = list(T).
:- mode tail1(in) = out is semidet.
:- mode tail1(in(non_empty_list)) = out is det.




% Replace all occurences of one character list (From) inside a character list (Where) with another
% character list (To). The replacement (To) isn't considered again. The replacement in Where
% continues after the occurence of From.

:- pred replace(
       list(char)::in,      % From
       list(char)::in,      % To
       list(char)::in,      % Where
       list(char)::out)     % Result
   is det.



% Like in Haskell, for instance:
% zip(['a', 'b', 'c'], [1, 2, 3]) = [{'a', 1}, {'b', 2}, {'c', 3}]

:- func zip(list(T), list(U)) = list(pair(T,U)).


% Split a path into path components. Path components are what is between two slashes (or at the
% start or beginning). Filter out empty path components. Return if the path starts with a slash
% (that means empty path component at the beginning).
%
% For instance:
% slice_path("foo/bar")   = ( ["foo", "bar"] - no )
% slice_path("foo///bar") = ( ["foo", "bar"] - no )
% slice_path("/foo/bar")  = ( ["foo", "bar"] - yes )

:- pred slice_path(string::in,          % Path to slice
                   list(string)::out,   % Path components (with empty ones omitted)
                   bool::out)           % If there is a slash at the beginning
   is det.


% slice_path as a function.

:- func slice_path_f(string) = { list(string), bool }.


% Same as slice_path/3, but in addition count the number of ".." path components at the beginning,
% and remove them from the path components list.

:- pred slice_path(string::in,          % Path to slice
                   list(string)::out,   % Path components (with empty ones and leading ".."
                                        % components omitted)
                   bool::out,           % If the path is relative. This means, if it starts with a
                                        % slash or a ".." component.
                   int::out)            % Number of ".." path components at the beginning of the
                                        % path to slice
   is det.


% Remove the last element of a list.

:- func skip_last(list(T)) = list(T).


% Like list.filter, but use an IO action which is to decide if a list element should be included in
% the result list.
:- pred filter_io(

    % IO action to call, in order to determine if the element is to be included. The arguments are
    % the list element, the filter result (yes or no) and IO.
    pred(T, bool, io, io)::in(pred(in, out, di, uo) is det),

    % Values which are being filtered.
    list(T)::in,

    % Matchin elements (filter returned "yes")
    list(T)::out,

    % Nonmatchin elements (filter returned "no")
    list(T)::out,

    % IO
    io::di, io::uo)
   is det.


% Sort a list and delete dupes.

:- pred sort_uniq(
    % Comparison predicate
    pred(T, T, comparison_result) :: pred(in, in, out) is det,
    
    % Input list
    list(T)::in,
    
    % Sorted, unique output list
    list(T)::out
) is det.


% Remove all spaces from the end of a character list

:- func trim(list(char)) = list(char).


% Apply a transformation predicate on the value which corresponds to a given key. Add a default
% value, if the key isn't found.
:- pred transform_add_value(
    pred(V, V)::in(pred(in, out) is det),       % Transformation predicate
    V::in,                                      % Default value
    K::in,                                      % Key to search
    map(K, V)::in,                              % Map in 
    map(K, V)::out)                             % Map out
is det.




%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module exception, int.
:- import_module posix.strerror, posix.chdir, posix.mkdir, posix.realpath.
:- import_module pages.


filter_io(_, [], [], [], !IO).

filter_io(P, [X | Xs], Ys, Zs, !IO) :-
    filter_io(P, Xs, Ys0, Zs0, !IO),
    P(X, Res, !IO),
    (
        if   Res = yes
        then Ys = [X | Ys0],
             Zs = Zs0
        else Ys = Ys0,
             Zs = [X | Zs0]
    ).


id(X) = X.


intersperse(_, []) = [].

intersperse(_, [X]) = [X].

intersperse(A, [X,Y|R]) = [X,A|R1] :-
    R1 = intersperse(A, [Y|R]).



map_io(_, [], [], !IO).

map_io(Pred, [A|As], [B|Bs], !IO) :-
    Pred(A, B, !IO),
    map_io(Pred, As, Bs, !IO).



map_io(_, [], !IO).

map_io(Pred, [A|As], !IO) :-
    Pred(A, !IO),
    map_io(Pred, As, !IO).



map_state(_, [], !State).

map_state(Pred, [A|As], !State) :-
    Pred(A, !State),
    map_state(Pred, As, !State).




posixerr2ex(Filename, Pred, Res, !IO) :-
    Pred(Result, !IO),
    (
        Result = posix.ok(Res)
    ;
        Result = posix.error(Err),
        strerror(Err, Msg, !IO),
        throw(Filename ++ ": " ++ Msg)
    ).


posixerr2ex(Filename, Pred, !IO) :-
    Pred(Result, !IO),
    (
        Result = posix.ok
    ;
        Result = posix.error(Err),
        strerror(Err, Msg, !IO),
        throw(Filename ++ ": " ++ Msg)
    ).



take(_, [], [], []).

take(N, [X|Xs], Fst, Rest) :-
    ( if   N = 0
      then Fst = [],
           Rest = [X|Xs]
      else Fst = [X|Fst1],
           take(N - 1, Xs, Fst1, Rest)
    ).




contents(Filename, Res, !IO) :-
    open_input(Filename, Res1, !IO),
    (
        Res1 = ok(Stream),

        read_file_as_string(Stream, Res2 : io.maybe_partial_res(string), !IO),
        (
            Res2 = ok(Contents),
            Res = ok(Contents)
        ;
            Res2 = error(_, Err2),
            Res = error(Err2)
        ),
        close_input(Stream, !IO)
    ;
        Res1 = error(Error : io.error),
        Res = io.error(Error)
    ).



contents_ex(Filename, Contents, !IO) :-
    contents(Filename, Res, !IO),
    ( Res = ok(Contents)
    ; Res = error(Error : io.error),
      throw(Filename ++ ": " ++ error_message(Error))
    ).



write_contents(Filename, Contents, !IO) :-
    open_output(Filename, Res1, !IO),
    ( Res1 = ok(Stream),
      write_strings(Stream, [Contents], !IO),
      close_output(Stream, !IO)
    ; Res1 = error(Error : io.error),
      throw(Filename ++ ": " ++ error_message(Error))
    ).


append_contents(Filename, Contents, !IO) :-
    open_append(Filename, Res1, !IO),
    ( Res1 = ok(Stream),
      write_strings(Stream, [Contents], !IO),
      close_output(Stream, !IO)
    ; Res1 = error(Error : io.error),
      throw(Filename ++ ": " ++ error_message(Error))
    ).



repeat(Str0, Num) = Str1 :-
    repeat(Str0, Num, List),
    string.append_list(List, Str1).


:- pred repeat(string::in, int::in, list(string)::out) is det.

repeat(Str, Num, List) :-
    ( if   Num = 0
      then List = []
      else List = [Str|Rest],
           repeat(Str, Num-1, Rest)
    ).


quote0(Str) = from_char_list(quote0_chars(to_char_list(Str))).

quote0_chars([]) = [].

quote0_chars([Ch|Chs]) = Chs1 :-
   ( if   member(Ch, ['"', '$', '`', '\\'])
     then Chs1 = ['\\', Ch|quote0_chars(Chs)]
     else Chs1 = [Ch|quote0_chars(Chs)]
   ).



quote(Str) = from_char_list(quote_chars(to_char_list(Str))).

quote_chars(Chars) = ['"'] ++ quote0_chars(Chars) ++ ['"'].




shell_quote(Str) = from_char_list(shell_quote_chars(to_char_list(Str))).

shell_quote_chars([]) = ['"', '"'].

shell_quote_chars(Txt@[_|_]) = Txt1 :-
    ( if find_first_match((pred(Ch::in) is semidet :-
                               member(Ch, ['\'', ' ', '\t', '\n', '"',
                                           '\\', '|', '&', ';', '(', ')', '<', '>',
                                           '!', '{', '}', '*', '[', '?', ']', '^',
                                           '$', '`', '#'])),   % "
                          Txt,
                          _)
      then Txt1 = quote_chars(Txt)
      else Txt1 = Txt
    ).



quote_qm(Str) =
    """" ++ string.from_char_list(quote_qm_cl(string.to_char_list(Str))) ++ """".

:- func quote_qm_cl(list(char)) = list(char).

quote_qm_cl([]) = [].
quote_qm_cl([Ch|Chs]) =
    ( if Ch = '"'
      then ['\\', '"'|quote_qm_cl(Chs)]
      else [Ch|quote_qm_cl(Chs)]
    ).


quote_qm2(Str) =
    """" ++ string.from_char_list(quote_qm2_cl(string.to_char_list(Str))) ++ """".

:- func quote_qm2_cl(list(char)) = list(char).

quote_qm2_cl([]) = [].
quote_qm2_cl([Ch|Chs]) =
    ( if Ch = '"'
      then ['"', '"'|quote_qm2_cl(Chs)]
      else [Ch|quote_qm2_cl(Chs)]
    ).



mkdir(Path, !IO) :-
    posix.mkdir.mkdir(Path, posix.mode(0b111111111), Result, !IO),
    handle_posix_error(Path, Result, !IO).


chdir(Path, !IO) :-
    posix.chdir.chdir(Path, Result, !IO),
    handle_posix_error(Path, Result, !IO).



:- pred handle_posix_error(string::in, posix.result::in, io::di, io::uo) is det.

handle_posix_error(Path, Result, !IO) :-
    ( Result = ok
    ; Result = error(Err),
      strerror(Err, Msg, !IO),
      throw(shell_quote(Path) ++ ": " ++ Msg)
    ).



system(Cmd, !IO) :-
    io.call_system(Cmd, Res, !IO),
    ( Res = ok(ExitStatus),
      ( if   ExitStatus = 0
        then true
        else throw("Command failed.\nCommand: " ++ Cmd ++ "\nExit Code: " ++
                   string.string(ExitStatus))
      )

    ; Res = io.error(ErrorCode),
      throw("Command failed.\nCommand: " ++ Cmd ++ "\nMessage: " ++
            io.error_message(ErrorCode))
    ).




head1([A|_]) = A.

tail1([_|A]) = A.





replace(From, To, Where, Res) :-
    (
        Where = [],
        Res = []
    ;
        Where = [W|Where1],
        ( if append(From, Rest, Where)

          % Where begins with From.
          then replace(From, To, Rest, Res1),
               Res = append(To, Res1)

          % Where doesn't begin with From.
          else Res = [W|Res1],
               replace(From, To, Where1, Res1)
        )
    ).




foldl_io(Pred, Values, Akku, Result, !IO) :-
    (
        Values = [],
        Result = Akku
    ;
        Values = [Val|ValRest],
        Pred(Val, Akku, Akku1, !IO),
        foldl_io(Pred, ValRest, Akku1, Result, !IO)
    ).



zip(L1, L2) = L3 :-
    (
        L1 = [],
        L3 = []
    ;
        L1 = [A|As],
        (
            L2 = [],
            L3 = []
        ;
            L2 = [B|Bs],
            L3 = [A - B | zip(As,Bs)]
        )
    ).



slice_path(Path, Compl, Startslash) :-
    slice_path_chr(to_char_list(Path), Compl0, Startslash),
    map((pred(Chars::in, Str::out) is det :-
             Str = from_char_list(Chars)),
        Compl0,
        Compl).



% Same as slice_path, but work with character lists instead of strings.

:- pred slice_path_chr(list(char)::in, list(list(char))::out, bool::out).

slice_path_chr(Chars, Compl, Startslash) :-
    ( if   Chars = ['/'|_]
      then Startslash = yes
      else Startslash = no
    ),
    take_comps(Chars, Compl0),
    filter((pred(Comp::in) is semidet :-
                Comp \= []),
           Compl0,
           Compl).


% Split off the first path component.

:- pred take_comp(list(char)::in,       % Path
                  list(char)::out,      % Path component at the beginning
                  list(char)::out)      % Rest of the path, without the '/' after the path component
                                        % at the beginning
   is det.

take_comp(Chars, Comp, Rest) :-
    take_while((pred(Char::in) is semidet :- Char \= ('/')),
               Chars,
               Comp,
               Rest0),
    ( Rest0 = [],
      Rest  = []
    ; Rest0 = [_|Rest1],
      Rest  = Rest1
    ).



% Split into path components. Empty ones not filtered out yet.

:- pred take_comps(list(char)::in,
                   list(list(char))::out)
   is det.

take_comps(Chars, [Comp|Compl]) :-
    take_comp(Chars, Comp, Rest),
    ( Rest  = [],
      Compl = []
    ; Rest  = [_|_],
      take_comps(Rest, Compl)
    ).


slice_path_f(Str) = { Compl, Startslash } :-
    slice_path(Str, Compl, Startslash).



slice_path(Path, Compl, Relative, DotDotNum) :-
    slice_path(Path, Compl0, Startslash),
    take_while((pred(Comp::in) is semidet :-
                    Comp = ".."),
               Compl0,
               Start,
               End),
    Compl = End,
    DotDotNum = length(Start),
    Relative = or(Startslash,
                  pred_to_bool((pred) is semidet :- DotDotNum > 0)).




skip_last([]) = [].

skip_last([_]) = [].

skip_last([A,_]) = [A].

skip_last([A,B,C|Rest]) =
    [A|skip_last([B,C|Rest])].



realpath(Path, Realpath, !IO) :-
    posix.realpath.realpath(Path, Res, !IO),
    (
        Res = ok(Realpath)
    ;   Res = posix.error(Error),
        strerror(Error, Msg, !IO),
        throw(quote(Path) ++ ": " ++ Msg)
    ).


sort_uniq(Comp, L1, L3) :-
    list.sort(Comp, L1, L2),
    list.remove_dups(L2, L3).




trim(In) = 
    list.reverse(
        drop_while((pred(' '::in) is semidet),
            list.reverse(In))).
    



transform_add_value(Transform, Default, Key, Mapin, Mapout) :-
    (
        if   map.search(Mapin, Key, Val1)
        then Transform(Val1, Val2), 
             map.set(Key, Val2, Mapin, Mapout)
        else map.set(Key, Default, Mapin, Mapout)
    ).
