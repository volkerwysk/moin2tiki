This document lists limitations of the last version of Moin2Tiki. If they don't allow you to use Moin2Tiki, I might
be willing to help you and improve it. Please contact me, at &lt;post at volker-wysk dot de>.


## Only One User

Moin2Tiki so far supports only one user. You can specifiy only one for each call of the `moin2tiki` program.
Theoretically, you can call it with different MoinMoin users, one for each call. But you would have to sort out the
pages of each MoinMoin user, what probably is impractical.

Moin2Tiki may support multiple users in a later version.


## Machinery

* You need command line access to the machines which run your MoinMoin and your Tiki server. Moin2Tiki reads the
  pages from MoinMoin's "pages" directory. You can use it in place, or copy this directory to another machine and
  run Moin2Tiki there. Moin2Tiki calls a script (`moin-import.php`) on the Tiki server - directly, not over HTTP.
  You need permissions to place this script, and a stylesheet (`moin.css`) in the Tiki root directory.

* Moin2Tiki is developed and tested on Linux. The provided [AppImage](https://appimage.org/) should run on all
  Linux distributions. There's no Windows or MacOS version.


## Non-supported MoinMoin features

* [MoinMoin Variables](http://moinmo.in/HelpOnVariables) aren't supported.

* Moin2Tiki has support for transforming MoinMoin's
  [Syntax Highlighting](https://moinmo.in/HelpOnFormatting#Colorize_code_.28Syntax_Highlighting.29),
  but for no other parsers.

* At present, the transforming of embeddings from MoinMoin to Tiki is possible for images only. There
  are other types of embeddings, which could be translated too. Such as diagrams, MP3 audio and flash
  videos, vector drawings. See [PluginFlash](https://doc.tiki.org/PluginFlash),
  [PluginYouTube](https://doc.tiki.org/PluginYouTube) and
  [PluginDiagram](https://doc.tiki.org/PluginDiagram). For things other that images, that are embedded in MoinMoin
  pages, a link is generated instead.

* Interwiki-Links aren't supported.

* MoinMoin page forwardings aren't supported, since I don't use them. I'm willing to add them, if there is any
  interest.

* (Mostly) only documented MoinMoin features are supported.

* CamelCase is supported only on the MoinMoin side. All encountered CamelCase links are transformed to regular
  links on the Tiki side.

* No support for MoinMoin themes. See [HelpOnThemes](https://moinmo.in/HelpOnThemes).

* Only basic support for MoinMoin tables. The table attributes, listed in
  (HelpOnTables)[https://moinmo.in/HelpOnTables] aren't supported.

* Only normal, simple code blocks that start with `{{{` on a separate line and end with `}}}` on a
  separate line. No support for nesting of parser sections. See [Parser sections and
  nesting](https://moinmo.in/HelpOnParsers#Parser_sections_and_nesting).

* No support for Creole.

* Only some of the MoinMoin macros are recognized and transformed to Tiki. MoinMoin has so many macros, it's
  impossible to support them all. In the version of Moin2Tiki, to which this page belongs, only the following
  macros are supported: &lt;&lt;BR>>, &lt;&lt;Anchor>>, &lt;&lt;TableOfContents>>, &lt;&lt;ListPages>>.
  Non-supported macro invocations will be included in the Tiki page verbatim, like they are called in the
  correspondig Moin2Moin page. Adding new MoinMoin macros to Moin2Tiki is straightforward. If you have some macros
  that you think should be recognized, then contact me.


## Non-perfect Transformations

* The output of definition lists isn't the same in Tiki as in MoinMoin, with respect to empty lines.
  It's the way Tiki handles them, I can't do anything about it.

* The ListPages macro has only partial support, because there is no exact equivalent in Tiki. In Tiki, you can't
  specify the pages to list with a regular expression, but only with a substring search.


## Broken Links for Some Anchors

When you have a link from a selected page to a non-selected page *and* the link has an anchor part *and* the anchor
name includes non-ASCII letters *and* it's an explicit anchor (not to one generated from a heading) then you will
get a broken link. The link to the page will be okay, but the anchor part will dangle. See [here](Anchors in
Tiki.html) for more details.


## Other Shortcomings

* Moin2Tiki doesn't know of Tiki's [Dynamic Variables](https://doc.tiki.org/Dynamic-Variable). There is the
  possibility that an occurence of one is created by accident. The Dynamic Variables is a questionable feature. You
  should probably turn it off. (Search for "Dynamic variables" in the control panels.)
