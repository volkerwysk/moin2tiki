%================================================================================
% The actions which can be specified on the command line.
%================================================================================

:- module actions.
:- interface.
:- import_module io, moin2tiki, string, list.
:- import_module pages.

% Call the action which has been specified on the command line.

:- pred call_action(
    % Name of the action
    string::in,

    % The collected data
    m2t::in,

    % IO
    io::di, io::uo)
is det.


% Get the full path to the file which stores the current revision of a MoinMoin page. Throw an
% exception if the page is unknown.

:- func moin_file_current(
    m2t,            % Collected data
    moinpage) =     % MoinMoin page name
    string          % Full path
is det.


% The names of the actions

:- func actionnames = list(string).


%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module exception, assoc_list, map, pair, require, int, maybe.
:- import_module posix, posix.chdir.
:- import_module tools, parsertools, writer, serialiser, create, attachments, pagenames,
                 unicode, parser, postprocess, pretty/*, trackers*/.


% All the actions, which can be called from main.

:- func actions = assoc_list(string, action_pred).

actions = [
    "parse"         - action_pred(do_parse),
    "convert"       - action_pred(do_convert),
    "create"        - action_pred(do_create),
    "listmoin"      - action_pred(do_listmoin),
    "listtiki"      - action_pred(do_listtiki),
    "attachments"   - action_pred(do_attachments),
    "migrate"       - action_pred(do_migrate),
    "ignore"        - action_pred(do_ignore)
/*    "records"       - action_pred(do_records),*/
/*    "fields"        - action_pred(do_fields),*/
/*    "regalplätze"   - action_pred(do_regalplaetze),*/
/*    "csv"           - action_pred(do_csv)*/
].


actionnames = map(fst, actions).


% We need to encapsulate the action's predicate inside an algebraic data type, because the
% assoc_list.search predicate doesn't return a higher order inst. This is a workaround. See the
% message "Issue with assoc_list and other container types" at 2022-01-19/2022-01-20 in the
% mercury-users mailing list for details.

:- type action_pred --->
    action_pred(
        pred(
            m2t::in,            % The collected data
            io::di, io::uo)
        is det
    ).



% Run the specified action, from the "actions" assoc_list (see above).

call_action(Action, M2T, !IO) :-
    (
        if   assoc_list.search(actions, Action, action_pred(Pred))
        then Pred(M2T, !IO)
        else throw("Bug: unknown action " ++ quote_qm(Action))
    ).



% For actions, which require that exactly one page is selected on the command line. This returns the
% full path to the file which the selected MoinMoin page is stored in. In case it isn't the case,
% throw an error message.

:- pred one_file(
    string::in,                 % Action name (only for error message)
    m2t::in,                    % All data. See main.m.
    moinpage::out,              % MoinMoin name of the selected page
    string::out)                % Full path to the selected Moin page
is det.

one_file(Action, M2T, MoinPage, File) :-
    MoinPages = M2T ^ m2t_selectedmoinpages,
    (
        MoinPages = [],
        throw("No pages have been selected.")
    ;
        MoinPages = [MoinPage]
    ;
        MoinPages = [_,_|_],
        throw("Error: Several pages selected. For the """ ++ Action ++ """" ++
              " action, exactly one page must be selected.")
    ),

    File = moin_file_current(M2T, MoinPage).


moin_file_current(M2T, MoinPage) = File :-
    ( if   map.search(M2T ^ m2t_moinpartsmap, MoinPage, MoinParts)
      then Current = MoinParts ^ mp_current
      else unexpected($pred, quote_qm(moinpage(MoinPage)) ++ " not found in MoinPartsMap.")
    ),

    File = (M2T ^ m2t_params ^ p_pagesdir) ++ "/" ++ moin(M2T ^ m2t_pages_dirs, MoinPage)
           ++ "/revisions/" ++ Current.



%--------------------------------------------------------------------------------
% Implementation of the actions which can be called from the command line.
%--------------------------------------------------------------------------------

:- pred do_parse(       m2t::in, io::di, io::uo) is det.
:- pred do_convert(     m2t::in, io::di, io::uo) is det.
:- pred do_create(      m2t::in, io::di, io::uo) is det.
:- pred do_listtiki(    m2t::in, io::di, io::uo) is det.
:- pred do_listmoin(    m2t::in, io::di, io::uo) is det.
:- pred do_attachments( m2t::in, io::di, io::uo) is det.
:- pred do_migrate(     m2t::in, io::di, io::uo) is det.
/*:- pred do_records(     m2t::in, io::di, io::uo) is det.*/
/*:- pred do_fields(      m2t::in, io::di, io::uo) is det.*/
/*:- pred do_regalplaetze(m2t::in, io::di, io::uo) is det.*/
/*:- pred do_csv(         m2t::in, io::di, io::uo) is det.*/
:- pred do_ignore(      m2t::in, io::di, io::uo) is det.


% Parse a MoinMoin page and pretty-print the parse result. Exactly one MoinMoin page must be
% selected. This prints the link targets in the RAW state.

do_parse(M2T, !IO) :-
    one_file("parse", M2T, _MoinPage, File),
    contents_ex(File, Txt, !IO),
    parse_uniq(page, Txt, Page0),
    postprocess(M2T, Page0, Page),

    writer.page(Page, Str),
    io.write_string(Str, !IO),
    io.nl(!IO).


% Parse one MoinMoin page and convert it to Tiki syntax. Output the result.

do_convert(M2T, !IO) :-
    one_file("convert", M2T, MoinPage, File),
    contents_ex(File, Txt, !IO),

    % Convert the Moin page to Tiki link target state.
    parsertools.parse_full(M2T, MoinPage, Txt, Page),

    TikiPage = (M2T ^ m2t_resolve)(MoinPage),
    serialiser.page1(Page, M2T, [TikiPage - Page], Str, Messages),

    pretty.print_messages(Messages, !IO),
    io.write_string("---\n", !IO),
    io.write_string(Str, !IO),
    io.write_string("\n---\n", !IO).


% Create the intermediate directory structure, which can be used to import the selected pages with
% the moin-import.php script.

do_create(M2T, !IO) :-
    create.create_intermediate_dir(M2T, !IO).



% List the selected MoinMoin page names, converted to Tiki page names.

do_listtiki(M2T, !IO) :-
    TikiPages = map(func(MoinPage) = (M2T ^ m2t_resolve)(MoinPage),
                    M2T ^ m2t_selectedmoinpages),
    TikiPages1 = sort(glib_utf8_collate,
                      map(func(TP) = tikipage(TP), TikiPages)),

    io.write_list(
        TikiPages1,
        "\n",
        io.write_string,
        !IO),
    io.nl(!IO).


% List the selected MoinMoin page names.

do_listmoin(M2T, !IO) :-
    io.write_list(
        list.sort(moinpage_collate, M2T ^ m2t_selectedmoinpages),
        "\n",
        (pred(moinpage(Str)::in, IOin::di, IOout::uo) is det :- io.write_string(Str, IOin, IOout)),
        !IO),
    io.nl(!IO).



% List the attachments of all selected Moin pages.

do_attachments(M2T, !IO) :-
    % Parse all the MoinMoin pages
    pages.parse_pages_full(M2T, ParsedPages, !IO),

    % Get data about the attachments in the parsed pages.
    analyse_attachments(ParsedPages, AttLL, _AttLL_straight, AttLL_dangling,
                        _ReferencedL, _DanglingL, _SelectedTikiPages,
                        ExtraTikiPages),

    % Print the pages and attached attachments, referenced in some attachment references
    foldl(do_attachments_1, AttLL, !IO),

    % Print the dangling attachment references, by referenced page, if any.
    create.report_dangling_attachments(AttLL_dangling, !IO),

    % Print the list of extra pages. Those are included in the two lists which have just been
    % printed (see above).
    io.write_string("Extra pages:\n", !IO),
    ( ExtraTikiPages = [],
      io.write_string("None\n", !IO)
    ; ExtraTikiPages = [_|_],
      io.write_list(list.map((func(TikiPage) = tikipage(TikiPage)), ExtraTikiPages),
                    "\n", io.write, !IO),
      io.nl(!IO)
    ).


% Print the attachments of one Moin page, which has been referenced in some attachment references.

:- pred do_attachments_1(
    % For one page, the list of the straight attachments.
    pair(tikipage, list(attachment(tikipage)))::in,

    io::di, io::uo)
is det.

do_attachments_1(TikiPage - AttL, !IO) :-

    AttL_straight = filter((pred(Att::in) is semidet :- Att = straight(_,_)), AttL),
    % AttL_dangling = filter((pred(Att::in) is semidet :- Att = dangling(_,_)), AttL),

    io.write_string("Attachments referenced in the " ++
                    quote_qm(tikipage(TikiPage)) ++
                    " Tiki page:\n", !IO),
    (
        AttL_straight = [],
        io.write_string("No attachments referenced.", !IO)
    ;
        AttL_straight = [_|_],
        AttLStr = map(
            (func(Att) = tikipage(AttTikiPage) - filename(AttFilename) :-
                ( Att = straight(AttTikiPage, AttFilename)
                ; Att = dangling(_,_),
                  unexpected($pred, "Dangling attachment reference")
                )),
            AttL_straight),
        io.write_list(AttLStr, "\n", io.write, !IO)
    ),
    io.nl(!IO), io.nl(!IO).




% Do the actual migration. This creates the intermediate directory structure and calls the
% "moin-import.php" script which imports the pages and attachments to the Tiki server. This script
% will write the list of successfully migrated Tiki pages to the success list, which is specified as
% the fourth parameter of the moin-import script. This list is then read by moin2tiki and converted
% backwards to the list of successfully migrated MoinMoin pages. The MoinMoin pages ist is then
% appended to the file specified by --ignore-list-write, if this parameter has been set.

do_migrate(M2T, !IO) :-
    Tmpdir  = M2T ^ m2t_params ^ p_tmpdir,
    Tikidir = M2T ^ m2t_params ^ p_wwwdir ++ "/" ++ M2T ^ m2t_params ^ p_tikidir,

    create.create_intermediate_dir(M2T, !IO),
    tools.system("chmod -R a+rX " ++ quote(Tmpdir), !IO),

    SuccessListPath = Tmpdir ++ "/successlist",
    IgnoreListWriteFn = M2T ^ m2t_params ^ p_ignore_list_write,

    posixerr2ex(Tikidir, posix.chdir.chdir(Tikidir), !IO),

    io.format("\nCalling the moin-import.php script. SuccessListPath=%s\n", [s(SuccessListPath)], !IO),
    io.flush_output(stdout_stream, !IO),
    
    Cmd =
        "php ./moin-import.php " ++
        quote(M2T ^ m2t_params ^ p_tmpdir)
        ++ " " ++ quote(M2T ^ m2t_params ^ p_user)
        ++ " " ++ quote(M2T ^ m2t_params ^ p_language)
        ++ " " ++ quote(SuccessListPath),
    tools.system(Cmd, !IO),
    io.write_string("\nmoin-import.php script finished.\n\n", !IO),

    % Read the success list
    ( if IgnoreListWriteFn \= "" then io.write_string("Reading success list...\n", !IO) else true ),
    tools.contents(SuccessListPath, Res, !IO),
    (
        Res = ok(SuccessList1),

        SuccessList2 = string.chomp(SuccessList1),
        TikiPagesL = string.split_into_lines(SuccessList2),
        BackwResolve =
            (func(In) = moinpage((M2T ^ m2t_backwards_resolve)(tikipage(In)))),
        MoinPagesL = list.map(BackwResolve, TikiPagesL),
        MoinPages = string.join_list("\n", MoinPagesL),

        io.format("%i pages have been successfully migrated.\n", 
                  [i(length(MoinPagesL))], !IO),
        (
            if   IgnoreListWriteFn \= ""
            then io.write_string("Adding the successfuly migrated pages to the ignore list.\n", !IO),
                 tools.append_contents(IgnoreListWriteFn, MoinPages, !IO)
            else io.write_string("Not adding the successfuly migrated pages to the ignore list,\n" 
                                 ++ "since none has been specified with --ignore-list-write.\n", !IO)
        )
    ;
        Res = error(Err),
        Msg = io.error_message(Err),
        throw(string.format("Couldn't read the list of the successfully migrated pages, which is\n" ++
                            "generated by the moin-import.php script. Is the path writeable by the\n" ++ 
                            "script?\n" ++
                            "The error message: %s\nThe path: %s",
                            [s(Msg), s(quote(SuccessListPath))])
              : string
             )
    ),

    % Remove the intermediate directroy
    create.remove_intermediate_dir(M2T, !IO).




% This only adds the selected pages to the ignore list, which has been specified with
% --ignore-list-write.

do_ignore(M2T, !IO) :-
    IgnoreOut = M2T ^ m2t_ignore_out,
    ( IgnoreOut = yes(Stream)
    ; IgnoreOut = no,
      unexpected($pred, "No ignore list output specified. This should have been intercepted in main.")
    ),

    io.write_list(
        list.sort(moinpage_collate, M2T ^ m2t_selectedmoinpages),
        "\n",
        (pred(moinpage(Str)::in, IOin::di, IOout::uo) is det
            :- io.format("Adding page to ignore list: %s", [s(Str)], IOin, IOout)
        ),
        !IO),

    io.write_list(
        Stream,
        list.sort(moinpage_collate, M2T ^ m2t_selectedmoinpages),
        "\n",
        (pred(moinpage(Str)::in, IOin::di, IOout::uo) is det :- io.write_string(Str, IOin, IOout)),
        !IO),
    io.nl(Stream, !IO),
    io.nl(!IO).



/*
% Read the selected pages as records and output the field name - field value pairs found. See trackers.m.

do_records(M2T, !IO) :-
    % Parse all the MoinMoin pages
    pages.parse_pages_full(M2T, ParsedPages, !IO),

    % Convert them all to records
    list.map_foldl(conv_page_msg(M2T), ParsedPages, PagesRecords, !IO),

    % Output the pages as records
    io.write_list(
        PagesRecords,
        "\n",
        (pred((TikiPage - Record)::in, IOin::di, IOout::uo) is det :-
            some [!IO1] (
                !:IO1 = IOin,
                io.format("Page >%s\n", [s(tikipage(TikiPage))], !IO1),
                io.write_list(
                    Record,
                    "",
                    (pred((RecTitle - RecVal)::in, In::di, Out::uo) is det :-
                        io.format("Field>%s -> %s\n", [s(RecTitle), s(RecVal)], In, Out)),
                    !IO1),
                IOout = !.IO1)),
        !IO).
*/


/*
do_fields(M2T, !IO) :-
    % Parse all the MoinMoin pages
    pages.parse_pages_full(M2T, ParsedPages, !IO),

    % Convert them all to records
    list.map_foldl(conv_page_msg(M2T), ParsedPages, Records, !IO),

    % Count all the fields
    trackers.count_fields(map(snd, Records), Numbers),

    io.format("\nNumber of Pages: %i\n\n" ++
              "Field names with number of occurrences and nonempty occurrences:\n",
              [i(length(M2T ^ m2t_selectedmoinpages))], !IO),
    io.write_list(Numbers, "\n", io.write, !IO),
    io.nl(!IO).
*/


% Convert one page to a record and print any messages.

/*
:- pred conv_page_msg(
    moin2tiki.m2t::in,                                  % Static data
    pair(tikipage, page(target(tikipage)))::in,         % Page name with parsed page
    pair(tikipage, assoc_list(string,string))::out,     % Page name with record (field list)
    io::di, io::uo)
is det.

conv_page_msg(M2T, TikiPage - Page, TikiPage - Record, !IO) :-
    trackers.page_to_record(M2T, Page, Record, Messages),
    (   Messages = []
    ;   Messages = [_|_],
        MoinPage = (M2T ^ m2t_backwards_resolve)(TikiPage),
        io.write_string("In the " ++ quote(moinpage(MoinPage)) ++ " page:\n", !IO),
        io.write_list(Messages, "\n", io.write_string, !IO),
        io.nl(!IO), io.nl(!IO)
    ).
*/


/*
do_regalplaetze(M2T, !IO) :-
    % Parse all the MoinMoin pages
    pages.parse_pages_full(M2T, ParsedPages, !IO),

    % Convert them all to records
    list.map_foldl(conv_page_msg(M2T), ParsedPages, Records, !IO),

    regalplaetze(Records, !IO).
*/


% Create a CSV file for import in Tiki

/*
do_csv(M2T, !IO) :-
    % Parse all the MoinMoin pages.
    pages.parse_pages_full(M2T, ParsedPages, !IO),

    % Convert them all to records.
    list.map_foldl(conv_page_msg(M2T), ParsedPages, Records, !IO),

    % Read the tracker schema.
    SchemaFilename = M2T ^ m2t_params ^ p_schema,
    trackers.schema(SchemaFilename, SchemaTxt, Schema, !IO),
    io.write_string("Schema:\n", !IO),
    io.write_list(Schema, "\n", io.write, !IO),
    io.nl(!IO),

    % Create the CSV file, write the schema (the first line) to it, and then write the records as
    % lines in the CSV file.
    CsvFilename = M2T ^ m2t_params ^ p_csv,
    open_output(CsvFilename, Res, !IO),
    (
        Res = ok(Stream),
        io.write_string(Stream, SchemaTxt, !IO),

        list.foldl(write_schema_line(Stream, Schema), Records, !IO),

        close_output(Stream, !IO)
    ;
        Res = error(Error : io.error),
        throw(CsvFilename ++ ": " ++ error_message(Error))
    ).
*/


/*
% Convert a record to one line in the CSV file.

:- pred write_schema_line(
    io.text_output_stream::in,                          % Stream to write to
    list(string)::in,                                   % Schema of the tracker (list of field names)
    pair(tikipage, assoc_list(string,string))::in,      % The record to be written
    io::di, io::uo)
is det.

write_schema_line(Stream, Schema, _TikiPage - Fields, !IO) :-

    % Get the quoted field values of the record, in the order of the fields in the schema.
    list.map(pred(Key::in, Val::out) is det :-
        ( if      Key = "Item ID"
          then    Val = "0"

          else if assoc_list.search(Fields, Key, Val0)
          then    Val = quote_qm2(Val0)

          else    Val = "" ),
        Schema,
        QuotedValues),

    % Write the field values to the CSV file, separated by commas.
    io.write_string(
        Stream,
        string.join_list(",", QuotedValues) ++ "\n",
        !IO).
*/
