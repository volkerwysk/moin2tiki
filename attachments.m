%================================================================================
% Dealing with attachments
%================================================================================

:- module attachments.

:- interface.
:- import_module list, pair, assoc_list.
:- import_module pages, parser.


% Type for attachment link targets only. These two data constructors correspond to
% parser.t_attachment and parser.t_dangling_attachment.

:- type attachment(P) --->
       straight( straight_pagename :: P,
                 straight_filename :: filename
               )
    ;  dangling( dangling_pagename :: P,
                 dangling_filename :: filename
               ).


% Assuming that the attachment in the first argument is straight, or dangling respectively, return
% the pagename and filename.

:- func straight_pf(attachment(P)) = pair(P,filename)  <=  pagename(P).
:- func dangling_pf(attachment(P)) = pair(P,filename)  <=  pagename(P).


% Comparison predicate for two attachments

:- pred compare_attachments(
    attachment(P)::in,
    attachment(P)::in,
    comparison_result::out)
is det <= pagename(P).


% Get a list of all the attachments which are refered to inside the specified page. The result is of
% type attachment (straight or dangling) for every link to an attachment, or embedded attachment.
% This works for link targets of type "target(P)", where P is the pagename (moinpage or tikipage).
% It doesn't work for raw link targets.
%
% The result is sorted and double entries are removed.

:- func attachments_in_page(
    page(target(P))
) = list(attachment(P))
<= pagename(P).


% Analyse the attachment references in all the specified parsed pages. Some pages selected on the
% command line reference an attachment, which is not attached to a selected page. Those non-selected
% pages are called "extra pages". See the "Attachments" design document.
%
% This works for "target(moinpage)" and "target(tikipage)" link targets.

:- pred analyse_attachments(
    % ParsedPages: List of the parsed pages, pairs of the Moin or Tiki page name and the parse
    % result. This must be the pages selected on the command line.
    assoc_list(P, page(target(P)))::in,

    % AttLL: For each selected page, the list of the attachment references in that page. The pages
    % are in the order of the ParsedPages argument. The attachment references are sorted and made
    % unique. The attachment references can be empty.
    assoc_list(P, list(attachment(P)))::out,

    % AttLL_straight: For each selected page, the list of the straight attachment references in that
    % page. The pages are in the order of the ParsedPages argument. The attachment references are
    % sorted and made unique. The attachment references can be empty.
    assoc_list(P, list(attachment(P)))::out,

    % AttLL_dangling: For each selected page, the list of the dangling attachment references in that
    % page. The pages are in the order of the ParsedPages argument. The attachment references are
    % sorted and made unique. The attachment references can be empty.
    assoc_list(P, list(attachment(P)))::out,

    % ReferencedL: List of the pages referenced by attachment links, together with the list of the
    % referenced attachments of the pages. This are only for straight, non-dangling pages. The list
    % contains only attachments of data constructor "straight".
    assoc_list(P, list(attachment(P)))::out,

    % DanglingL: List of the dangling attachment references. This is the list of pages referenced by
    % dangling attachment links, together with the dangling attachment references. The list contains
    % only attachments of data constructor "dangling".
    assoc_list(P, list(attachment(P)))::out,

    % List of all the page names which are included in the first argument of analyse_attachments.
    % This are all the pages selected on the command line.
    list(P)::out,

    % List of the extra page names. See above.
    list(P)::out
) is det  <= pagename(P).


%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module maybe, bool, pagenames, tools, unicode, string, require, io.



compare_attachments(T1, T2, Comp) :-
    ( T1 = straight(_,_),
      T2 = dangling(_,_),
      Comp = ('<')

    ; T1 = dangling(_,_),
      T2 = straight(_,_),
      Comp = ('>')

    ; T1 = straight(Pagename1, Filename1),
      T2 = straight(Pagename2, Filename2),
      compare_str_pair(
            to_str(Pagename1) - filename(Filename1),
            to_str(Pagename2) - filename(Filename2),
            Comp)

    ; T1 = dangling(Pagename1, Filename1),
      T2 = dangling(Pagename2, Filename2),
      compare_str_pair(
            to_str(Pagename1) - filename(Filename1),
            to_str(Pagename2) - filename(Filename2),
            Comp)
    ).


% Comparison predicate for two pairs of strings.

:- pred compare_str_pair(
    pair(string,string)::in, pair(string,string)::in, comparison_result::out)
is det.

compare_str_pair(A - X, B - Y, Comp) :-
    glib_utf8_collate(A, B, Comp1),
    (
        Comp1 = ('<'),
        Comp  = ('<')
    ;
        Comp1 = ('>'),
        Comp  = ('>')
    ;
        Comp1 = ('='),
        glib_utf8_collate(X, Y, Comp)
    ).



straight_pf(Att) = (Page - Filename) :-
    ( Att = straight(Page, Filename)
    ; Att = dangling(_,_),
      unexpected($pred, "Unexpected dangling attachment reference")
    ).

dangling_pf(Att) = (Page - Filename) :-
    ( Att = dangling(Page, Filename)
    ; Att = straight(_,_),
      unexpected($pred, "Unexpected straight attachment reference")
    ).




attachments_in_page(Parseresult) = L :-
    L0 = list.condense(map(attachments_in_paragraph, Parseresult)),
    tools.sort_uniq(compare_attachments, L0, L).



% Get the attachments inside a paragraph

:- func attachments_in_paragraph(paragraph(target(P))) = list(attachment(P))   <= pagename(P).

attachments_in_paragraph(heading(_,_)) = [].

attachments_in_paragraph(code_block(_)) = [].

attachments_in_paragraph(text(_, Inlines)) =
    list.condense(map(attachments_in_inline, Inlines)).

attachments_in_paragraph(comment_block(_)) = [].

attachments_in_paragraph(empty_line) = [].

attachments_in_paragraph(list_item(_, _, Inlines)) =
    list.condense(map(attachments_in_inline, Inlines)).

attachments_in_paragraph(def_list_item(_, _, Inlines)) =
    list.condense(
        map(attachments_in_inline,
            Inlines)).

attachments_in_paragraph(horizontal_rule(_,_)) = [].

attachments_in_paragraph(table(_, InlineLLL)) =
    attlll(InlineLLL).

:- func attl(list(inline(target(P)))) = list(attachment(P))   <= pagename(P).

attl(InlineL) = condense(map(attachments_in_inline, InlineL)).

:- func attll(list(list(inline(target(P))))) = list(attachment(P))  <= pagename(P).

attll(InlineLL) = condense(map(attl, InlineLL)).

:- func attlll(list(list(list(inline(target(P)))))) = list(attachment(P))   <= pagename(P).

attlll(InlineLLL) = condense(map(attll, InlineLLL)).



% Get the attachments inside an inline.

:- func attachments_in_inline(
    inline(target(P))
) = list(attachment(P))   <= pagename(P).

attachments_in_inline(word(_)) = [].

attachments_in_inline(macro(_)) =
    [].

attachments_in_inline(
    link(link(Target,             % Link target
              _MaybeSubsection,   % Part after a "#" in the MoinMoin link
              MaybeDescription,   % If it has a description (after the first "|")
              _LinkArguments))    % Arguments of the link (after the second "|")
) = L :-
    ( Target = t_wikipage(_ : P),                                   A = []
    ; Target = t_external(_),                                       A = []
    ; Target = t_dangling_wikipage(_ : P),                          A = []
    ; Target = t_attachment(AttPage : P, AttFilename),              A = [ straight(AttPage, AttFilename) ]
    ; Target = t_dangling_attachment(AttPage : P, AttFilename),     A = [ dangling(AttPage, AttFilename) ]
    ),
    ( if   MaybeDescription = yes(right(Embed))
      then B = attachments_in_inline(embed(Embed))
      else B = []
    ),
    L = A ++ B.

attachments_in_inline(
    embed(embed(Target,
                _MaybeDescription,
                _Arguments))
) = L :-
    ( Target = t_wikipage(_ : P),                                   L = []
    ; Target = t_external(_),                                       L = []
    ; Target = t_dangling_wikipage(_ : P),                          L = []
    ; Target = t_attachment(AttPage : P, AttFilename),              L = [ straight(AttPage, AttFilename) ]
    ; Target = t_dangling_attachment(AttPage : P, AttFilename),     L = [ dangling(AttPage, AttFilename) ]
    ).

attachments_in_inline(inline_code(_)) = [].
attachments_in_inline(space) = [].
attachments_in_inline(italics) = [].
attachments_in_inline(bold) = [].
attachments_in_inline(underline) = [].
attachments_in_inline(subscript) = [].
attachments_in_inline(superscript) = [].
attachments_in_inline(begin_small) = [].
attachments_in_inline(end_small) = [].
attachments_in_inline(begin_big) = [].
attachments_in_inline(end_big) = [].
attachments_in_inline(begin_stroke) = [].
attachments_in_inline(end_stroke) = [].



% Analyse the attachment references inside all the parsed pages. See the interface section.

analyse_attachments(
    % in:
    ParsedPages,        %: assoc_list(P, page(target(P))),

    % out:
    AttLL,              %: assoc_list(P, list(attachment(P))),
    AttLL_straight,     %: assoc_list(P, list(attachment(P))),
    AttLL_dangling,     %: assoc_list(P, list(attachment(P))),
    ReferencedL,        %: assoc_list(P, list(attachment(P))),
    DanglingL,          %: assoc_list(P, list(attachment(P))),
    SelectedPages,      %: list(P),
    ExtraPages          %: list(P)
) :-

    % Extract the attachment references from the parsed pages.
    list.map((pred((Page - ParsedPage)::in, AttL::out) is det :-
                 AttL0 = attachments_in_page(ParsedPage),
                 sort_uniq(compare_attachments, AttL0, AttL1),
                 AttL = Page - AttL1),
              ParsedPages,
              AttLL  : assoc_list(P, list(attachment(P)))
             ),

    % The list of all the selected pages, together with the list of all straight attachment
    % references in that page.
    AttLL_straight  :  assoc_list(P, list(attachment(P))) = 
        map((func(P - AttL) = 
                P - filter((pred(Att::in) is semidet :- Att = straight(_,_)), AttL)),
            AttLL),

    % The list of all the selected pages, together with the list of all dangling attachment
    % references in that page.
    AttLL_dangling  :  assoc_list(P, list(attachment(P))) = 
        map((func(P - AttL) = 
                P - filter((pred(Att::in) is semidet :- Att = dangling(_,_)), AttL)),
            AttLL),
             
    % All Tiki page references, from all pages, sorted and made unique.
    sort_uniq(compare_attachments, 
              condense(map(snd, AttLL)),
              AllPageRefs  :  list(attachment(P)) ),
    SelectedPages = map(fst, ParsedPages),
    list.filter((pred(Att::in) is semidet :- Att = straight(_,_)),
                AllPageRefs,
                StraightRefs  :  list(attachment(P)),
                DanglingRefs  :  list(attachment(P))),

    ( if   map((pred(Att::in, Pagename::out) is semidet :- Pagename = straight_pagename(Att)),
               StraightRefs, StraightPages0)
      then StraightPages = StraightPages0
      else unexpected($pred, "map failed")
    ),

    % The extra Tiki pages. This are the pages referenced in an attachment reference, which aren't selected.
    ExtraPages = list.delete_elems(StraightPages, SelectedPages),

    % ReferencedL: List of the pages referenced by attachment links, together with the list of the
    % file names of the referenced attachments.
    ( if   group_straight(StraightRefs, ReferencedL0)
      then ReferencedL = ReferencedL0
      else unexpected($pred, "group_straight/2 failed")
    ),

    % DanglingL: List of the pages referenced by dangling attachment links, together with the list of the
    % file names of the referenced attachments.
    ( if   group_dangling(DanglingRefs, DanglingL0)
      then DanglingL = DanglingL0
      else unexpected($pred, "group_dangling/2 failed")
    ).




:- pred group_straight(list(attachment(P))::in, assoc_list(P, list(attachment(P)))::out) is semidet.

group_straight([], []).
group_straight(L@[straight(Page, _) | _], Res) :-
    take_while(
        (pred(Att::in) is semidet :- straight_pagename(Att) = Page),
        L,
        Same,
        Different),
    Res = [ Page - Same | Rest ],
    group_straight(Different, Rest).


:- pred group_dangling(list(attachment(P))::in, assoc_list(P, list(attachment(P)))::out) is semidet.

group_dangling([], []).
group_dangling(L@[dangling(Page, _) | _], Res) :-
    take_while(
        (pred(Att::in) is semidet :- dangling_pagename(Att) = Page),
        L,
        Same,
        Different),
    Res = [ Page - Same | Rest ],
    group_dangling(Different, Rest).
