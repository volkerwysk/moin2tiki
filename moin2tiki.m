%================================================================================
% The main module. This contains the command line arguments processing, the main/2 predicate, and
% the execution of the action chosen on the command line.
%================================================================================

:- module moin2tiki.

%================================================================================
% Interface
%================================================================================

:- interface.
:- import_module io, bool, list, map, set, maybe.
:- import_module pagenames, pages.
:- pred main(io::di, io::uo) is cc_multi.


% The command line arguments.
:- type params
   ---> params( p_user               :: string,
                p_language           :: string,
                /*p_history            :: bool,*/
                p_pagesdir           :: string,           % absolute path
                p_tmpdir             :: string,
                p_pnreplfile         :: string,
                p_pathsep            :: string,
                p_pages              :: list(moinpage),
                p_prefix             :: string,
                p_all                :: bool,
                p_nozombiemsg        :: bool,
                /*p_wwwuser            :: string,*/
                p_tikidir            :: string,
                p_wwwdir             :: string,
                /*p_schema             :: string,*/
                /*p_csv                :: string,*/
                p_adjust_hlevels     :: bool,
                p_ignore_list_read   :: string,
                p_ignore_list_write  :: string
              ).

:- func null_params = params.


% All the data that needs to be carried around.
:- type m2t
   ---> m2t(
       % Command line arguments
       m2t_params :: params,

       % The two mappings between Moin page names and Moin directory names
       m2t_pages_dirs :: pages_dirs,

       % The list of the selected Moin pages, on the command line. This is (unicode aware) sorted.
       m2t_selectedmoinpages :: list(moinpage),

       % The Moin page name -> Moin page parts mapping for all pages except zombies.
       m2t_moinpartsmap :: map(moinpage, moinparts),

       % Moin page name to Tiki page name resolver
       m2t_resolve :: func(moinpage) = tikipage,

       % Tiki page name to Moin page name resolver
       m2t_backwards_resolve :: func(tikipage) = moinpage,

       % Set of MoinMoin page names to ignore
       m2t_ignore :: set(moinpage),

       % File handle for the output ignore list
       m2t_ignore_out :: maybe(io.text_output_stream)
   ).


% Needed for writer.m
:- func null_m2t = m2t.


%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module char, getopt, string, exception, int, assoc_list, pair.
:- import_module posix, posix.strerror.
:- import_module tools, writer, parsertools, serialiser, create, dir_contents,
                 attachments, actions, unicode.


null_params = params("", "", /*no,*/ "", "", "", "", [], "", no, no, /*"",*/ "", "", /*"", "",*/ no, "", "").

null_m2t = m2t(null_params, null_pages_dirs, [], map.init,
               func(_) = tikipage(""),
               func(_) = moinpage(""),
               set.init,
               no).


% All the actions which require one selected page
:- func one_page_actions = list(string).
one_page_actions = ["parse", "convert"].


main(!IO) :-
    ( try [io(!IO)] main_1(!IO) )
    then
        true
    catch Str ->
        io.write_string(Str, !IO),
        io.nl(!IO),
        io.set_exit_status(if Str = "" then 0 else 1, !IO)
    catch (software_error(Str)) ->
        io.write_string(Str, !IO),
        io.nl(!IO)
    catch_any Any ->
        io.write_string("Unexpected exception:\n", !IO),
        io.write(Any, !IO),
        io.nl(!IO),
        io.set_exit_status(2, !IO).


:- pred main_1(io::di, io::uo) is det.

main_1(!IO) :-
    setlocale(!IO),

    io.command_line_arguments(Args, !IO),

    OptionOpts = option_ops(short_option, long_option, option_defaults),
    getopt.process_options(OptionOpts, Args, NonOptionArgs, OptionsResults),
    (
        OptionsResults = ok(OptTable),
        main_2(OptTable, NonOptionArgs, !IO)
    ;
        OptionsResults = error(OptionError),
        (   OptionError = unrecognized_option(Opt),
            io.format("Unrecognized option: %s\n", [s(quote_qm(Opt))], !IO)
        ;   OptionError = option_error(_, Opt, _),
            io.format("Error option %s\n", [s(quote_qm(Opt))], !IO)
        ),
        io.set_exit_status(1, !IO)
    ).


:- pred main_2(option_table(option)::in, list(string)::in, io::di, io::uo) is det.

main_2(OptTable, NonOptionArgs, !IO) :-

    ( if   NonOptionArgs = ["help"]
      then io.write_string(usage_info, !IO),
           throw("")

      else if NonOptionArgs = ["license"]
      then io.write_string(license_info, !IO),
           throw("")
      else true
    ),

    % Process command line options

    getopt.lookup_accumulating_option(OptTable, page, Page0),
    getopt.lookup_string_option(OptTable, prefix, Prefix),
    getopt.lookup_bool_option(OptTable, allpages, Allpages),
    getopt.lookup_string_option(OptTable, pagesdir, Pagesdir_rel),
    getopt.lookup_string_option(OptTable, pnreplfile, Pnreplfile),
    getopt.lookup_string_option(OptTable, tmpdir, Tmpdir),
    getopt.lookup_string_option(OptTable, user, User),
    getopt.lookup_string_option(OptTable, language, Language),
    getopt.lookup_string_option(OptTable, pathsep, Pathsep),
    /*getopt.lookup_bool_option(OptTable, history, History),*/
    getopt.lookup_bool_option(OptTable, nozombiemsg, Nozombiemsg),
    /*getopt.lookup_string_option(OptTable, wwwuser, WWWUser),*/
    getopt.lookup_string_option(OptTable, tikidir, Tikidir),
    getopt.lookup_string_option(OptTable, wwwdir, WWWDir),
    /*getopt.lookup_string_option(OptTable, schema, Schema),*/
    getopt.lookup_bool_option(OptTable, adjust_hlevels, Adjust_hlevels),
    /*getopt.lookup_string_option(OptTable, csv, CSV),*/
    getopt.lookup_string_option(OptTable, ignore_list_read, Ignore_list_read),
    getopt.lookup_string_option(OptTable, ignore_list_write, Ignore_list_write),

    Params = params(User, Language, /*History,*/ Pagesdir, Tmpdir, Pnreplfile, Pathsep,
                    Page, Prefix, Allpages, Nozombiemsg, /*WWWUser,*/ Tikidir, WWWDir, /*Schema,*/ /*CSV,*/
                    Adjust_hlevels, Ignore_list_read, Ignore_list_write),

    ( if   NonOptionArgs = [Command0]
      then Command = Command0
      else throw("Error: Exactly one action must be specified. Call ""moin2tiki help"" for help.")
    ),

    Page = map((func(P) = moinpage(P)), Page0),

    ( if member(Command, actionnames)
      then true
      else throw("Unknown action """ ++ Command ++ """")
    ),

    ( if   Pagesdir_rel = ""
      then throw("The path to the MoinMoin ""pages"" directory must be specified with -d or \n" ++
                 "--pagesdir.")
      else true
    ),

    tools.realpath(Pagesdir_rel, Pagesdir, !IO),

    ( if at_most_one([if Page \= []      then yes else no,
                      if Prefix \= ""    then yes else no,
                      if Allpages = yes  then yes else no
                     ])
      then true
      else throw("At most one of the page selection options must be present. Call ""moin2tiki\n" ++
                 "help"" for help.\n")
    ),

    ( if   Command = "migrate", ( User = "" ; Language = "" )
      then throw("The ""migrate"" command requieres that the user and the language for the Tiki\n" ++
                 "pages is specified with ""--user"" and ""--language"".")
      else true
    ),

    /*
    ( if   Command = "csv", ( Schema = "" ; CSV = "" )
      then throw("The ""csv"" command requires the schema and CSV files to be specified with " ++
                 "--schema and --csv")
      else true
    ),*/

    ( if   Command = "ignore", Ignore_list_write = ""
      then throw("The ""ignore"" command needs the --ignore-list-write argument.")
      else true
    ),

    ( if   member(Command, one_page_actions),
           not(SelectedMoinPages = [_])
      then throw("For the """ ++ Command ++ """ action, exactly on page must be selected.")
      else true
    ),

    % Read the ignore list, if any

    ( if   Ignore_list_read \= ""
      then tools.contents(Ignore_list_read, Res, !IO),
           ( Res = ok(Contents),
             IgnoreList = string.split_into_lines(Contents)
           ; Res = error(Error),
             io.format("Can't read the ignore list %s, ignoring:\n%s\n\n", 
                       [s(quote(Ignore_list_read)), s(error_message(Error))], !IO),
             IgnoreList = []
           )
      else IgnoreList = []
    ),
    Moinpage1 = (func(MP) = moinpage(MP)),
    Ignore = set.list_to_set(list.map(Moinpage1, IgnoreList)),
          
    % Open the output ignore list file, if any.

    ( if   Ignore_list_write \= ""
      then io.open_append(Ignore_list_write, ResILW, !IO),
           ( ResILW = ok(Stream),
             IgnoreOut = yes(Stream)
           ; ResILW = error(ErrorILW : io.error),
             throw(Ignore_list_write ++ ": " ++ error_message(ErrorILW))
           )
      else IgnoreOut = no
    ),
           
    % Read in the data about all pages and the pages selected on the command line

    pages_data(Params,                  % Command line arguments
               Ignore : set(moinpage),  % Moin Pages to ignore
               IgnoreOut,               % Output file for zombie pages

               % Output arguments:
               Pages_Dirs,              % The two page name <-> dir name mappings
               SelectedMoinPages,       % The names of the Moin pages selected on the command line,
                                        % sorted.
               MoinpartsMap,            % The mapping Moin page name -> Moinparts for all pages
                                        % except zombies
               Resolve,                 % Moin page name -> Tiki page name resolver function
               BackwardsResolve,        % Tiki page name -> Moin page name resolver function
               !IO),
           
    % Call the selected action

    M2T = m2t(Params, Pages_Dirs, SelectedMoinPages, MoinpartsMap, 
              Resolve, BackwardsResolve, Ignore, IgnoreOut),
    call_action(Command, M2T, !IO),

    % Close the output ignore list file, if any.

    ( IgnoreOut = yes(IOStream),
      close_output(IOStream, !IO)
    ; IgnoreOut = no
    ).


%--------------------------------------------------------------------------------
% Command line arguments
%
% /usr/local/src/mercury-srcdist-20.01.2/extras/posix/samples/mdprof_cgid.m

:- type option
         ---> pagesdir            % pages directory
         ;    tmpdir              % File name for the ZIP file which is created
         ;    user                % User for the wiki pages
         ;    language            % Language for the wiki pages
/*         ;    history             % Include the full history of pages*/
         ;    page                % select one page
         ;    prefix              % select all pages with a prefix
         ;    allpages            % select all pages
         ;    pnreplfile          % Name of the page names replacement file
         ;    pathsep             % Path separator for hierarchical namespaces
         ;    nozombiemsg         % Don't output the messages about ignoring zombie pages
/*         ;    wwwuser             % Tiki user name*/
         ;    tikidir             % Path of the Tiki root directory under the document root directory
         ;    wwwdir              % the document root directory
/*         ;    schema              % File to contain a tracker schema (fields list)*/
/*         ;    csv                 % CSV file name to be generated by the "csv" action.*/
         ;    adjust_hlevels      % Adjust heading levels
         ;    ignore_list_read    % File name of list of MoinMoin page to ignore.
         ;    ignore_list_write.  % File name of list for appending processed MoinMoin pages.

:- pred short_option(char::in, option::out) is semidet.

short_option('d',  pagesdir).
short_option('t',  tmpdir).
short_option('p',  page).
short_option('P',  prefix).
short_option('a',  allpages).
/*short_option('h',  history).*/
short_option('r',  pnreplfile).
short_option('s',  pathsep).
short_option('n',  nozombiemsg).
/*short_option('w',  wwwuser).*/
short_option('T',  tikidir).
short_option('D',  wwwdir).
/*short_option('S',  schema).*/
/*short_option('C',  csv).*/
short_option('R',  ignore_list_read).
short_option('W',  ignore_list_write).

:- pred long_option(string::in, option::out) is semidet.

long_option("pagesdir",          pagesdir).
long_option("tmpdir",            tmpdir).
long_option("page",              page).
long_option("prefix",            prefix).
long_option("all",               allpages).
long_option("user",              user).
long_option("language",          language).
/*long_option("history",           history).*/
long_option("pnrepl",            pnreplfile).
long_option("pathsep",           pathsep).
long_option("nozombiemsg",       nozombiemsg).
/*long_option("wwwuser",           wwwuser).*/
long_option("tikidir",           tikidir).
long_option("wwwdir",            wwwdir).
/*long_option("schema",            schema).*/
/*long_option("csv",               csv).*/
long_option("adjust-hlevels",    adjust_hlevels).
long_option("ignore-list-read",  ignore_list_read).
long_option("ignore-list-write", ignore_list_write).


:- pred option_default(option::out, option_data::out) is multi.

option_default(page,              accumulating([])).
option_default(prefix,            string("")).
option_default(allpages,          bool(no)).
option_default(pagesdir,          string("")).
option_default(tmpdir,            string("moin2tiki.tmp")).
option_default(user,              string("")).
option_default(language,          string("")).
/*option_default(history,           bool(no)).*/
option_default(pnreplfile,        string("pnrepl")).
option_default(pathsep,           string(":_:")).
option_default(nozombiemsg,       bool(no)).
/*option_default(wwwuser,           string("www-data")).*/
option_default(tikidir,           string("tiki")).
option_default(wwwdir,            string("/var/www/html")).
/*option_default(schema,            string("")).*/
/*option_default(csv,               string("")).*/
option_default(adjust_hlevels,    bool(no)).
option_default(ignore_list_read,  string("")).
option_default(ignore_list_write, string("")).


:- pred option_defaults(option::out, option_data::out) is nondet.

option_defaults(Option, Default) :-
    option_default(Option, Default),
    semidet_true.




% Test if at most one action is specified. Takes a list of bools and throws an exception if there
% are more than one yes in it.

:- pred at_most_one(list(bool)::in) is semidet.

at_most_one(L) :-
    foldl((pred(Yes::in, Num::in, Num1::out) is det :-
               if Yes = yes then Num1 = Num + 1
                            else Num1 = Num),
          L,
          0,
          Numb),
    ( if   Numb =< 1
      then true
      else fail
    ).

:- func usage_info = string.

usage_info =
"MoinMoin to Tiki migration tool, version 1.1.1, released 2023-02-28.
Author: Volker Wysk <post@volker-wysk.de>

moin2tiki action [options]

You need to specify an action and select some pages.

Actions:
  help           Print this help.
  license        Print copyright and warranty information.
  migrate        Convert the selected pages from MoinMoin to Tiki and import
                 them to the Tiki server.
  listmoin       List the MoinMoin page names of all selected pages.
  listtiki       List the Tiki versions of the selected MoinMoin page names.
  attachments    For each selected page, list the attachments which it refers.
  ignore         Just add the selected pages, and the zombie pages, to the
                 ignore list. Don't do anything with the selected pages. See
                 --ignore-list-write.

Developer actions:
  parse          Parse the selected page and print the results. One page must
                 be selected.
  convert        Convert one page from MoinMoin to Tiki and output the result.
                 Exactly one page must be selected.
  create         Create the intermediate directory structure, which can be used
                 to import the selected pages with the moin-import.php script.
                 The directory path is specified with the ""--tmpdir"" argument.

Page selections options:
  -p/--page pagename   Select a page for processing. This can be specified
                       several times.
  -P/--prefix txt      Select all pages with the prefix txt for processing.
  -a/--all             Select all pages in the pages directory for processing.

Required options:
  -d/--pagesdir dir    The MoinMoin ""pages"" directory.
  -t/--tmpdir name     The name for intermediate directory. Defaults to
                       ""./moin2tiki.tmp"".
  --user user          User to be filled in for the user and creator of wiki
                       pages.
  -l/--language lang   Language to be filled in for wiki pages.

Other options:
  -r/--pnrepl filename The name of the page names replacement file (defaults to
                       ""./pnrepl"")
  -s/--pathsep sep     The separator for the path components of  namespaces.
                       Defaults to the Tiki default, which is "":_:"".
  -n/--nozombiemsg     Don't print messages about zombie pages being ignored.

  -T/--tikidir relpath The path of the Tiki root directory in the web server, 
                       below the document root. This defaults to ""tiki"".
                       Can be empty.
  -W/--wwwdir dir      The document root directory of the webserver. This
                       defaults to ""/var/www/html/"".
  --adjust-hlevels     Change the levels of the headings in the selected pages,
                       such that the headings' levels start at 1.
  -R/--ignore-list-read filename
                       File name of the list of MoinMoin pages which are to be
                       ignored.
  -W/--ignore-list-write filename
                       File name of list of MoinMoin pages which have been
                       successfully processed. This appends the page names
                       to the file.
".



/*
"Actions only needed for ""record pages"":
  records        The pages as records and output them.
  fields         Read several pages as records and report the field names
                 found.
  regalplätze    ..." ++
*/

/*"  -h/--history         Include the page history in the Tiki pages. The default
                       is to only include the current version of pages." ++ 
*/

/*
"  -w/--wwwuser user    Unix user name for the Tiki PHP scripts. Defaults to
                       ""www-data""." ++
*/

/*
"  -S/--schema filename Tiki tracker export file, containing the schema in the
                       first line. This is for the ""csv"" action.
  -C/--csv filename    File name for the CSV file which is to be generated by
                       the ""csv"" action." ++ 
*/


:- func license_info = string.

license_info =
"Copyright 2023 Volker Wysk <post@volker-wysk.de>

Moin2Tiki is distributed under the terms of the GNU General Public License,
Version 3 or any later version. See the file COPYING for details.

Moin2Tiki is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Moin2Tiki is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with Moin2Tiki. If not, see <https://www.gnu.org/licenses/>.
".
