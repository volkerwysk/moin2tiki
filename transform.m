%================================================================================
% Page transformator. This maps a completely parsed page from one link target type to another.
%================================================================================

:- module transform.

:- interface.
:- import_module parser, moin2tiki, pages.


% Transform a list of paragraphs from one link target type to another.

:- pred transform(
    (pred(Targ1, Targ2)::(pred(in,out) is det)),% The link target transformation predicate
    page(Targ1)::in,                            % Parsed page in
    page(Targ2)::out)                           % Parsed page out
is det.


% Transformator for raw link targets to MoinMoin link targets

:- pred raw_to_moin(
    m2t::in,                    % Collected data about all pages
    moinpage::in,               % The page being transformed
    raw::in,                    % The raw link target
    target(moinpage)::out)      % The MoinMoin link target
is det.


% Transformator for MoinMoin link targets to Tiki link targets

:- pred moin_to_tiki(
    m2t::in,                    % Collected data about all pages
    target(moinpage)::in,       % The Moin link target
    target(tikipage)::out)      % The raw link target
is det.


%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module list, int, exception, maybe, bool, map, string, char.
:- import_module tools, pagenames.


transform(Transf, PageIn, PageOut) :-
    list.map(trans_para(Transf), PageIn, PageOut).



% Transform a paragraph from one link target type to another.

:- pred trans_para(
    (pred(Targ1, Targ2)::in(pred(in,out) is det)),   % The link target transformation predicate
    paragraph(Targ1)::in,                            % Paragraph in
    paragraph(Targ2)::out)                           % Paragraph out
is det.

trans_para(_, heading(Level, Str), heading(Level, Str)).
trans_para(_, code_block(Codeblock), code_block(Codeblock)).
trans_para(_, comment_block(Lines), comment_block(Lines)).
trans_para(_, empty_line, empty_line).
trans_para(_, horizontal_rule(Ind, Width), horizontal_rule(Ind, Width)).



% type error: variable `InlineLLL1' has type `
% (some [Targ1] list.list(list.list(list.list(parser.inline(Targ1)))))',

% expected type was `(some [Targ1]
%               list.list(list.list(list.list(Targ1))))'.

trans_para(Transf, table(Ind, InlineLLL1), table(Ind, InlineLLL2)) :-
    list.map(
        list.map(
            list.map(trans_inline(Transf))),
        InlineLLL1, InlineLLL2).

trans_para(Transf, text(Ind, Inlines1), text(Ind, Inlines2)) :-
    trans_inlines(Transf, Inlines1, Inlines2).

trans_para(Transf, list_item(Ind, Type, Inlines1), list_item(Ind, Type, Inlines2)) :-
    trans_inlines(Transf, Inlines1, Inlines2).

trans_para(Transf, def_list_item(Ind, Word, Inlines1), def_list_item(Ind, Word, Inlines2)) :-
    trans_inlines(Transf, Inlines1, Inlines2).



% Transform a list of inlines from one link target type to another.

:- pred trans_inlines(
    (pred(Targ1, Targ2)::in(pred(in,out) is det)),       % The link target transformation predicate
    list(inline(Targ1))::in,                    % Inlines in
    list(inline(Targ2))::out)                   % Inlines out
is det.

trans_inlines(Transf, Inlines1, Inlines2) :-
    map(trans_inline(Transf), Inlines1, Inlines2).



% Transform an inline from one link target type to another.

:- pred trans_inline(
    (pred(Targ1, Targ2)::in(pred(in,out) is det)),  % The link target transformation predicate
    inline(Targ1)::in,                              % Inline in
    inline(Targ2)::out)                             % Inline out
is det.

trans_inline(_, word(Word), word(Word)).
trans_inline(_, macro(Macro), macro(Macro)).
trans_inline(_, inline_code(Code), inline_code(Code)).
trans_inline(_, space, space).
trans_inline(_, italics, italics).
trans_inline(_, bold, bold).
trans_inline(_, underline, underline).
trans_inline(_, subscript, subscript).
trans_inline(_, superscript, superscript).
trans_inline(_, begin_small, begin_small).
trans_inline(_, end_small, end_small).
trans_inline(_, begin_big, begin_big).
trans_inline(_, end_big, end_big).
trans_inline(_, begin_stroke, begin_stroke).
trans_inline(_, end_stroke, end_stroke).

trans_inline(
    Transf,
    link(link(Target1, MSubs, MDesc1, Args)),
    link(link(Target2, MSubs, MDesc2, Args))
) :-
    Transf(Target1, Target2),
    (   MDesc1 = yes(left(Str)),
        MDesc2 = yes(left(Str))
    ;
        % MDesc1 = yes(right(Embed1)),
        % MDesc2 = yes(right(Embed2)),
        % trans_inline(Transf, embed(Embed1), embed(Embed2))
        MDesc1 = yes(right(embed(TargetA, EMDesc, EArgs))),
        MDesc2 = yes(right(embed(TargetB, EMDesc, EArgs))),
        Transf(TargetA, TargetB)
    ;
        MDesc1 = no,
        MDesc2 = no   %%%
    ).

trans_inline(
    Transf,
    embed(embed(Target1, MDesc, Args)),
    embed(embed(Target2, MDesc, Args))
) :-
    Transf(Target1, Target2).


%--------------------------------------------------------------------------------
% Transformer predicates from one link target type to another.
%--------------------------------------------------------------------------------


raw_to_moin(
    M2T,                        % in
    Parent,                     % in
    raw(RawTarget, IsAtt),      % in
    MoinTarget                  % out
) :-
    (
        if
            IsAtt = yes
        then
            % Link to an attachment
            att_link_parts(RawTarget, AttMoinPageStr, AttFilename0),
            AttFilename = attname_encode(AttFilename0),
            AttMoinPage = make_link_absolute(Parent, AttMoinPageStr),
            (
                if
                    % If the page exists (is present in the pages->dirs mapping)
                    _Dir = page_to_dir(M2T ^ m2t_pages_dirs, AttMoinPage)

                then
                    % The page exists. The link can still dangle, when there isn't the attached
                    % file
                    Moinparts = map.lookup(M2T ^ m2t_moinpartsmap, AttMoinPage),
                    (    
                        if
                            list.member(AttFilename, Moinparts ^ mp_attachments)
                        then
                            % The attached file exists on disk. We have a valid link to an attachment.
                            MoinTarget = t_attachment(AttMoinPage, AttFilename)
                        else
                            % The attached file doesn't exists on disk. We have a dangling attachment.
                            MoinTarget = t_dangling_attachment(AttMoinPage, AttFilename)
                    )
                else
                    % Else we have a dangling link to an attachment
                    MoinTarget = t_dangling_attachment(AttMoinPage, AttFilename)
            )
        else
            % External or internal link to a page
            (
                if
                    is_external_link(RawTarget)
                then
                    % External link (begins with "https://" or "http://")
                    MoinTarget = t_external(RawTarget)
                else
                    % Link to a wiki page (internal link)
                    AbsTarget = make_link_absolute(Parent, RawTarget),
                    (
                        if
                            % If the page exists (is present in the pages->dirs mapping)
                            _ = page_to_dir(M2T ^ m2t_pages_dirs, AbsTarget)
                        then
                            % Then we have a valid link to a wiki page
                            MoinTarget = t_wikipage(AbsTarget)
                        else
                            % Else we have a dangling link to an attachment
                            MoinTarget = t_dangling_wikipage(AbsTarget)
                    )
            )
    ).



% MoinMoin changes some characters in attachement file names to underscores, so the limitations of
% the underlying file system are met. This function does this. See the "Coding of attachment file
% names" thread in the moin-user mailing list, as of 2022-08-08.

:- func attname_encode(filename) = filename.
attname_encode(In) = Out :-
    InList = string.to_char_list(filename(In)),
    Replace = (func(InChar) = OutChar :-
                  ( if char.to_int(InChar) =< 31
                       ; InChar = (':')
                       ; InChar = ('/')
                       ; InChar = ('\\')
                       ; InChar = ('<')
                       ; InChar = ('>')
                       ; InChar = ('"')
                       ; InChar = ('*')
                       ; InChar = ('?')
                       ; InChar = ('%')
                       ; InChar = ('|')
                    then OutChar = ('_')
                    else OutChar = InChar
                  )),
    OutList = list.map(Replace, InList),
    Out = filename(string.from_char_list(OutList)).



moin_to_tiki(
    M2T,                % Collected data about all pages
    MoinPage,           % The Moin link target
    TikiPage            % The raw link target
) :-
    (
        MoinPage = t_wikipage(Page),
        TikiPage = t_wikipage((M2T ^ m2t_resolve)(Page))
    ;
        MoinPage = t_external(URL),
        TikiPage = t_external(URL)
    ;
        MoinPage = t_attachment(AttPage, AttFilename),
        TikiPage = t_attachment((M2T ^ m2t_resolve)(AttPage), AttFilename)
    ;
        MoinPage = t_dangling_wikipage(Page),
        TikiPage = t_dangling_wikipage((M2T ^ m2t_resolve)(Page))
    ;
        MoinPage = t_dangling_attachment(AttPage, AttFilename),
        TikiPage = t_dangling_attachment((M2T ^ m2t_resolve)(AttPage), AttFilename)
    ).




% Parse a MoinMoin link into page and filename parts. The link is the page part, a "/" and the
% filename part. This applies to the attachment link after "attachment:" as well as to URLs.

:- pred att_link_parts(
    string::in,      % The MoinMoin link target (after the "attachment:" for attachments, or the
                     % entire link otherwise)
    string::out,     % The page name part of the link (can be empty). This is a Moin page name or
                     % the the beginning of an URL.
    filename::out)   % The filename part of the link. This is everything after the last "/".
is det.


att_link_parts(
    Target,             % in:  Link target (after the "attachment:")
    PagePart, Filename  % out: The MoinMoin page specified in the link target and the filename of
                        %      the link target.
) :-
    slice_path(Target, Comps, _),
    (   Comps = [],
        PagePart = "",
        Filename = filename("")
    ;   Comps = [_|_],
        det_split_last(Comps, PageL, Filename0),
        PagePart = join_list("/", PageL),
        Filename = filename(Filename0)
    ).

