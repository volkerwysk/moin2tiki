
## Operation

MoinMoin stores all the pages as plain text files in a shared directory with the name "pages". Each page has a
subdirectory whose name is derived from the page's name. In the "revisions" subsubdirectory, the different
revisions of a page are stored. The current version of Moin2Tiki handles only the current revision. This
means that different revisions aren't supported (yet). Moin2Tiki, in the current version, doesn't touch any other
MoinMoin data than what's in that `pages` directory.

Tiki stores its pages in a MySQL/MariaDB database. This database is accessed from the PHP scripts which constitute
the Tiki engine. In order to store pages in the database, Moin2Tiki uses a PHP script, called `moin-import.php`,
which calls the respective Tiki PHP scripts, which access the database. This script must be placed in the Tiki top
directory, together with all the other top-level Tiki PHP scripts.

Moin2Tiki reads the selected MoinMoin pages and parses them. Then, for each page, it generates a Tiki page which
recreates the corresponding MoinMoin page. These pages, together with any attachments of the original pages, are
fed into the `moin-import.php` script, which stores them in the Tiki database.

Attachments are imported to Tiki only, when there isn't already an attachment with the same name. When there is,
the two aren't compared. It is assumed that you're trying to import new pages to Tiki, not replace existing ones.
But you can do that. When there already is a page with the same name, the imported one is saved as a new revision
of that page.

The Moin2Tiki program, "`moin2tiki`", is to be run as the web server's user. This is because it needs access to
the Tiki installation. The user usually has the name "www-data". You should use the command `sudo -uwww-user
/path/to/moin2tiki ...` to call Moin2Tiki. This means that the WWW user must have the permissions to read and
execute the `moin-import.php` script.

There are quite a few parameters, which the Moin2Tiki program takes - see [Invocation](Invocation.html). You are
encouraged to write a script which calls it, which sets all the parameters. There's `moin2tiki.sh` as an example.


## Actions

The `moin2tiki` program takes exactly one action when it is called. The main action is `migrate`, which migrates
MoinMoin pages to Tiki. Other notable actions are `listmoin`/`listtiki`, which will show you which pages you have
selected. And `ignore` for pages you don't want to migrate.

Run `moin2tiki help` for a list of all the actions and other options.

Apart from an action, `moin2tiki` also requires some pages, to be selected, to operate on. (Except for the `help`
and `license` actions).

See [Invocation](Invocation.html) for more details.


## Users

The biggest limitation on Moin2Tiki is, for now, that only one user is supported. See
[Limitations](Limitations.html). You need to create this user in Tiki. The name of that user must be
specified to the `moin2tiki` program. See [Invocation](Invocation.html).


## The Intermediate Directory Structure

Before calling `moin-import.php`, the `moin2tiki` program creates an intermediate directory structure, which
contains the converted pages and symbolic links to the attachments. The attachments are stored by MoinMoin in the
pages directory (inside subsubdirectories called "attachments", for each page). The `moin-import.php` script scans
this intermediate directory structure and uses it to feed the data into the Tiki database. Since the
`moin-import.php` script is run as the same user as the main `moin2tiki` program, the WWW user must have read
permissions for the MoinMoin pages directory.

You normally don't catch sight of this intermediate directory, because it is be removed after a migration. If you
want to see it without doing any migration, you can create it with the `create` action. It won't be deleted like
after a migration run, then. But you need to delete it, or move it out of the way, before the next time you do
something.


## The Ignore List

The ignore list is a text file of MoinMoin page names, one per line, which are ignored by all Moin2Tiki actions.
This list can be used to track which pages have already been migrated. You can also use it to specify that some
pages should *not* be migrated, by using the "ignore" action. This way you don't need to migrate all pages at once.

There are two options for the ignore list(s). `--ignore-list-read` specifies the ignore list, which is to be read
before executing the selected action. The pages listed in it will be ignored. The `--ignore-list-write` option
specifies an ignore list, to which the names of successfully migrated MoinMoin pages will be appended. You can also
explicitly add pages to the ignore list with the `ignore` action. Then the selected pages will just be added to the
ignore list, without migrating them.

Both lists can be the same file.

It doesn't hurt, however, to import the same pages several times. When a page is imported, Moin2Tiki compares its
content with the last version of the page, which is already there (if any). When the two are identical, the page is
skipped and not migrated again.


## The Success List

The `moin-import.php` script writes a list of all successfully migrated Tiki pages, called the "success list". This
list is then read by `moin2tiki` and converted backwards to a list of successfully migrated MoinMoin pages.
These MoinMoin page names are appended to the ignore list (see above). 


## Transformation of Page Names

MoinMoin has the advantage that all characters, except for the slash, can be used in page names. The slash
separates page name components for hierachical page names. Tiki doesn't have as much flexibility. The following
characters are called "problematic" in page names:

/ ? # [ ] @ $ & + ; = &lt; > "

Moin2Tiki rewrites the MoinMoin page names such that these characters don't occur any longer. For this, a file
called the "replacements file" is being used. It is meant for replacing "problematic" characters, or character
sequences, in MoinMoin page names with allowed character sequences in the Tiki page names. But it can also be used
for replacements of any character sequences.

The replacements file contains a list of rules for how to replace sequences of characters, one per line. Each rule
consists of a character sequence to be replaced and one to replace it. The two sequences are strings, enclosed in
quotes and they are separated by `->`.

Any problematic characters that aren't replaced by the rules in the replacements file, get replaced with
underscores.

What happens when two distinct MoinMoin page names are translated to the same Tiki page name? This is possible. In
this case, the two are made unique by adding a number at the end.


## The Namespace Separator

Tiki has something similar to MoinMoin's hierarchical page names. It's called "namespaces". Like the slash in
MoinMoin, they have a separator of the page name components. Unlike MoinMoin, this separator can be a string, not
just one character. And it isn't unchangable, but can be configured (like most things in Tiki).

The separator can be chosen in the Tiki control panels. See [Preparations](Preparations.html). The default
namespace separator is `:_:`. This isn't very pretty. `::` can be used as the separator, but this is already used
for centering text. Therefore there is the Tiki option to change the markup for centering text to `:::`. This can't
be done retroactively. When you already have Tiki pages, which use the old namespace separator, then you're
stuck with it, since the pages won't automatically be rewritten to use a new separator.

You specify the namespace separator for Moin2Tiki with the `--pathsep` option. When you use `::` as it, then
Moin2Tiki also assumes that `:::` is used for centering text.

The slash can't be used as the namespace separator.


## Pages and Extra Pages

MoinMoin pages can make use of the attachments in any page. The attachments don't need to be in the same pages as
the links to them. So we have two sorts of pages when migrating from MoinMoin to Tiki. First, we have the selected
pages. They can have attachments. Second, we have references to attachements in other, non-selected pages. The
pages in which those external attachments are stored, are called "extra pages" in Moin2Tiki.

When migrating from MoinMoin to Tiki, the regular pages as well as the extra pages must be considered. This means
that you get your migrated pages and, additionally, the extra pages in Tiki. The latter are empty, only their
attachments are in use.

When you later select extra pages to be migrated, they are created as regular pages. The empty page will be
replaced by the new, migrated page, so you won't get an empty first revision for extra pages.


## Zombie Pages

Pages that are deleted in MoinMoin aren't really dead. The page isn't deleted, only the current revision is
removed. When you create a new page with the same name, the rest of the revisions of the "deleted" page come
back to life.

I call those not quite deleted pages "zombie pages". They are ignored by Moin2Tiki. When you start moin2tiki for
the first time, you get messages about the detected zombie pages. When you chose to add the processed pages to the
ignore list, then the zombie pages will be added to the list of pages which are being ignored, and you won't get
the messages about zombie pages again. If this is all what you want, you can select no pages and use the "ignore"
action.

