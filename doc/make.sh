#! /bin/bash

#for n in *.md ; do cmark-gfm --extension table "$n" > "`basename --suffix=.md \"$n\"`.html" ; done

for n in *.md ; do 
    BASENAME=`basename --suffix=.md "$n"`
    echo "$BASENAME"
    pandoc --toc --standalone "$n" -o "$BASENAME.html" --metadata title="$BASENAME" \
           --css=custom.css --embed-resources --standalone
done
