%================================================================================
% Parser for MoinMoin wiki syntax
% See https://moinmo.in/HelpOnMoinWikiSyntax
%================================================================================

:- module parser.

:- interface.
:- import_module list, string, int, bool, maybe, char, assoc_list.
:- import_module tools, pages.

%:- pred main(io::di, io::uo) is det.
:- pred page(list(paragraph(raw))::out, list(char)::in, list(char)::out) is nondet.


% Types =========================================================================

:- type page(T) == list(paragraph(T)).


% Things at the paragraph level
:- type paragraph(T)
   ---> heading(int, string)          % Level and text
   ;    code_block(code_block)        % A block of code. See "type code_block", below
   ;    text(int, list(inline(T)))    % Indentation and contents
   ;    comment_block(list(string))   % A comment, can be several lines
   ;    empty_line                    % One or several empty lines
   ;    list_item(                    % An element of a list
                  int,                       % Indentation of the list item. Starts at 1.
                  list_type,                 % Style of the list
                  list(inline(T)))           % The contents of the list item
   ;    def_list_item(                % An element of a definition list
                  int,                       % Indentation of the list item. Starts at 1.
                  string,                    % The defined word(s)
                  list(inline(T)))           % The definition
   ;    horizontal_rule(              % Horizontal rule
                        int,                 % Indentation (see comment at the parse rule)
                        int)                 % Width of the rule (4 to 10)
   ;    table(                        % A Table
            int,                             % Indentation
            list(list(list(inline(T))))). % The contents of the table


% The various list types in MoinMoin
:- type list_type
   ---> bullet                  % " * "
   ;    dot                     % " . "
   ;    number                  % " 1. ", " 2. " etc.
   ;    uppercase_letter        % " A. "
   ;    lowercase_letter        % " a. "
   ;    uppercase_roman         % " I. "
   ;    lowercase_roman.        % " i. "


% Things inside paragraphs
:- type inline(T)
   ---> word(string)

   ;    macro(macro)                    % Macro

   ;    link(link(T))                   % Hypertext link (inside "[[...]]")

   ;    embed(embed(T))                 % Embedded object (inside "{{...}}")

   ;    inline_code(string)             % Code inside an inline (inside "{{{...}}}" or "`...`")

   ;    space
   ;    italics
   ;    bold
   ;    underline
   ;    subscript
   ;    superscript
   ;    begin_small                     % ~-
   ;    end_small                       % -~
   ;    begin_big                       % ~+
   ;    end_big                         % +~
   ;    begin_stroke                    % --(
   ;    end_stroke.                     % )--


% Hypertext link. See https://moinmo.in/HelpOnLinking
:- type link(T)
   ---> link(
       % The link target
       link_target :: T,

       % Subsection, if any. After "#"
       link_msubsection :: maybe(string),

       % Description (after the first "|"), if specified. "no" when empty or missing.
       % yes(left(string)) for descriptive text
       % yes(right(embed)) for an embedded object, in "{{...}}"
       link_mdescription :: maybe(either(string,
                                         embed(T))),

       % Arguments of the link (after the second "|")
       link_arguments :: arguments).


% Embedded object. See https://moinmo.in/HelpOnLinking
:- type embed(T)
   ---> embed(
       % What is to be embedded (referenced page (may be emtpy) and filename)
       embed_target :: T,

       % Description ("alt text"), after the first "|", when appropriate
       embed_mdescription :: maybe(string),

       % Arguments of the link (after the second "|")
       embed_arguments :: arguments).


% MoinMoin macro. A macro call can either have no arguments ("<<Macro>>" or "<<Macro()>>"), a string
% ("<<Macro(String)>>") or a list of key-value pairs as its arguments ("<<Macro(key=val, ...)>>").
% See pred macro for more information.
%
% If it has a string argument, then spaces at the beginning and end of it are removed.
:- type macro
   ---> macro( macro_name :: string,                       % name of the macro
               macro_arguments ::
                   maybe(                                  % the arguments of the macro, if any
                       either(string,                      % a string as argument
                           arguments))).                   % a list of key-value pairs as arguments


% A code block (paragraph level)

:- type code_block
   ---> code_block(
            int,                % Indentation
            list(string),       % The text of the code block (lines)
            maybe(              % In case there is syntax highlighting,
                { string,       %   Name of the hightlighing syntax (e.g. "shell")
                  bool })).     %   If line numbers are to be generated


% Arguments of a macro, a link or an embed, in the form "name=value". Can be a single, unnamed value
% (empty name), or several named values. Can be an unnamed value, for instance in
% "<<TableOfContents(2)>>". Then the arguments list consists of one argument, with empty name.

:- type arguments
   == assoc_list(string,             % name
                 string).            % value






% A raw link target, like the parser delivers. The target string hasn't been examined yet.

:- type raw
   ---> raw(
       raw_target :: string,    % The link target like it is specified in the Moin page. For
                                % attachments, the part after "attachment:".
       raw_isatt :: bool).      % If it is a reference to an attachment.


% A link target which has been completed to an absolute page path.

:- type target(P)
   --->
       t_wikipage(                      % Internal link to another wiki page
           t_wikipage :: P
       )
   ;
       t_external(                      % External reference
           t_url :: string              % URL of the link
       )
   ;
       t_attachment(                    % Reference to an attachment
           t_attpage :: P,              % A complete, absolute Moin page name. Is not empty.
           t_attfilename :: filename    % The filename part in an "attachment:..." link target.
       )
   ;
       t_dangling_wikipage(             % Dangling internal link
           t_dwikipage :: P
       )
   ;
       t_dangling_attachment(           % Dangling reference to an attachment
           t_dattpage :: P,             % A complete, absolute Moin page name. Is not empty.
           t_dattfilename :: filename   % The filename part in an "attachment:..." link target.
       ).


% An inst for a target that's a straight or dangling wikipage.
:- inst wikipage ---> t_wikipage(ground) ; t_dangling_wikipage(ground).

% An inst for a target that's external or a straight attachment.
:- inst extatt   ---> t_external(ground) ; t_attachment(ground, ground).


% Determine if the target of a link refers to an external address (meaning it's an URL). At present,
% it's just checked if the link target begins with "http://" or "https://".

:- pred is_external_link(
    string::in)    % Link target
is semidet.



% Test a page for the presence of specific, explicit anchor (defined by "<<Anchor(...)>>" in MoinMoin). This will
% succeed when an explicit anchor of the specified name exists in the page.

:- pred explicit_anchor_included(
    string::in,         % Name of the anchor
    page(P)::in)        % Page to search
is semidet.



% Test a page for the presence of an anchor generated from a heading. This will succeed when there's a heading with
% the same name as the supplied anchor.

:- pred heading_anchor_included(
    string::in,         % Name of the anchor. That's the heading itself.
    page(P)::in)        % Page to search
is semidet.



%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module solutions, pair.
:- import_module parsertools, unicode, postprocess.


%================================================================================
% Headings

:- pred heading({int, string}::out, list(char)::in, list(char)::out) is nondet.

heading({Level, Txt}) -->
      ( str("= "), repeat(nonnl, TxtL), str(" ="), eol,
        { Level = 1, from_char_list(TxtL, Txt) } )
    ; ( str("== "), repeat(nonnl, TxtL), str(" =="), eol,
        { Level = 2, from_char_list(TxtL, Txt) } )
    ; ( str("=== "), repeat(nonnl, TxtL), str(" ==="), eol,
        { Level = 3, from_char_list(TxtL, Txt) } )
    ; ( str("==== "), repeat(nonnl, TxtL), str(" ===="), eol,
        { Level = 4, from_char_list(TxtL, Txt) } )
    ; ( str("===== "), repeat(nonnl, TxtL), str(" ====="), eol,
        { Level = 5, from_char_list(TxtL, Txt) } ).



%================================================================================
% Comments
%
% Comments are lines which begin with two "#". Comments divide paragraphs (like empty lines). They
% can't occur in code blocks (aren't interpreted as comments there).

:- pred comment_block(list(string)::out, list(char)::in, list(char)::out) is nondet.
:- pred comment_line(string::out, list(char)::in, list(char)::out) is nondet.

comment_line(Txt) -->
    str("##"),
    maybe(space),
    line(Txt).

comment_block(Lines) -->
    repeatmax(comment_line, Lines).



%================================================================================
% Code blocks

:- pred code_block(
    code_block::out,
    list(char)::in, list(char)::out)
is nondet.


% For instance: "{{{#!highlight shell numbers=disable"
% See http://moinmo.in/HelpOnParsers

:- pred code_block_beginning(
    int::out,           % Indentation
    maybe(              % In case there is syntax highlighting,
        { string,       %   Name of the hightlighing syntax (e.g. "shell")
          bool }        %   If line numbers are to be generated
    )::out,
    list(char)::in, list(char)::out)
   is semidet.

code_block_beginning(Indentation, MaybeHighlighting) -->
    count(space, Indentation),
    str("{{{"),
    ( if   str("#!highlight ")
      then {MaybeHighlighting = yes({Syntax, Linenumbers}) },
           spaces0,
           alnumstr(Syntax),
           spaces0,
           ( if      str("numbers=disable")
             then    { Linenumbers = no }
             else if str("numbers=enable")
             then    { Linenumbers = yes }
             else    { Linenumbers = yes }
           )
      else { MaybeHighlighting = no }
    ),
    eol.

code_block(code_block(Indentation, Lines, MaybeHighlighting)) -->
    code_block_beginning(Indentation, MaybeHighlighting),
    repeatmax0(code_block_0, Lines),
    str("}}}"),
    eol.

:- pred code_block_0(string::out, list(char)::in, list(char)::out) is nondet.

code_block_0(Line) -->
    line(Line),
    { Line \= "}}}" }.


% An uninterpreted line of input, such as inside a code block. The terminating newline character is
% consumed, but not returned, if there is any.

:- pred line(string::out,                       % the line
             list(char)::in, list(char)::out)
   is nondet.

line(Line) -->
    ( if   end
      then { fail }
      else repeatmax0(nonnl, LineL),
           ( end ; nl ),
           { from_char_list(LineL, Line) }
    ).



%================================================================================
% Text paragraphs
%
% A text paragraph. It consists of all the text lines with the same indentation. It is terminated by
% a different type of paragraph, by a text line with a different indentation and by empty lines.

:- pred text({int, list(inline(raw))}::out,       % Indentation and Lines
             list(char)::in, list(char)::out)
   is semidet.

% One line of text (inlines). Consumes at the end spaces (if any) and the newline character (if
% any, and not at the end of the input). Succeeds only when there is anything in the line.

:- pred textline(int, list(inline(raw)), list(char), list(char)).
:- mode textline(out, out, in, out) is semidet.
:- mode textline(in,  out, in, out) is semidet.

textline(Ind, Inlines) -->
    ( if
          (   comment_line(_)
          ;   table_row(_,_)
          ;   empty_line
          ;   heading(_)
          ;   code_block_beginning(_,_)
          ;   list_item(_)
          ;   def_list_item(_)
          )
      then { fail }
      else count(space, Ind),
           repeatmax(inline, Inlines),
           spaces0,
           eol
    ).

text({Ind, Inlines}) -->
    textline(Ind, Inlines0),
    repeatmax0((pred(L::out, in, out) is semidet -->
                    textline(Ind, L)),
               Inlines1),
    { Inlines = condense(intersperse([space], [Inlines0|Inlines1])) }.



%================================================================================
% Empty line.
%
% Spaces only don't count as constituting a non-empty line.

:- pred empty_line(list(char)::in, list(char)::out) is semidet.

empty_line -->
    ( if   end
      then { fail }
      else spaces0,
           eol
    ).


%================================================================================
% Paragraphs
%
% This are the high level kind of things in the parser, just under a page.

% A text paragraph is a catch-all for things that aren't of any specific paragraph type. For
% instance, specific types of paragraphs with faulty syntax become text paragraphs.
%
% An "empty_line" stands for one or more empty lines in the input.

:- pred paragraph(paragraph(raw)::out,                       % parsed paragraphis (see above)
                  list(char)::in, list(char)::out)
   is nondet.

paragraph(Paragraph) -->
    (
        if      repeatmax(empty_line)
        then    { Paragraph = empty_line }

        else if heading({Level, Txt})
        then    { Paragraph = heading(Level, Txt) }

        else if code_block(Codeblock)
        then    { Paragraph = code_block(Codeblock) }

        else if comment_block(Lines)
        then    { Paragraph = comment_block(Lines) }

        else if list_item({Ind, Type, Inlines})
        then    { Paragraph = list_item(Ind, Type, Inlines) }

        else if def_list_item({Ind, Txt, Inlines})
        then    { Paragraph = def_list_item(Ind, Txt, Inlines) }

        else if horizontal_rule(Ind, Width)
        then    { Paragraph = horizontal_rule(Ind, Width) }

        else if table({Ind, LLL})
        then    { Paragraph = table(Ind, LLL) }

        else    text({Indentation, Inlines}),
                { Paragraph = text(Indentation, Inlines) }
    ).



%================================================================================
% Pages
%
% A page is the entire input. It is the highest level thing.

page(Paragraphs) -->
    repeatmax0(empty_line),
    repeatmax0(paragraph, Paragraphs),
    repeatmax0(empty_line),
    end.


%================================================================================
% Macros
%
% A macro. That is enclosed in "<<...>>" in MoinMoin. There are three forms:
%
% - No arguments: "<<Name>>" or "<<Name()>>"  (for instance, "<<BR>>"). In this case, the
%   "macro_arguments" part of the "macro" data structure is "no".
% - A single string as an argument: <<Name(Str)>> (for instance, "<<Anchor(Str)>>". In this case,
%   the "macro_arguments" part of the "macro" data structure is "yes(left(Str))".
% - A list of key-value pairs as arguments: <<Name(Key1=Val1, Key2=Val2...)>> (for instance,
%   "<<ListPages(...)>>". In this case, the "macro_arguments" part of the "macro" data structure is
%   "yes(right(Args))".
%
% The Value of a macro argument can be enclosed in quotes and contain any text. Or it is without
% quotes, then it is a string of alphanumeric characters. In the first case, quotes inside the
% argument value must be quoted with "\".
%
% A macro can't span multiple lines in MoinMoin.
%
% Anchor names are case sensitive.

:- pred macro(macro::out,
              list(char)::in, list(char)::out)
   is semidet.

macro(macro(Name, Args)) -->
    str("<<"),
    macro_name(Name),
    (
        if        [ '(' ], spaces0, [ ')' ]
        then      { Args = no }

        else if   [ '(' ],
                  arguments_comma(Argl),
                  [ ')' ]

        then      { Args = yes(right(Argl)) }

        else if   [ '(' ],
                  macro_str_val(Value),
                  [ ')' ]

        then      { Args = yes(left(Value)) }

        else      { Args = no }
    ),
    str(">>").


% Name of a macro. Macro names are case senstitive in MoinMoin. Don't know if a macro name can
% contain an underscore. Allowing it here.

:- pred macro_name(string::out,
                   list(char)::in, list(char)::out) is semidet.

macro_name(Name) -->
    alnumstr_(Name).



:- pred macro_str_val(string::out,
                  list(char)::in, list(char)::out) is det.

macro_str_val(Value) -->
    spaces0,
    (
        if   [ '"' ],
             repeatmax0(quoted_arg_value_char, Valuechars),
             [ '"' ]
        then { Value = from_char_list(Valuechars) }
        else repeatmax0(macro_str_val_char, Valuechars),
             { Value = from_char_list(tools.trim(Valuechars)) }
    ),
            spaces0.


% A character inside a non-quoted macro string argument (NO key-value par)

:- pred macro_str_val_char(char::out, list(char)::in, list(char)::out) is semidet.

macro_str_val_char(Char) -->
   (
       if   ['\n']
       then { fail }

       else if [')']
       then { fail }

       else [ Char ]
   ).


% Name of an argument of a macro, a link or an embedment. Can begin with "&". (See
% https://moinmo.in/HelpOnLinking)

:- pred arg_name(string::out,
                 list(char)::in, list(char)::out) is semidet.

arg_name(Name) -->
    ( if   [ '&' ]
      then alnumstr_(Name0),
           { Name = "&" ++ Name0 }
      else alnumstr_(Name)
    ).



% Value of an argument of a macro, may be quoted (then it can contain spaces) or not (letters only).

:- pred arg_value(string::out,
                  list(char)::in, list(char)::out) is semidet.

arg_value(Value) -->
    (
        if   [ '"' ]
        then repeatmax0(quoted_arg_value_char, Valuechars),
             [ '"' ],
             { from_char_list(Valuechars, Value) }
        else alnumstr_(Value)
    ).


% One character in an argument value. Double quotes can be escaped.
:- pred quoted_arg_value_char(char::out,
                              list(char)::in, list(char)::out) is semidet.

quoted_arg_value_char(Char) -->
   (
       if   ['\\', '"']
       then { Char = '"' }

       else if ['\n']
       then { fail }

       else if ['"']
       then { fail }

       else [ Char ]
   ).


% One argument of a macro. That's "NAME = VALUE". There may be spaces around NAME and VALUE.

:- pred argument(pair(string, string)::out,
                 list(char)::in, list(char)::out) is semidet.

argument(Name - Value) -->
    spaces0,
    arg_name(Name),
    spaces0,
    [ '=' ],
    spaces0,
    arg_value(Value).


% Arguments of a link. The arguments are separated by spaces.

:- pred arguments_space(
    arguments::out,
    list(char)::in, list(char)::out)
is det.

arguments_space(Argl) -->
    repeatmax0(argument, Argl),
    spaces0.


:- pred arguments_comma(
    arguments::out,
    list(char)::in, list(char)::out)
is semidet.

arguments_comma(Argl) -->
    (
        if   argument(Arg)
        then (
               if   spaces0, [ ',' ]
               then arguments_comma(Argl1),
                    { Argl = [Arg|Argl1] }
               else spaces0,
                    { Argl = [Arg] }
             )
        else spaces0,
             { Argl = [] }
    ).


%================================================================================
% Words
%
% Words are inline elements. They can occur where there are inlines allowed: in lists, enumerated
% lists, text paragraphs, and inside tables. A word is a "catch all" for inlines which aren't of any
% specific type. See the type "inline". This means that markup which isn't understood or which is
% faulty, ends up as words.
%
% There are two flavors of words: words and cell words. The difference is, that a word is terminated
% by an inline thing (other than a word) and by whitespace. A cell word occurs inside tables (inside
% the cells). It is terminated like a normal word, and additionally by the table cell separator
% "||". "||" strings aren't allowed inside tables.
%
% A word (or cell word) consists of non-space characters only, and of at least one of them.
%
% We translate non-negated CamelCase words to regular links and negated CamelCase words to ordinary
% words.


% A word.

:- pred word(inline(raw)::out, list(char)::in, list(char)::out) is semidet.

word(Inline) -->
    ( if camelcase(Neg, Num, Word)

      then
          { CamelCase = tools.repeat("../", Num) ++ Word },
          (
              if   { Neg = yes }
              then { Inline = word(CamelCase) }
              else { Inline = link(link(raw(CamelCase, no), no, no, [])) }
          )

      else word_0(WordL),
           ( if    { WordL = [] }
             then  { fail }
             else  { from_char_list(WordL, Word),
                     Inline = word(Word)
                   }
           )
    ).


:- pred word_0(list(char)::out, list(char)::in, list(char)::out) is semidet.

word_0(Chars) -->
    (
      if    end
      then  { Chars = [] }
      else
            if   not inline_thing(_),
                 not whitespace

            then [ Char ],
                 word_0(Chars1),
                 { Chars = [Char|Chars1] }

            else { Chars = [] }
    ).


% A cell word (see above, at "Words")

:- pred cell_word(inline(raw)::out, list(char)::in, list(char)::out) is semidet.

cell_word(Inline) -->
    ( if camelcase(Neg, Num, Word)

      then
          { CamelCase = tools.repeat("../", Num) ++ Word },
          (
              if   { Neg = yes }
              then { Inline = word(CamelCase) }
              else { Inline = link(link(raw(CamelCase, no), no, no, [])) }
          )

      else cell_word_0(WordL),
           ( if    { WordL = [] }
             then  { fail }
             else  { from_char_list(WordL, Word),
                     Inline = word(Word)
                   }
           )
    ).


:- pred cell_word_0(list(char)::out, list(char)::in, list(char)::out) is semidet.

cell_word_0(WordL) -->
    (
      if    end
      then  { WordL = [] }
      else
            if   not inline_thing(_),
                 not whitespace,
                 not str("||")

            then [ Letter ],
                 cell_word_0(WordL1),
                 { WordL = [Letter|WordL1] }

            else { WordL = [] }
    ).



%================================================================================
% Camelcase links.
%
% This can be negated (turned off) with a leading "!". there can also be (several) leading "../"
% components. for instance, "../FooBar" refers to the sister page "FooBar".
%
% A camelcase word consist of parts. Each part consists of an uppercase letter, followed by one ore
% more lowercase letters. Digits count as lowercase letters.

:- pred camelcase(bool::out,                 % if it is negated with a leading "!"
                  int::out,                  % number of "../" components at the beginning
                  string::out,               % the actual camelcase word
                  list(char)::in, list(char)::out)
   is semidet.

camelcase(Neg, Num, Word) -->
    ( if [ '!' ]
      then { Neg = yes },
           camelcase_0(Num, Word)
      else { Neg = no },
           camelcase_0(Num, Word)
    ).


:- pred camelcase_0(int::out,                  % number of "../" components at the beginning
                    string::out,               % the actual camelcase word
                    list(char)::in, list(char)::out)
   is semidet.

camelcase_0(Num, Word) -->
    count(str("../"), Num),
    camelcase_body(Chars),
    not alnumchar(_),
    { Chars \= [],
      from_char_list(Chars, Word)
    }.


:- pred camelcase_body(list(char)::out, list(char)::in, list(char)::out) is semidet.

camelcase_body(Chars) -->
    camelcase_body_0(Chars1),
    repeatmax(camelcase_body_0, CharLL),
    { Chars1 \= [],
      CharLL \= [],
      Chars2 = list.condense(CharLL),
      Chars = Chars1 ++ Chars2
    }.


:- pred camelcase_body_0(list(char)::out, list(char)::in, list(char)::out) is semidet.

camelcase_body_0(Chars) -->
    ( if   [Ch],
           { glib_isupper(Ch) },
           repeatmax((pred(Char::out, In::in, Out::out) is semidet :-
                          In = [Char|Out],
                          ( glib_islower(Char) ; is_digit(Char) )),
                     Chars1)
      then { Chars = [Ch|Chars1] }
      else { fail }
    ).



%================================================================================
% Inline code
%
% This has the form "{{{...}}}" or "`...`". Must all be in one line. In inline code, no wiki syntax
% is recognised.

:- pred inline_code(string::out, list(char)::in, list(char)::out)
   is semidet.

inline_code(Code) -->
    (
        if       str("{{{")
        then     inline_code_1("}}}", Chars)
        else if  str("`")
        then     inline_code_1("`", Chars)
        else     { fail }
    ),
    { from_char_list(Chars, Code) }.


:- pred inline_code_1(string::in, list(char)::out, list(char)::in, list(char)::out) is semidet.

inline_code_1(End, Chars) -->
    ( if   str(End)
      then { Chars = [] }
      else [ Ch ],
           ( if   { Ch = '\n' }
             then { fail }
             else
                  inline_code_1(End, Chars1),
                  { Chars = [Ch|Chars1] }
           )
    ).



%================================================================================
% Inlines: Things that occur in continuous text
%
% Don't produce a "space" at the end of the line (or the table cell). This means, that spaces at the
% end aren't consumed, when calling "repeatmax(inline, Inlines)". They must be explicitly consumed
% by a "spaces0". And after that, usually a newline is to be parsed, with "eol".
%
% Inlines inside tables are a little different, in that they are terminated by "||", the cell
% separator.


% All things that come inside of continuous text, except for words.
:- pred inline_thing(inline(raw)::out,
                     list(char)::in, list(char)::out)
   is semidet.

inline_thing(Inline) -->
    (
      if   inline_code(Code)
      then { Inline = inline_code(Code) }

      else if macro(Macro)
      then { Inline = macro(Macro) }

      else if link(Link)
      then { Inline = link(Link) }

      else if embed(Embed)
      then { Inline = embed(Embed) }

      else if str("'''")
      then { Inline = bold }

      else if str("''")
      then { Inline = italics }

      else if str("__")
      then { Inline = underline }

      else if str(",,")
      then { Inline = subscript }

      else if str("^")
      then { Inline = superscript }

      else if str("~+")
      then { Inline = begin_big }

      else if str("+~")
      then { Inline = end_big }

      else if str("~-")
      then { Inline = begin_small }

      else if str("-~")
      then { Inline = end_small }

      else if str("--(")
      then { Inline = begin_stroke }

      else if str(")--")
      then { Inline = end_stroke }

      else { fail }
    ).



% Anything inline. The first argument is either "word" or "cell_word" and choses one of the two, for
% text that isn't any other inline thing. Table cells require "cell_word", because they must respect
% the cell separator "||". All other cases use the "word" predicate.
:- pred inline_with_word(pred(inline(raw), list(char), list(char)),
                         inline(raw),
                         list(char), list(char)).
:- mode inline_with_word(pred(out, in, out) is semidet,
                         out,
                         in, out)
   is semidet.

inline_with_word(Wordpred, Inline) -->
    (
        if   spaces
        then ( if   peek(eol)
               then { fail }
               else { Inline = space }
             )
        else
             (
               if   inline_thing(Inline0)
               then { Inline = Inline0 }

               else if empty_line
               then { fail }

               else if end
               then { fail }

               else Wordpred(Word),
                    { Inline = Word }
             )
    ).


% Convenience predicate. See above.

:- pred inline(inline(raw)::out, list(char)::in, list(char)::out) is semidet.

inline(Inline) -->
    inline_with_word(word, Inline).


% Convenience predicate. See above.

:- pred cell_inline(inline(raw)::out, list(char)::in, list(char)::out) is semidet.

cell_inline(Inline) -->
    inline_with_word(cell_word, Inline).





% Make a semidet predicate, which maintains an accumulator, deterministic. In case the predicate, the resulting
% predicate will copy the accumulator over.

:- func makedet(
    (pred(T, Accu, Accu)::in(pred(in, in, out) is semidet)))
  = (pred(T, Accu, Accu)::out(pred(in, in, out) is det)).

makedet(Pred) =
   (pred(T::in, In::in, Out::out) is det :-
       (
           if   Pred(T, In, Out0)
           then Out = Out0
           else Out = In
       )
   ).



% Traverse one paragraph for inlines. The supplied predicate is applied to all inlines in the paragraph. It carries
% an accumulator, which will collect all the results of the threaded predicate.

:- pred traverse_paragraph_for_inlines(
    pred(inline(T), Accu, Accu)::in(pred(in, in, out) is semidet),
    paragraph(T)::in,
    Accu::in,
    Accu::out)
is det.

traverse_paragraph_for_inlines(
    Pred,
    Paragraph,
    AccuIn,
    AccuOut)
:-
    (
        Paragraph = heading(_, _),
        AccuOut = AccuIn
    ;
        Paragraph = code_block(_),
        AccuOut = AccuIn
    ;
        Paragraph = text(_, Inlines),
        foldl(makedet(Pred), Inlines, AccuIn, AccuOut)
    ;
        Paragraph = comment_block(_),
        AccuOut = AccuIn
    ;
        Paragraph = empty_line,
        AccuOut = AccuIn
    ;
        Paragraph = list_item(_, _, Inlines),
        foldl(makedet(Pred), Inlines, AccuIn, AccuOut)
    ;
        Paragraph = def_list_item(_, _, Inlines),
        foldl(makedet(Pred), Inlines, AccuIn, AccuOut)
    ;
        Paragraph = horizontal_rule(_, _),
        AccuOut = AccuIn
    ;
        Paragraph = table(_, LLL),
        FoldL = foldl(makedet(Pred)),
        FoldLL = foldl(FoldL),
        FoldLLL = foldl(FoldLL),
        FoldLLL(LLL, AccuIn, AccuOut)
    ).



% Inspect a macro for being an anchor ("<<Anchor(...)>>" in MoinMoin). If so, add the anchor name to the handed
% over list of anchor names.

:- pred anchor(inline(P)::in, list(string)::in, list(string)::out) is semidet.

anchor(
    macro(macro("Anchor", yes(left(AName)))),
    AnchorsIn,
    [AName | AnchorsIn]).



% Test for the presence of an explicit anchor (defined by "<<Anchor(...)>>" in MoinMoin) in a page.

explicit_anchor_included(Anchor, Page) :-
    foldl(traverse_paragraph_for_inlines(anchor), Page, [], Anchors),
    member(Anchor, Anchors).



% Test for the presence of an anchor generated from a heading.

heading_anchor_included(Anchor, Page) :-
    any_true(
        (pred(heading(_, Heading)::in) is semidet :-
            Heading = Anchor),
        Page).




%================================================================================
% Hypertext links, enclosed in "[[...]]"
% See https://moinmo.in/HelpOnLinking
%
% Links can be quite complicated. The link target may either be an inner-wiki-address, or an
% external (http) address. After a "|", there can follow a descriptive text which is displayed
% instead of the link target. After the "|", there also may be an embedded object, in "{{...}}".
% Here you can specify a clickable icon. The link target (when an intra-wiki-address) and an
% embedded object can be an attachment. Then "attachment:" is prefixed. Last but not least, a link
% may also have a second "|" and some arguments afters. They have the same form as in a macro call
% ("Name=Value ..."), except for the delimiter is a space and not a comma.
%
%

% A hypertext link. See above.

:- pred link(link(raw)::out, list(char)::in, list(char)::out) is semidet.

link(link(raw(Target, IsAtt), MaybeSubsection, MaybeDesc, Argl)) -->
    str("[["),
    ( if str("attachment:")
         then { IsAtt = yes }
         else { IsAtt = no }
    ),

    % Main section
    repeatmax0(link_0, TargetD),
    { Target = from_char_list(TargetD) },

    % Subsection
    ( if   { IsAtt = no },
           str("#")
      then ( if   repeatmax(link_3, SubsD)
             then { MaybeSubsection = yes(from_char_list(SubsD)) }
             else { MaybeSubsection = no }
           )
      else { MaybeSubsection = no }
    ),

    % Descriptive text or an embedded object.
    ( if   ['|']
      then ( if   spaces0, embed(Embed), spaces0
             then { MaybeDesc = yes(right(Embed)) }
             else repeatmax(link_1, DescL),
                  { MaybeDesc = yes(left(from_char_list(DescL))) }
           )
      else { MaybeDesc = no }
    ),

    ( if   ['|']
      then arguments_space(Argl)
      else { Argl = [] }
    ),

    str("]]").


% Helpers for link/6.

:- pred link_0(char::out, list(char)::in, list(char)::out) is semidet.
:- pred link_1(char::out, list(char)::in, list(char)::out) is semidet.
:- pred link_2(char::out, list(char)::in, list(char)::out) is semidet.
:- pred link_3(char::out, list(char)::in, list(char)::out) is semidet.

% Inside the main part of the link, before the optional subsection
link_0(Char) -->
    not ['|'],
    not ['#'],
    not eol,
    not str("]]"),
    [ Char ].

link_1(Char) -->
    not ['|'],
    not str("]]"),
    [ Char ].

link_2(Char) -->
    not ['|'],
    not str("}}"),
    [ Char ].

% Inside the optional subsection of the link
link_3(Char) -->
    not ['|'],
    not eol,
    not str("]]"),
    [ Char ].



%================================================================================
% Embedded objects
% See https://moinmo.in/HelpOnLinking
%
% This is like a hypertext link, except for there can't be an embedded object inside the embedded
% object. Embedded objects can't have a subsection.


% An embedded object

:- pred embed(embed(raw)::out, list(char)::in, list(char)::out)
   is semidet.

embed(embed(raw(Target, IsAtt), MDesc, Argl)) -->
    str("{{"),
    ( if str("attachment:")
         then { IsAtt = yes }
         else { IsAtt = no }
    ),
    repeatmax(embed_0, TargetD),
    { Target = from_char_list(TargetD) },


    % Descriptive text
    ( if   ['|']
      then repeatmax0(embed_1, DescL),
           { MDesc = yes(from_char_list(DescL)) }
      else { MDesc = no }
    ),

    % Contrary to what the HelpOnLinking MoinMoin page says, embed parameters must be separated by
    % commas, not spaces. This holds for independent embedments as well as for embedments inside a
    % link.
    ( if   ['|']
      then arguments_comma(Argl)
      else { Argl = [] }
    ),

    str("}}").


% Helpers for embed/6

:- pred embed_0(char::out, list(char)::in, list(char)::out) is semidet.
:- pred embed_1(char::out, list(char)::in, list(char)::out) is semidet.

embed_0(Char) -->
    not ['|'],
    not eol,
    not str("}}"),
    [ Char ].

embed_1(Char) -->
    not ['|'],
    not str("}}"),
    [ Char ].



%================================================================================
% Lists (except for definition lists)
% See https://moinmo.in/HelpOnLists
%
% Normal (non-definition) lists differ only in the style (bullets, numbers...). They have an
% indentation of at least 1 (otherwise they aren't recognized as list items).
%
% The parser only parses list items. It doesn't merge them to a list. This requires postprocessing
% first, because of the habit of list items to swallow any subsequent text paragraphs with the same
% indentation level.
%
% A list item consists of the beginning (for instance, " * " or " 1. ") and a list item body. The
% body is a sequence of inlines. Any inlines are allowed. The body may consist of several subsequent
% lines, all with the same indentation as the start line.


% A list item of various styles.
:- pred list_item({ int,             % Indentation, starts at 1 for list items
                    list_type,       % Type/style of the list item (bullet, numbers, ...)
                    list(inline(raw))     % The contents of the list item
                  }::out,
                  list(char)::in, list(char)::out) is nondet.

list_item({Ind, Type, Inlines}) -->
    count(space, Ind),
    ( str("* "),                      { Type = bullet }
    ; str(". "),                      { Type = dot }
    ; repeatmax(isdigit), str(". "),  { Type = number }
    ; str("A. "),                     { Type = uppercase_letter }
    ; str("a. "),                     { Type = lowercase_letter }
    ; str("I. "),                     { Type = uppercase_roman }
    ; str("i. "),                     { Type = lowercase_roman }
    ),
    repeatmax0(inline, Inlines0),
    spaces0,
    eol,
    repeatmax0((pred(Inl::out, X::in, Y::out) is nondet :-
                    textline(Ind, Inl, X, Y)),
               Inlines1),
    { Inlines = condense(intersperse([space], [Inlines0|Inlines1])) }.


:- pred isdigit(list(char)::in, list(char)::out) is semidet.

isdigit -->
    [ Ch ],
    { is_digit(Ch) }.



%================================================================================
% Definition lists
% See https://moinmo.in/HelpOnLists
%
% Definition list items have an indentation of at least 1 (otherwise they aren't recognized as
% definition list items).
%
% The parser only parses definition list items. It doesn't merge them to a definition list. This
% requires postprocessing first, because of the habit of definition list items to swallow any
% subsequent text paragraphs with the same indentation level.
%
% Definition list items are different from ordinary list (see above), in that they have a head (the
% word or phrase being defined) and a body (the definition). The head is only text, not a inlines
% sequence. No text formatting is allowed inside the head. The body is a sequence of inlines. Any
% inlines are allowed. The body may consist of several subsequent lines, all with the same
% indentation as the start line.
%
% Definition list items can be continued with the " :: ..." syntax. This means that the head is the
% empty string. Such continued text goes to a new line.


% A definition list item

:- pred def_list_item(
        { int,                  % Indentation, starts at 1 for list items
          string,               % Defined word(s)
          list(inline(raw))     % The definition/description
        }::out,
        list(char)::in, list(char)::out)
   is semidet.

def_list_item({Ind, DefWord, Inlines}) -->
    def_list_item_beginning(Ind, DefWord),
    def_list_item_text(Ind, Inlines).


:- pred def_list_item_beginning(
        int::out,                  % Indentation, starts at 1 for list items
        string::out,               % Defined word(s)
        list(char)::in, list(char)::out)
   is semidet.

def_list_item_beginning(Ind, DefWord) -->
    count(space, Ind),
    { Ind > 0 },
    repeatmax(def_letter, DefLetters),
    { DefWord = from_char_list(tools.trim(DefLetters)) },
    ( if   str("::"), peek(eol)
      then { true }
      else str("::"), peek(space)
    ).


:- pred def_list_item_text(
    int::in,                              % Indentation
    list(inline(raw))::out,               % The definition/description
    list(char)::in, list(char)::out)
is semidet.

def_list_item_text(Ind, Inlines) -->
    (
        if   spaces0, eol
        then % Nothing in the same line as the defined word
             repeatmax0(def_item_line(Ind),
                        Inlines1),
             { Inlines = condense(intersperse([space], Inlines1)) }

        else % Something in the same line as the defined word
             spaces,
             repeatmax0(inline, Inlines0),
             spaces0,
             eol,
             repeatmax0(def_item_line(Ind),
                        Inlines1),
             { Inlines = condense(intersperse([space], [Inlines0|Inlines1])) }
    ).


:- pred def_item_line(int, list(inline(raw)), list(char), list(char)).
:- mode def_item_line(in,  out, in, out) is semidet.

def_item_line(Ind, Inlines) -->
    (
      if      count(space, Ind),
              str("::"),
              eol
      then    { Inlines = [ macro(macro("BR",no)) ] }

      else if count(space, Ind),
              str(":: ")
      then    repeatmax0(inline, Inlines0),
              spaces0,
              eol,
              { Inlines = [ macro(macro("BR",no)) | Inlines0 ] }

      else if def_list_item_beginning(_, _)
      then    { fail }

      else    count(space, Ind),
              repeatmax0(inline, Inlines),
              spaces0,
              eol
    ).


% A letter in the head part of a definition list item

:- pred def_letter(char::out,
                   list(char)::in, list(char)::out)
   is semidet.

def_letter(Char) -->
    ( if ( str("::")
         ; eol
         )
      then { fail }
      else [ Char ]
    ).



%================================================================================
% Horizontal rules
%
% Horizontal rules have an indentation. It has no effect on how the rule is rendered, but it affects
% the indentation of following paragraphs.

:- pred horizontal_rule(int::out,       % Indentation
                        int::out,       % Width
                        list(char)::in, list(char)::out)
   is semidet.

horizontal_rule(Ind, Width) -->
   count(space, Ind),
   count((pred(In::in, Out::out) is semidet :-
              In = ['-'|Out]),
              Cnt),
   { Cnt >= 4,
     ( if Cnt > 10 then Width = 10 else Width = Cnt )
   },
   spaces0,
   eol.



%================================================================================
% Tables
% See https://moinmo.in/HelpOnTables
%
% A table is a list of table rows, which are a list of table cells, which are a list of inlines. No
% effort is made to ensure that all rows have the same number of cells.
%
% A table has an indentation, but it can't occur inside continuous text or inside other tables. It
% is a the paragraph level.
%
% A table has at least one row. Each row is on one line and can't span multiple lines. Other than
% list items, the rows of a table are merged to a "table" object (see the type "paragraph").
%
% At the time being (2021-04-30), only simple tables are supported. Table attributes aren't
% recognised. They are treated as cell content.

% A table
:- pred table({int,                             % Indentation
               list(list(list(inline(raw))))}::out,  % The contents of the table
              list(char)::in, list(char)::out)
   is nondet.

table({Ind, LLL}) -->
    table_row(Ind, LL0),
    repeatmax0((pred(LL::out, X::in, Y::out) is nondet :-
                    table_row(Ind, LL, X, Y)),
               LLL0),
    { LLL = [LL0|LLL0] }.


% A table row consist of at least one table cell. table_row consumes one line.
:- pred table_row(int,                             % Indentation
                  list(list(inline(raw))),              % The contents of the row
                  list(char), list(char)).
:- mode table_row(in, out, in, out) is semidet.
:- mode table_row(out, out, in, out) is semidet.

table_row(Ind, LL) -->
    count(space, Ind),
    str("||"),
    repeatmax(table_cell, LL),
    % Spaces are NOT allowed at the end of a table row line.
    eol.


% A table cell is between "||...||". It can be empty.
:- pred table_cell(list(inline(raw))::out,
                   list(char)::in, list(char)::out)
   is semidet.

table_cell(Inlines) -->
    spaces0,
    repeatmax0(cell_inline, Inlines),
    spaces0,
    str("||").



%--------------------------------------------------------------------------------
% Test if a link is an external link. This checks if the given link target begins with a protocol part, such as
% "https://". The protocol doesn't matter, all non-empty sequences of alpha characters (as defined by
% char.is_alpha/1) are accepted.


is_external_link(Target) :-
    is_external_link(string.to_char_list(Target), _).

        

:- pred is_external_link(list(char)::in, list(char)::out) is semidet.

is_external_link -->
    alphas1,
    [ ':', '/', '/' ].



:- pred alphas1(list(char)::in, list(char)::out) is semidet.

alphas1 -->
   (   if   [ Chr ]
       then { char.is_alpha(Chr) },
            alphas0
       else { true }
   ).



:- pred alphas0(list(char)::in, list(char)::out) is semidet.

alphas0 -->
    ( if   [ Chr ],
           { char.is_alpha(Chr) }
      then alphas0
      else { true }
    ).
