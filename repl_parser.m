%================================================================================
% Parser for the string replacements in page names file
%
% Page names are more restricted in Tiki than in MoinMoin. Therefore, some characters or charater
% combinations in MoinMoin page names must be replaced with different strings in the Tiki page
% names. The replacements which are to be done are specified in the replacements file. This module
% provides the parser for this file.
%
% Replacement rules are specified as "From -> To", one on each line (except for empty lines, which
% are ignored). "From" and "To" are quoted strings. Quotation marks can be included by quoting them
% with a leading backslash.
%
% Any left over characters, which aren't allowed in Tiki page names, are replaced by "_" after all
% the rules have been applied. The replecements file may be empty.

:- module repl_parser.

%================================================================================
% Interface
%================================================================================
:- interface.
:- import_module assoc_list.


% Parse the contents of the replacements file. This will fail on a parse error.
:- pred parse_repl_file(
       string::in,                      % The contents of the replacements file
       assoc_list(string,string)::out)  % The replacements
   is semidet.
   


%================================================================================
% Implementation
%================================================================================
:- implementation.
:- import_module string, char, list, pair.
:- import_module parsertools.



parse_repl_file(Cnt, Repl) :-
    string.to_char_list(Cnt, Chars),
    repl_rules(ReplChr, Chars, []),
    list.map((pred((FromChr - ToChr)::in, (FromStr - ToStr)::out) is det :-
                  string.from_char_list(FromChr, FromStr),
                  string.from_char_list(ToChr, ToStr)),
             ReplChr,
             Repl).


:- pred repl_rules(
       assoc_list(list(char), list(char))::out,           % All the replacement rules
       list(char)::in, list(char)::out)
   is det.

repl_rules(Repl) -->
    repeatmax0(repl_rule, Repl).


:- pred repl_rule(
       pair(list(char), list(char))::out,                 % One replacement rule
       list(char)::in, list(char)::out)
   is semidet.

repl_rule((From - To)) -->
    spaces0,
    repl_str(From),
    spaces0,
    ['-', '>'],
    spaces0,
    repl_str(To),
    spaces0,
    eol.


:- pred repl_str(list(char)::out,
                 list(char)::in, list(char)::out) 
   is semidet.

repl_str(Chars) -->
    (
        [ '"' ],
        repeatmax0(quoted_repl_str_char, Chars),
        [ '"' ]
    ).


:- pred quoted_repl_str_char(char::out,
                              list(char)::in, list(char)::out) 
   is semidet.

quoted_repl_str_char(Char) -->
   (
       if   ['\\', '"']
       then { Char = '"' }

       else if ['"'] 
       then { fail }

       else if ['\n']
       then { fail }
   
       else [ Char ]
   ).

