#! /bin/bash

# This script assumes that everything (for instance the "moin2tiki" program) is in 
# the directory which the script is in.

cd "`dirname $0`"

sudo mkdir -p /tmp/moin2tiki
sudo cp -f moin2tiki moin2tiki.sh pnrepl /tmp/moin2tiki
sudo touch /tmp/moin2tiki/ignore-list
sudo chmod a+x /tmp/moin2tiki/moin2tiki /tmp/moin2tiki/moin2tiki.sh
sudo chown -R www-data:www-data /tmp/moin2tiki


# Saving moin-import.php and moin.css in the Tiki root directory:

#sudo cp moin-import.php /var/www/html/tiki/moin-import.php || exit 1
#sudo cp moin.css /var/www/html/tiki/moin.css || exit 1
#sudo chown -R www-data:www-data /var/www/html/tiki/moin-import.php /var/www/html/tiki/moin.css || exit 1
