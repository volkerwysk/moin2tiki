% MoinMoin syntax is a mess. This module copes with MoinMoin bugs and weirdness.
:- module postprocess.

%================================================================================
% Interface
%================================================================================

:- interface.
:- import_module parser, moin2tiki.

% Apply all the postprocesses.
:- pred postprocess(m2t::in, page(raw)::in, page(raw)::out) is det.
%:- pred postprocess(m2t::in, page(raw)::in, page(target(moinpage))::out) is det.


%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module list, int, exception, maybe, transform, bool.

postprocess(M2T, In, Out) :-
    some [!P] (
        !:P = In,
        postprocess_swallow(!P),
        postprocess_styles(!P),
        postprocess_remove_empty_lines(!P),
        postprocess_dot_list_items(!P),
        postprocess_list_item_levels(!P),
        (
            if   M2T ^ m2t_params ^ p_adjust_hlevels = yes
            then postprocess_heading_levels(!P)
            else true
        ),
        Out = !.P
    ).



%================================================================================
% The swallow-following-paragraphs postprocess step
%================================================================================

% Indented text greedily swallows all following text paragraphs with the same indentation level,
% provided the indentation is greater than zero. Empty lines don't stop it. The same holds for list
% items and definition list items. They always have an indentation level greater than zero. They
% swallow all following text paragraphs with the same indentation (but not other (definition) list
% items).
:- pred postprocess_swallow(page(raw)::in, page(raw)::out) is det.


postprocess_swallow(ParaLIn, ParaLOut) :-
    ( ParaLIn  = [],
      ParaLOut = []
    ;
      ParaLIn = [In|ParaLInRest],
      (
        % Merge an indented paragraph with all following paragraphs of the same indentation.
        if In = text(Ind0, _),
           Ind0 > 0
        then
             swallow(Ind0, ParaLInRest, Matching, ParaLInRest1),
             condense_paragraphs([In|Matching], Inlines),
             ParaLOut = [text(Ind0, Inlines)|ParaLOutRest],
             postprocess_swallow(ParaLInRest1, ParaLOutRest)


        % Merge a list item with all following text paragraphs of the same indentation.
        else if In = list_item(Ind0, Style, Inlines)  % Ind0 is always greater than zero.
        then
             swallow(Ind0, ParaLInRest, Matching, ParaLInRest1),
             ( Matching = [],
               ParaNew = list_item(Ind0, Style, Inlines)
             ;
               Matching = [_|_],
               ( if  ParaLInRest  = [empty_line|_]
                 then Space = [ space ]
                 else Space = [ ]
               ),
               condense_paragraphs(Matching, Inlines1),
               ParaNew = list_item(Ind0, Style, Inlines ++ Space ++ Inlines1)
             ),
             ParaLOut = [ParaNew|ParaLOutRest],
             postprocess_swallow(ParaLInRest1, ParaLOutRest)


        % Merge a definition list item with all following text paragraphs of the same indentation.
        else if In = def_list_item(Ind0, DefWord, Inlines)  % Ind0 is always greater than zero.
        then
             swallow(Ind0, ParaLInRest, Matching, ParaLInRest1),
             ( Matching = [],
               ParaNew = def_list_item(Ind0, DefWord, Inlines)
             ;
               Matching = [_|_],
               ( if  ParaLInRest = [empty_line|_],
                     Inlines \= []
                 then Space = [ space ]
                 else Space = [ ]
               ),
               condense_paragraphs(Matching, Inlines1),
               ParaNew = def_list_item(Ind0, DefWord, Inlines ++ Space ++ Inlines1)
             ),
             ParaLOut = [ParaNew|ParaLOutRest],
             postprocess_swallow(ParaLInRest1, ParaLOutRest)


        % Nothing to be done for the current element.
        else ParaLOut = [In|ParaLOut1],
             postprocess_swallow(ParaLInRest, ParaLOut1)
      )
    ).


% Take a list of text paragraphs with the same indentation and create the condensed list of all the
% inlines of all the the paragraphs. This is called with text paragraphs only and the same
% level of indentation.

:- pred condense_paragraphs(list(paragraph(raw))::in, list(inline(raw))::out) is det.

condense_paragraphs([], _) :-
    throw("Bug in condense_paragraphs: empty list").

condense_paragraphs([In|InL], Out) :-
    foldl((pred(Para1::in, Para2::in, Para3::out) is det :-
               ( if   Para1 = text(Ind, Inlines1),
                      Para2 = text(Ind, Inlines2)
                 then Para3 = text(Ind, Inlines2 ++ [space] ++ Inlines1)
                 else throw("Bug in condense_paragraphs: inconsistent paragraphs")
               )),
          InL,
          In,
          Out0),
    ( if   Out0 = text(_, Out1)
      then Out = Out1
      else throw("Bug in condense_paragraphs: unexpected paragraph type")
    ).


% Get the list of text paragraphs with the same indentation. Skip empty lines. But leave an empty
% line, in front of the fist non-matching paragraph, alone.

:- pred swallow(int::in,                       % Indentation
                list(paragraph(raw))::in,      % Input list
                list(paragraph(raw))::out,     % Matching text paragraphs at the beginning
                list(paragraph(raw))::out)     % Rest of input list
   is det.

swallow(_, [], [], []).

swallow(Ind, Texts, Matching, Rest) :-
    Texts = [_|_],
    (
        if      Texts = [empty_line, text(Ind1, _)|_],
                Ind \= Ind1
        then    Matching = [],
                Rest = Texts

        else if Texts = [empty_line|Texts1]
        then    swallow(Ind, Texts1, Matching, Rest)

        else if Texts = [Text@text(Ind1, _Inlines)|TextsRest]
        then    ( if   Ind1 = Ind
                  then Matching = [Text|MatchingRest],
                       swallow(Ind, TextsRest, MatchingRest, Rest)
                  else Matching = [],
                       Rest = Texts
                )
        else Matching = [],
             Rest = Texts
    ).



%================================================================================
% The step of translating text styles to Tiki
%================================================================================

% This copes with text styles - such as bold, italics, underline, typewriter... Both MoinMoin and
% Tiki have some styles, for which the beginning and the end token are the same, and some for which
% they are different. In the case that the Tiki beginning and the end tokens are different, whereas
% they are the same in MoinMoin, the MoinMoin token must be converted to Tiki tokens. To achieve
% this, the use of the MoinMoin token must be tracked, such that each occurrence of the MoinMoin
% token can be translated to either a beginning or an end token in Tiki.
%
% The following MoinMoin tokens, with same for beginning and end can occur (see the "inline" type in
% the parser module). The inline code blocks have different beginning and end tokens.
%
%               MoinMoin        Tiki
%               --------        ----
% italics       ''              ''
% bold          '''             __
% underline     __              ===
% subscript     ,,              {SUB()}...{SUB}
% superscript   ^               {SUP()}...{SUP}
% typewriter    `                -+...+-  ?!?!?
% inline code   {{{...}}}       ?
%
% https://doc.tiki.org/Wiki-Syntax#Quick_reference_-_basic_text_formatting

:- pred postprocess_styles(page(raw)::in, page(raw)::out) is det.

postprocess_styles(ParaLIn, ParaLOut) :-
    ParaLOut = ParaLIn.



%================================================================================
% Remove empty lines from the parse result.
%
% Empty lines separate (indentation 0) paragraphs, but it *seems* like they don't have any effect on
% the output. I'm not removing them from the parser output until I'm sure. I'm removing them here
% instead.
%================================================================================

:- pred postprocess_remove_empty_lines(page(raw)::in, page(raw)::out) is det.

postprocess_remove_empty_lines(ParaLIn, ParaLOut) :-
    list.filter((pred(Para::in) is semidet :-
                     ( if   Para = empty_line
                       then false
                       else true
                     )),
                ParaLIn,
                ParaLOut).



%================================================================================
% Translate list items of the dot (".") type to paragraphs.
%
% List items being specified with a dot aren't rendered as list items, but as paragraphs. There is
% nothing that looks like a list item (like a bullet or a number). Instead, dots are used to specify
% paragraphs inside a list item. Real paragraphs can't be used, because they are swallowed by
% preceding list items (see postprocess_swallow, above).
%================================================================================

:- pred postprocess_dot_list_items(page(raw)::in, page(raw)::out) is det.

postprocess_dot_list_items(PageIn, PageOut) :-
    list.map((pred(ParaIn::in, ParaOut::out) is det :-
                  ( if   ParaIn = list_item(Ind, dot, Inlines)
                    then ParaOut = text(Ind, Inlines)
                    else ParaOut = ParaIn )),
             PageIn,
             PageOut).




%================================================================================
% Reduce the indentation level of list items by one.
%
% This is needed because MoinMoin and Tiki have a different idea about what the indentation of a
% list item is.
%================================================================================

:- pred postprocess_list_item_levels(page(raw)::in, page(raw)::out) is det.

postprocess_list_item_levels(PageIn, PageOut) :-
    list.map((pred(ParaIn::in, ParaOut::out) is det :-
                  ( if      ParaIn  = list_item(Ind, List_type, Inlines)
                    then    ParaOut = list_item(Ind - 1, List_type, Inlines)

                    else if ParaIn  = def_list_item(Ind, Word, Inlines)
                    then    ParaOut = def_list_item(Ind - 1, Word, Inlines)

                    else    ParaOut = ParaIn )),
             PageIn,
             PageOut).



%================================================================================
%================================================================================
:- pred postprocess_heading_levels(page(raw)::in, page(raw)::out) is det.

postprocess_heading_levels(PageIn, PageOut) :-
    min_hlevel(PageIn, Min),
    adjust_hlevels(Min, PageIn, PageOut).


:- pred min_hlevel(page(raw)::in, int::out) is det.

min_hlevel([], 1000).

min_hlevel([Para|Paras], Min) :-
    (
        if
            Para = heading(Level1,_)
        then
            min_hlevel(Paras, Level2),
            Min = int.min(Level1, Level2)
        else
            min_hlevel(Paras, Min)
    ).


:- pred adjust_hlevels(int::in, page(raw)::in, page(raw)::out) is det.

adjust_hlevels(_, [], []).

adjust_hlevels(Min, [ParaIn|ParasIn], [ParaOut|ParasOut]) :-
    (
        if
            ParaIn = heading(Level1, Txt)
        then
            ParaOut = heading(Level1 - Min + 1, Txt)
        else
            ParaOut = ParaIn
    ),
    adjust_hlevels(Min, ParasIn, ParasOut).
