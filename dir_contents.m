%================================================================================
% Reading directory contents
%================================================================================

:- module dir_contents.

%================================================================================
% Interface
%================================================================================

:- interface.
:- import_module io, string, list, posix.


% Get the contents of some directory. The list will be sorted, according to the standard ordering of
% the char type. This works badly with non-ASCII unicode characters. The "." and ".." entries are
% removed.
:- pred dir_contents(
       string::in,                        % Path of the directory
       posix.result(list(string))::out,   % List of entries in the directory, or posix error
       io::di, io::uo) is det.
 

% Like dir_contents, but don't return errors. Throw them as an exception instead.
:- pred dir_contents_ex(
       string::in,                      % Path of the directory
       list(string)::out,               % List of entries in the directory (without "." and "..")
       io::di, io::uo) is det.
 

%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module maybe.
:- import_module posix.opendir, posix.closedir, posix.readdir, tools.

:- pred dir_contents_1(posix.dir::in, posix.result(list(string))::out, io::di, io::uo) is det.
:- pred dir_contents_0(string::in, posix.result(list(string))::out, io::di, io::uo) is det.
 

dir_contents_ex(Pfad, List, !IO) :-
    posixerr2ex(Pfad, dir_contents(Pfad), List, !IO).


dir_contents(Pfad, Erg, !IO) :-
    dir_contents_0(Pfad, Erg0, !IO),
    ( if    Erg0 = posix.ok(L)
      then  filter((pred(Dn::in) is semidet :- Dn \= ".", Dn \= ".."), L, L1),
            Erg = posix.ok(L1)
      else  Erg = Erg0
    ).
    

dir_contents_0(Pfad, Erg, !IO) :-
    opendir(Pfad, Erg1, !IO),
    (
        Erg1 = posix.ok(Dir),
        dir_contents_1(Dir, Erg2, !IO),
        (
            Erg2 = posix.ok(Files),
            list.sort(Files, Files1),
            Erg = posix.ok(Files1)
        ;
            Erg2 = posix.error(_),
            Erg = Erg2
        ),
        closedir(Dir, !IO)
    ;
        Erg1 = posix.error(Err),
        Erg = posix.error(Err)
    ).

dir_contents_1(Dir, Akku, !IO) :-
    readdir(Dir, Erg, !IO),
    (
        % End of the stream
        Erg = eof,
        Akku = ok([])
    ;
        % Directry entry read
        Erg = ok(File),
        dir_contents_1(Dir, Akku1, !IO),
        (
            Akku1 = posix.error(Err),
            Akku = posix.error(Err)
        ;
            Akku1 = ok(Files),
            Akku = ok([File|Files])
        )
    ;
        % readdir reported error
        Erg = error(Err),
        Akku = error(Err)
    ).
