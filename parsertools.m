:- module parsertools.

:- interface.
:- import_module list, char, string, int.
:- import_module parser, moin2tiki, pages.


% Apply the specified parser, and get it's result, without consuming the parsed text.
:- pred peek(pred(T, list(char), list(char)), T, list(char), list(char)).
:- mode peek(pred(out, in, out) is det,     out, in, out) is det.
:- mode peek(pred(out, in, out) is semidet, out, in, out) is semidet.
:- mode peek(pred(out, in, out) is multi,   out, in, out) is multi.
:- mode peek(pred(out, in, out) is nondet,  out, in, out) is nondet.


% Apply the specified parser, which doesn't have any result, without consuming the parsed text.
:- pred peek(pred(list(char), list(char)), list(char), list(char)).
:- mode peek(pred(in, out) is det,     in, out) is det.
:- mode peek(pred(in, out) is semidet, in, out) is semidet.
:- mode peek(pred(in, out) is multi,   in, out) is det.
:- mode peek(pred(in, out) is nondet,  in, out) is semidet.


% The end of the input
:- pred end(list(char)::in, list(char)::out) is semidet.


% A newline character
:- pred nl(list(char)::in, list(char)::out) is semidet.


% A space character. A newline is *not* accepted.
:- pred space(list(char)::in, list(char)::out) is semidet.


% Zero to several space characters. Newlines are *not* accepted.
:- pred spaces0(list(char)::in, list(char)::out) is det.


% Several spaces (at least one). Newlines are *not* accepted.
:- pred spaces(list(char)::in, list(char)::out) is semidet.


% One digit
:- pred digit(char::out, list(char)::in, list(char)::out) is semidet.


% Several digits
:- pred digits(list(char)::out, list(char)::in, list(char)::out) is semidet.


% Not a newline character
:- pred nonnl(char::out, list(char)::in, list(char)::out) is semidet.


% A white space character
:- pred whitespace(list(char)::in, list(char)::out) is semidet.


% Not a white space character
:- pred nonwhitespace(char::out, list(char)::in, list(char)::out) is semidet.


% The end of the line. This consumes the '\n' character (or '\r','\n') 
:- pred eol(list(char)::in, list(char)::out) is semidet.


% A fixed string at the beginning of the text.
:- pred str(string::in, list(char)::in, list(char)::out) is semidet.


% A (non-empty) sequence of alphanumeric characters
:- pred alnumstr(string::out, list(char)::in, list(char)::out) is semidet.

% A (non-empty) sequence of alphanumeric characters and underscores
:- pred alnumstr_(string::out, list(char)::in, list(char)::out) is semidet.


% A (non-empty) sequence of alphanumeric characters
:- pred alnumchars(list(char)::out, list(char)::in, list(char)::out) is semidet.


% A single alphanumeric character
:- pred alnumchar(char::out, list(char)::in, list(char)::out) is semidet.


% Count the number of occurances of a thing (zero to more). Consumes the occurences which it found.
:- pred count(pred(list(char), list(char)),     % the thing
              int,                              % number of occurances
              list(char), list(char)).
:- mode count(pred(in, out) is det,     out, in, out) is det.
:- mode count(pred(in, out) is semidet, out, in, out) is semidet.
:- mode count(pred(in, out) is multi,   out, in, out) is multi.
:- mode count(pred(in, out) is nondet,  out, in, out) is nondet.


%--------------------------------------------------------------------------------
% Parser combinators


% Something repeated zero to several times, without any return value

:- pred repeat0(pred(list(char), list(char)), list(char), list(char)).
:- mode repeat0(pred(in, out) is det,     in, out) is multi.
:- mode repeat0(pred(in, out) is semidet, in, out) is multi.
:- mode repeat0(pred(in, out) is multi,   in, out) is multi.
:- mode repeat0(pred(in, out) is nondet,  in, out) is multi.


% Something repeated zero to several times. Return the collected return values as a list.

:- pred repeat0(pred(T, list(char), list(char)), list(T), list(char), list(char)).
:- mode repeat0(pred(out, in, out) is det,     out, in, out) is multi.
:- mode repeat0(pred(out, in, out) is semidet, out, in, out) is multi.
:- mode repeat0(pred(out, in, out) is multi,   out, in, out) is multi.
:- mode repeat0(pred(out, in, out) is nondet,  out, in, out) is multi.

% Something repeated one to several times, without any return value

:- pred repeat(pred(list(char), list(char)), list(char), list(char)).
:- mode repeat(pred(in, out) is det,     in, out) is multi.
:- mode repeat(pred(in, out) is semidet, in, out) is nondet.
:- mode repeat(pred(in, out) is multi,   in, out) is multi.
:- mode repeat(pred(in, out) is nondet,  in, out) is nondet.


% Something repeated one to several times. Return the collected return values as a list.

:- pred repeat(pred(T, list(char), list(char)), list(T), list(char), list(char)).
:- mode repeat(pred(out, in, out) is det,     out, in, out) is multi.
:- mode repeat(pred(out, in, out) is semidet, out, in, out) is nondet.
:- mode repeat(pred(out, in, out) is multi,   out, in, out) is multi.
:- mode repeat(pred(out, in, out) is nondet,  out, in, out) is nondet.

% The longest sequence of repetitions of the specified thing, which doesn't return anything.
% Succeeds for zero repetitions.

:- pred repeatmax0(pred(list(char), list(char)),  list(char), list(char)).
:- mode repeatmax0(pred(in, out) is det,     in, out) is det.
:- mode repeatmax0(pred(in, out) is semidet, in, out) is det.
:- mode repeatmax0(pred(in, out) is multi,   in, out) is multi.
:- mode repeatmax0(pred(in, out) is nondet,  in, out) is nondet.


% The longest sequence of repetitions of the specified thing, which returns something. Its
% return values are returned as a list. Succeeds for zero repetitions (with an empty list).

:- pred repeatmax0(pred(T, list(char), list(char)), list(T), list(char), list(char)).
:- mode repeatmax0(pred(out, in, out) is det,     out, in, out) is det.
:- mode repeatmax0(pred(out, in, out) is semidet, out, in, out) is det.
:- mode repeatmax0(pred(out, in, out) is multi,   out, in, out) is multi.
:- mode repeatmax0(pred(out, in, out) is nondet,  out, in, out) is nondet.

% The longest sequence of repetitions, of the specified thing, which doesn't return anything. The
% thing must succeed at least once.

:- pred repeatmax(pred(list(char), list(char)), list(char), list(char)).
:- mode repeatmax(pred(in, out) is det,     in, out) is det.
:- mode repeatmax(pred(in, out) is semidet, in, out) is semidet.
:- mode repeatmax(pred(in, out) is multi,   in, out) is multi.
:- mode repeatmax(pred(in, out) is nondet,  in, out) is nondet.


% The longest sequence of repetitions, of the specified thing, which returns something. The return
% values are collected and returned as a list. The thing must succeed at least once.

:- pred repeatmax(pred(T, list(char), list(char)), list(T), list(char), list(char)).
:- mode repeatmax(pred(out, in, out) is det,     out, in, out) is det.
:- mode repeatmax(pred(out, in, out) is semidet, out, in, out) is semidet.
:- mode repeatmax(pred(out, in, out) is multi,   out, in, out) is multi.
:- mode repeatmax(pred(out, in, out) is nondet,  out, in, out) is nondet.


% A thing that may or may not appear. This consumes the thing at the beginning, if it is there. If
% not, it succeds as well.

:- pred maybe(pred(list(char), list(char)), list(char), list(char)).
:- mode maybe(pred(in, out) is semidet, in, out) is det.


%--------------------------------------------------------------------------------
% Parser drivers

% Execute a parser and return the results, along with the unparsed rest of the input, as a list. The
% page parser produces exactly one solution.
:- pred parse( % Parser
               pred(T, list(char), list(char)),
               % The text to parse
               string,
               % The parser results along with the rest of the text
               list({T, list(char)})
   ).

:- mode parse((pred(out, in, out) is nondet),
              in,
              out)
   is det.


% Execute a parser which is required to produce exactly one result, and which consumes the entire
% input. It is an error for it to produce no or several results. It's also an error not to consume
% the entire input. Bot result in an exception.
:- pred parse_uniq( % Parser
                    pred(T, list(char), list(char)),
                    % The text to parse
                    string,
                    % The parser result
                    T).

:- mode parse_uniq((pred(out, in, out) is nondet),
                   in,
                   out)
   is det.


% Execute the page parser, which is required to produce exactly one result, and consume the entire
% input. It is an error for it to produce no or several results. It's also an error not to consume
% the entire input. Both result in an exception. After that, the parse result is postprocessed and
% the links are converted from raw to MoinMoin to Tiki.

:- pred parse_full(
    % Collected information. This is needed because the postprocess run needs to convert raw link
    % targets to MoinMoin link targets.
    m2t::in,

    % The name of the page being parsed
    moinpage::in,

    % The text to parse
    string::in,
 
    % The postprocessed parser result
    page(target(tikipage))::out)
   is det.


%================================================================================
% Implementation

:- implementation.
:- import_module solutions, unicode, exception.
:- import_module postprocess, serialiser, transform.

end([], []).


nl -->
    ( ['\n'] ; ['\r', '\n'] ).


space --> ( [' '] ; ['\t'] ).


spaces0 --> repeatmax0(space).


spaces --> repeatmax(space).


digits(Digits) -->
    repeatmax(digit, Digits).


digit(Ch) -->
    [ Ch ],
    { char.is_digit(Ch) }.
            

nonnl(Ch) -->
    not(nl),
    [ Ch ].


whitespace -->
    [ Char ],
    { is_whitespace(Char) }.


nonwhitespace(Char) -->
    [ Char ],
    { not(is_whitespace(Char)) }.


eol(['\n'|Rest], Rest).
eol(['\r', '\n'|Rest], Rest).
eol([], []).


str(Str, Txt1, Txt2) :-
    to_char_list(Str, Chars),
    append(Chars, Txt2, Txt1).


alnumstr(Txt) -->
    repeatmax((pred(Ch::out, [Ch|Out]::in, Out::out) is semidet :-
                   glib_isalnum(Ch)
              ),
              L),
    { from_char_list(L, Txt) }.


alnumstr_(Txt) -->
    repeatmax((pred(Ch::out, [Ch|Out]::in, Out::out) is semidet :-
                   ( glib_isalnum(Ch) ; Ch = '_' )
              ),
              L),
    { from_char_list(L, Txt) }.


alnumchars(L) -->
    repeatmax(alnumchar, L).


alnumchar(Ch) -->
    [ Ch ],
    { glib_isalnum(Ch) }.


%--------------------------------------------------------------------------------
% Parser combinators


repeat0(_) -->
    [].

repeat0(Pars) -->
    Pars,
    repeat0(Pars).


repeat0(_, []) -->
    [].

repeat0(Pars, Values) -->
    { Values = [Val|Values1] },
    Pars(Val),
    repeat0(Pars, Values1).


repeat(Pars) -->
    Pars,
    repeat0(Pars).


repeat(Pars, [Val|Values]) -->
    Pars(Val),
    repeat0(Pars, Values).


repeatmax0(Pars) -->
    ( if   Pars
      then repeatmax0(Pars)
      else []
    ).

repeatmax0(Pars, Values) -->
    ( if   Pars(Val)
      then { Values = [Val|Vals] },
           repeatmax0(Pars, Vals)
      else { Values = [] }
    ).


repeatmax(Pars) -->
    Pars,
    repeatmax0(Pars).


repeatmax(Pars, [Val|Values]) -->
    Pars(Val),
    repeatmax0(Pars, Values).


parse(Parser, Str, Results) :-
    to_char_list(Str, StrL),
    solutions((pred({Val, Rest}::out) is nondet :-
                   Parser(Val, StrL, Rest)),
              Results).


parse_uniq(Parser, Str, Res) :-
    parse(Parser, Str, ResL),
    (   ResL = [{Res, Rest}],
        ( if   Rest = []
          then true
          else throw("Bug: Parsing didn't encompass the whole page.")
        )   
    ;   ResL = [],
        throw("Bug: Parsing the page failed.")
    ;   ResL = [_,_|_],
        throw("Bug: Ambigious parse of page.")
    ).


parse_full(M2T, Parent, Str, Page) :-
    parse_uniq(page, Str, Page1),
    postprocess(M2T, Page1, Page2),
    transform(raw_to_moin(M2T, Parent), Page2, Page3),
    transform(moin_to_tiki(M2T), Page3, Page).



% :- pred count_0(pred(list(char), list(char)),     % the thing
%                 int,                              % number of occurances
%                 list(char), list(char)).
% :- mode count_0(pred(in, out) is det,     out, in, out) is multi.
% :- mode count_0(pred(in, out) is semidet, out, in, out) is multi.
% :- mode count_0(pred(in, out) is multi,   out, in, out) is multi.
% :- mode count_0(pred(in, out) is nondet,  out, in, out) is multi.

% count_0(_, 0) -->
%     [].

% count_0(Pars, Number) -->
%     Pars,
%     count_0(Pars, Number0),
%     { Number = Number0 + 1 }.

% count(Pars, Number) -->
%     count_0(Pars, Number),
%     ( if   not Pars
%       then []
%       else end
%     ).

count(Parser, Number) -->
   ( if   Parser
     then count(Parser, Number1),
          { Number = Number1 + 1 }
     else { Number = 0 }
   ).

maybe(Pred) -->
    ( if   Pred
      then []
      else { true }
    ).


peek(Pars, T, Txt, Txt) :-
    Pars(T, Txt, _).


peek(Pars, Txt, Txt) :-
    Pars(Txt, _).

