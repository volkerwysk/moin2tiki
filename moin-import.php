<?php

///XXX alle Rückgabewerte überprüfen

require_once('tiki-setup.php');

error_reporting(E_ALL);
//ini_set('display_errors', '1');


// Command line arguments

if (count($argv) != 5) {
    echo "\nThe ", $argv[0], " script requires the following command line arguments:\n",
        "1. The path to the intermediate directory\n",
        "2. The Tiki user.\n",
        "3. The language of the pages\n",
        "4. File name for the list of successfully migrated Tiki pages\n";
    exit(1);
}

$tmpdir       = $argv[1];
$user         = $argv[2];
$language     = $argv[3];
$successlist  = $argv[4];


// Load the needed Tiki libraries
$tikilib  = TikiLib::lib('tiki');
$wikilib  = TikiLib::lib('wiki');
$histlib  = TikiLib::lib('hist');
$cachelib = TikiLib::lib('cache');
$mimelib  = TikiLib::lib('mime');


// Get and sort the contents of a directory. Filter out "." and "..".
function dircontents(string $dir): array
{
    if (is_dir($dir)){
        if ($dh = opendir($dir)){

            $contents = [];
            while (($file = readdir($dh)) !== false) {
                if ($file !== '.' && $file !== '..') {
                    $contents[] = $file;
                }
            }
            closedir($dh);
            sort($contents);
            return $contents;

        } else {
            echo "Can't open the directory \"$dir\":\n" . error_get_last()['message'] . "\n";
            exit(1);
        }
    } else {
        echo "Not a directory: \"$dir\"\n";
        exit(1);
    }
}


// Test for intermediate directory structure
$tmpcontents = dircontents($tmpdir);

if ($tmpcontents !== ['attachments', 'pages']) {
    echo "moin-import.php error: Expecting intermediate directory structure.\n";
    exit(1);
}




try {
    // Open the success list for writing. The contents will be replaced. This success list will
    // contain the Tiki page names for all the successfully migrated pages. The moin2tiki program
    // reads it after the call of moin-import.php finished.
    $succlog = fopen($successlist, 'w');
    if (! $succlog) {
        echo "moin-import.php error: Can't open $successlist for writing.\n";
        exit(2);
    }

    // 1. Process the attachments of all the pages which have some. For each page with attachment(s)
    // there's a subdirectory of the "attachments" directory, with the name of the page as the
    // directory name.

    $attpages = dircontents($tmpdir . "/attachments");
    $attmap = [];

    // For all the pages which have attachments...
    foreach($attpages as $page) {

        // Create the page which holds the attachment(s), if necessary. Create a blank page, for the
        // only purpose of holding the attachment(s). This page possibly will be replaced by a
        // regular (non-blank) wiki page later.
        if ($tikilib->page_exists($page)) {
            echo "\nPage \"$page\" already exists.\n";
        } else {
            echo "\nPage \"$page\" doesn't exist yet, creating... ";

            $ret = $tikilib->create_page(
                $page,
                0,                          //$info['hits'],
                "",                         //$info['data'],
                $tikilib->now,              //$info['lastModif'],
                "",                         //$info['comment'],
                $user,                      //! empty($this->config['fromUser']) ? $this->config['fromUser'] : $info['user'],
                null,                       //! empty($this->config['fromSite']) ? $this->config['fromSite'] : $info['ip'],
                "Imported from MoinMoin",   //$info['description'],
                $language                   //isset($info['lang']) ? $info['lang'] : '',
            );

            if ($ret === false) {
                echo "moin-import.php error creating page.\n";
                exit(1);
            } else {
                echo "Created page.\n";
            }
        }

        // Get info about all the attachments to the page. The function list_wiki_attachments is in
        // wikilib.php, starting at line 893.
        //
        // The result is an array with two entries, labeled "data" and "cant". The "data" entry
        // holds a list with a map for each attachment that has been found. This map holds the
        // entries "user", "attId", "page", "filename", "filesize", "filetype", "hits", "created"
        // and "comment".
        $res = $wikilib->list_wiki_attachments($page);

        // For all the attachments to the page, which come from Moin2Tiki...
        $attlist = dircontents($tmpdir . "/attachments/" . $page);
        foreach($attlist as $filename) {
            echo "Processing attachment \"$filename\"...\n";

            $attpath = $tmpdir . "/attachments/" . $page . "/" . $filename;
            $data = file_get_contents($attpath);

            $attid = false;
            foreach($res['data'] as $attdata) {
                if ($attdata['filename'] == $filename) {
                    $attid = $attdata['attId'];
                    $attmap[$page . "/" . $filename] = $attid;
                    break;
                }
            }

            if ($attid) {
                echo "Attachment already present. ID = $attid\n";
            } else {
                echo "New attachment.\n";

                $mimetype = $mimelib->from_filename($filename);

                $attid = $wikilib->wiki_attach_file(
                    $page,
                    $filename,
                    $mimetype,                                  // $type,
                    strlen($data),                              // $size,
                    $data,                                      // $data,
                    "",                                         // $comment,
                    $user,                                      // $user,
                    "",                                         // $fhash,
                    ''                                          // $date = '')
                );

                $attmap[$page . "/" . $filename] = $attid;

                echo "Stored Attachment with ID $attid\n";
            }
        } // foreach $attlist
    } // foreach $attpages


    // 2. Process the pages. Each page is a file in the "pages" directory. The file name is the page
    // name.
    $pages = dircontents($tmpdir . "/pages");
    foreach($pages as $page) {
        echo "\nProcessing wiki page \"$page\"...\n";

        $pagepath = $tmpdir . "/pages/" . $page;
        $data = file_get_contents($pagepath);
        $offset = 0;
        $datanew = "";

        while (true) {
            $matches = [];
            $ret = preg_match("|////(.*)////|U", $data, $matches, PREG_OFFSET_CAPTURE , $offset);
            if ($ret === 1) {
                if ($matches[1][0] == '') {
                    $datanew .= substr($data, $offset, $matches[0][1] - $offset);
                    $datanew .= "////";
                } else {
                    $datanew .= substr($data, $offset, $matches[0][1] - $offset);
                    $datanew .= $attmap[$matches[1][0]];
                }
                $offset = $matches[0][1] + strlen($matches[0][0]);
            } else {
                $datanew .= substr($data, $offset);
                break;
            }
        }

        $ret = $tikilib->get_page_info($page);

        if ($ret === false) {
            // Page doesn't exist yet.

            // function create_page($name, $hits, $data, $lastModif, $comment, $user = 'admin', $ip
            // = '0.0.0.0', $description = '', $lang = '', $is_html = false, $hash = null, $wysiwyg
            // = null, $wiki_authors_style = '', $minor = 0, $created = '')
            echo "Creating new page \"$page\"...\n";
            $tikilib->create_page(
                $page,                      // page name
                0,                          // hits
                $datanew,                   // data
                $tikilib->now,   ///null,                       // lastModif
                "Imported from MoinMoin",   // comment
                $user,                      // user
                '0.0.0.0',                  // IP
                "",                         // description
                $language
            );
        } elseif ($ret['data'] == $datanew) {
            echo "Same page already stored.\n";
        } else {
            // The page has already been created above (see "$tikilib->create_page")
            echo "Updating page \"$page\"...\n";
            $tikilib->update_page(
                $page,
                $datanew,
                'Imported from MoinMoin',
                $user,     // user
                null,      // ip
                '',        // description
                0,         // edit_minor
                $language, // lang
                false,     // is_html
                null,      // hash
                $tikilib->now,   ///null,      // saveLastModif    
                null);     // wysisyg
        }

        // In case the page has attachments, an empty dummy page has been created, for the sole
        // purpose of holding the attachments. Only after this, the real page has been saved in the
        // database. This means we can have a redundant, empty page with the version 1 and the real
        // page with the version 2. We check if this is the case. If so, we remove the empty version
        // 1 page and change the version of the real page from 2 to 1. This affects the two database
        // tables tiki_pages and tiki_history.
        $ret = $histlib->get_page_history($page);  // returns sorted by version, in *descending* order

        if (count($ret) == 2
            && $ret[0]['version'] == 2
            && $ret[1]['version'] == 1
            && $ret[1]['data'] == '')
        {
            $histlib->remove_version($page, 1);

            $pages = $tikilib->table('tiki_pages');
            $resud = $pages->update(['version' => 1],
                                    ['pageName' => $page, 'version' => 2]);

            $hist = $tikilib->table('tiki_history');
            $resud = $hist->update(['version' => 1],
                                   ['pageName' => $page, 'version' => 2]);
        }

        // Log successful creation of page
        $retfw = fwrite($succlog, "$page\n");
        if (!$retfw) {
            echo "moin-import.php error: Can't write to $successlist\n";
        }
        fflush($succlog);
    } // foreach($pages as $page)

    fclose($succlog);
    
    $cachelib->flush_opcode_cache();
    $cachelib->flush_memcache();
    $cachelib->flush_redis();

}
catch (Exception $e) {
    echo "moin-import.php Error: " . $e->getMessage() . "\nAborting execution.\n";
}
catch (Error $e) {
    echo "moin-import.php Error: " . $e->getMessage() . "\nAborting execution.\n";
}
