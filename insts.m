:- module insts.
:- interface.
:- import_module list.


:- inst non_empty_list(I) 
    ---> [ I | non_empty_list_1(I) ].

:- inst non_empty_list_1(I) 
    ---> []
    ;    [ I | non_empty_list_1(I) ].


:- pred append1(list(T), list(T), list(T)).
:- mode append1(
    in(list(non_empty_list)),
    in(non_empty_list(non_empty_list)),
    out(non_empty_list(non_empty_list)))
is det.



:- pred split_last1(
    list(T)::in(non_empty_list(I)),
    list(T)::out(list(I)),
    T::out(I))
is det.



% :- pred foldl1(pred(L, A, A), list(L), A, A).
% :- mode foldl1(pred(in(I), in(A), out(A)) is det, in(I), in(A), out(A)) is det.


:- implementation.


% foldl1(_, [], !A).
% foldl1(P, [H | T], !A) :-
%     P(H, !A),
%     foldl1(P, T, !A).


append1([], Ys, Ys).
append1([X | Xs], Ys, [X | Zs]) :-
    append1(Xs, Ys, Zs).



split_last1([H | T], AllButLast, Last) :-
    (
        T = [],
        AllButLast = [],
        Last = H
    ;
        T = [TH | TT],
        det_split_last_loop1(TH, TT, AllButLastTail, Last),
        AllButLast = [H | AllButLastTail]
    ).

:- pred det_split_last_loop1(
    T::in(T),
    list(T)::in(list(T)), 
    list(T)::out(list(T)), 
    T::out(T))
is det.

det_split_last_loop1(H, T, AllButLast, Last) :-
    (
        T = [],
        AllButLast = [],
        Last = H
    ;
        T = [TH | TT],
        det_split_last_loop1(TH, TT, AllButLastTail, Last),
        AllButLast = [H | AllButLastTail]
    ).

