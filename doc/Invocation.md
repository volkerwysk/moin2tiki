This page documents the usage of the Moin2Tiki program, and the parameters it has.

## Actions

For calling moin2tiki you need to specify an action. All actions, except for the "help" action (and the "ignore"
action, see below), require some pages to be selected. This are the normal actions. There are some more actions
for development, see below.

* `help` - Prints the usage info.

* `listmoin` - List the MoinMoin page names of the selected pages.

* `listtiki` - List the Tiki versions of the selected MoinMoin page names.

* `attachments` - For each selected page, list the attachments which it refers.

* `migrate` - This is the main action. It converts the selected pages from MoinMoin markup syntax to Tiki
  syntax and imports them in the Tiki server.

* `ignore` - Just add the selected pages, and the zombie pages, to the ignore list. Don't do anything different
  with the. See `--ignore-list-write`. When there aren't any selected pages, this will only add the zombie pages to
  the ignore list.


## Selecting Pages

The moin2tiki program requires that you select some pages which should be operated on (except for the "help"
action). Pages are selected with one of the following arguments. But see "Ignored pages", below.

* `--page=...` - Selects a single page. This can be specified multiple times in an invocation. But it can't be 
  combined with the other page selection options.

* `--prefix=...` - Selects all pages with a common prefix. This is a prefix for MoinMoin page names, not Tiki
  page names.

* `--all` - Select all pages.

All the selected pages will be processed according to the chosen action.



## Usual Options

* `--pagesdir=...` - The path to the MoinMoin "pages" directory. See [Operation](Overview.html#operation). This is
  needed for all actions except for `help`.

* `--user=...` - The name of the Tiki user, which is to own the migrated pages. See
  [Users](Overview.html#users). 

* `--language=...` - Tiki has multi-language support. This option specifies the language of the migrated 
  pages. It's a two-letter language code, like `de` or `en`. For a call of `moin2tiki`, only one language
  can be used. There's a list of the supported language codes in lang/langmapping.php, relative
  to the Tiki root directory.

* `--wwwdir=...` - The document root directory of the webserver. This defaults to "/var/www/html/".

* `--tikidir=...` - The path of the Tiki root directory, below the document root of the web server. This defaults
  to "tiki". Can be empty.



## Other Options

* `--ignore-list-read=...` - Specify a file, which lists MoinMoin page names, which should be ignored. The listed
  MoinMoin pages will be treated like they don't occur in the "pages" directory.

* `--ignore-list-write=...` - Specify a file, to which a list of MoinMoin page names will be appended (one per
  line). The MoinMoin page names of all successfully migrated pages are added. When used in conjunction with the
  `--ignore-list-read=...` option, this will exclude pages which already have been migrated, from further
  processing. You can also explicitly exclude pages from processing, with the `ignore` action.

* `--pnrepl=...` - Specifies the name for the replacements file. See 
  [Transformation of Page Names](Overview.html#transformation-of-page-names). This is defaults to `./pnrepl`.

* `--tmpdir=...` - The name for intermediate directory. See 
  [The Intermediate Directory Structure](Overview.html#the-intermediate-directory-structure). 
  Defaults to `./moin2tiki.tmp`.

* `--nozombiemsg` suppress the (somewhat annoying) messages about zombie pages.

* `--adjust-hlevels` - If you have used heading levels in MoinMoin, such that the used levels start at something
  other than the first level ("`= ... =`"), you can use this option. It changes the used heading levels, such that
  they begin at the first level, in the generadte in Tiki pages ("`! ...`").


## Development and Debugging

The following actions are for development and debugging. You don't need them if you just want to migrate from 
MoinMoin to Tiki.

* `convert` - Convert one page from MoinMoin to Tiki and output the result. Exactly one page must be selected.

* `create` - Create the intermediate directory structure, which can be used to import the selected pages with the
  `moin-import.php` script, but don't call this script. This directory tree is normally deleted after a 
  successful migration. You need to delete or move the directory out of the way before you can call `moin2tiki` 
  again.

* `parse` - Parse the one selected page and print the results.
