Moin2Tiki comes with an [AppImage](https://appimage.org/), so you don't need to compile it yourself, unless you
want to change something. See the `appimage` directory. You can just rename the `moin2tiki-*-x86_64.AppImage` file
to `moin2tiki` and use that.

Moin2Tiki runs under Linux. I don't plan to do a release for Windows. Porting it shouldn't be very hard, though,
provided you know Mercury.

## Install the Mercury compiler

Moin2Tiki is written in the [Mercury](https://mercurylang.org/) language, except for a small part in PHP. You need
the glorious Melbourne Mercury Compiler to compile. Mercury isn't included in the Ubuntu repositories, and probably
the repositoris of other Linux distributions either.

You have the following options to install the Mercury compiler:

* Get the latest stable release, packaged by the Mercury team. They provide a PPA for Ubuntu and other Debian based
  Linux distributions. Follow the instructions on [http://dl.mercurylang.org/deb/](http://dl.mercurylang.org/deb/).

* Use Fabrice Nicol's [Docker images](https://github.com/fabnicol/). [Docker](https://www.docker.com/) is a way to
  package binaries of software, which run on any Linux distribution where Docker is available. Fabrice provides
  Docker images packaged on a daily basis (the newest ROTD and Git versions). Chose one of the three linked at the
  bottom of the page (for instance, ubuntu4mercury).

* You can compile the latest ROTD source distribution of the compiler yourself. Download it from
  [here](http://dl.mercurylang.org/index.html) under the heading "ROTD versions". The compilation and installation
  isn't particularly difficult, but it takes a long time to compile (about an hour on my system). See the INSTALL
  file for instructions. 


## Install the Posix library

You need to install the Posix "extra" library for compiling Moin2Tiki. For this, you need a running compiler and a
current source distribution of the compiler, because it is distributed in the "extras" directory in the compiler
sources. Download it from [here](http://dl.mercurylang.org/index.html) under the heading "ROTD versions".

Go to the `extras/posix` directory and run this command:

```
mmc -j8 --make libposix.install
```

The `-j8` argument specifies your number of processor cores. Change as needed.


## More Dependencies

Now you need to install the development files of the glib2.0 library, which Moin2Tiki uses. This is a package
which should be included in all Linux distributions. In Ubuntu, it is called `libglib2.0-dev`.


##  Moin2Tiki

Go to the top directory of the Moin2Tiki source distribution and run this command:

```
./mmc-make
```

You get the binary program `moin2tiki`.
