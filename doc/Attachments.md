## How it works

Attachments in MoinMoin can be linked to and can be embedded. In both cases, the linking/embedding
page might be a different page from the host of the attachment. So attachments are identified by the
page name and the attachment file name. Thus they are global (just the page defaults to the refering
page).

This means that we have the pages which have been selected in the moin2tiki command line, and
additional pages which occur only as the host of some attachments, which are refered to in some of
the selected pages.

On the Tiki side, those additional pages need to be created, so the attachments can be saved. They
are created as empty pages with attachments.

In Tiki, attachments are identified by attachment IDs, which are global in Tiki - *not* local to the
host page. In Tiki pages which refer to attachments, the attachment IDs need to be included. But
those are generated only when the attachment is saved in the Tiki database. The moin2tiki converter
can't know the attachment IDs. It only knows the page names and attachment file names.

This means that either the moin-import.php script is to tell the moin2tiki program the attachment
IDs, or it converts the pages such that the correct attachment IDs are inserted in the new pages. I
follow the second approach. 

Care must be taken when an attachment is already present in the Tiki database. It must not be saved
again.

So the moin-import.php script is called with a page, which has placeholders for attached files. They
need to be read in and saved in the Tiki database. The placeholders need to be converted to Tiki
attachment IDs. And the resulting Tiki page needs to be saved in the Tiki database.

The script needs to know where to find the files which are to be attached. Different files with the
same name may exist, being attached to different MoinMoin pages.

The script must create new, empty pages for attachments which aren't contained in pages which have
been converted from MoinMoin to Tiki.



The moin2tiki program creates new Tiki pages with placeholders for the MoinMoin pages which have
been selected on its command line. They are laid out in some pages directory ("pages"). For the
attachments, every page which has attachments refered to by the new MoinMoin pages, a directory with
the same name as the host page is created. Inside this directory, there are symbolic links which
refer to the attached files. The attachment directories are contained in some super directory
("attachments").

The moin-import script gets passed the pages directory and the attachments directory, as command
line arguments. Then it processes all the pages. In each page, the placeholders are examined. Each
consists of the page name and attachment file name. The script reads the attachment. If it is
already present in the corresponding Tiki page, then its attachment id is used. Otherwise, the
attachment is saved as a new attachment of the corresponding Tiki page. If the page doesn't exist
yet, a new, empty page is created.

For the selected pages, all the attachments will be included. For the additional pages, only the
attachments which have been embedded or linked to by some selected pages, will be included.

