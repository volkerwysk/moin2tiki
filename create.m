%================================================================================
% Generation of the intermediate directory structure, containing the translated pages and the
% attachments.
%================================================================================
:- module create.

%================================================================================
% Interface
%================================================================================
:- interface.
:- import_module io, assoc_list, list.
:- import_module moin2tiki, attachments, pages.


% Build the intermediate directroy structure, consisting of the converted pages and symlinks to the
% page attachments. This means that there will be all the pages selected on the command line, and
% attachments for all the attachments that those pages have or refer to. We get an attachment
% directory for each page selected on the command line, and additionally an attachment directory for
% each extra page. Extra pages are pages linked to by attachment references, which aren't selected
% on the command line. For each selected page, all attachments are linked. For each extra page, only
% the referenced attachments are linked.

:- pred create_intermediate_dir(
    m2t::in,            % see main.m
    io::di, io::uo
) is det.



% Remove the intermediate directroy structure after a successful migration.

:- pred remove_intermediate_dir(
    m2t::in,            % see main.m
    io::di, io::uo
) is det.



% Report dangling attachment references to the user.

:- pred report_dangling_attachments(
    % AttLL_dangling: For each selected page, the list of the dangling attachment references in that
    % page.
    assoc_list(tikipage, list(attachment(tikipage)))::in,

    io::di, io::uo)
is det.


%================================================================================
% Implementation
%================================================================================
:- implementation.
:- import_module char, getopt, bool, string, exception, int, pair, map, solutions, require.
:- import_module tools, dir_contents, parser, parsertools, posix, posix.strerror, posix.realpath, posix.stat.
:- import_module serialiser, unicode, pagenames, pretty.



create_intermediate_dir(M2T, !IO) :-

    % Create the intermediate directory structure for the intermediate data, which is to be fed to the
    % moin-import.php script
    Intermediate = M2T ^ m2t_params ^ p_tmpdir,

    % Check if the intermediate directory structure already exists.
    posix.stat.stat(Intermediate, Res, !IO),
    (   
        if ( Res = ok(_)
           ; Res = error(EC),
             EC \= eNOENT
           )
        then 
           % Exists already
           throw("The intermediate " ++ quote(Intermediate) ++ " already exists. Normally, it is automatically " ++
                 "removed after successful migration (except for the action ""create""). You can " ++
                 "safely remove it, unless you have made an error when specifying it with --tmpdir.")
        else
           % Not found - that's what we want. Make the directory skel.
           mkdir(Intermediate, !IO),
           mkdir(Intermediate ++ "/pages", !IO),
           mkdir(Intermediate ++ "/attachments", !IO)
    ),

    % Parse all the MoinMoin pages. "ParsedPages" is an association list of the Tiki page names and
    % the parse result. The parse results are converted to Tiki link targets.
    pages.parse_pages_full(
        M2T, 
        ParsedPages,  % : pair(tikipage, page(target(tikipage)))
        !IO),

    % Get data about the attachments in the parsed pages.
    analyse_attachments(ParsedPages, _AttLL, _AttLL_straight, AttLL_dangling,
                        ReferencedL, _DanglingL, _SelectedTikiPages, ExtraTikiPages),

    % Inform the user about dangling attachment references
    report_dangling_attachments(AttLL_dangling, !IO),

    % Create the Tiki pages in the intermediate directory
    list.foldl(write_tiki_page(M2T, ParsedPages), ParsedPages, !IO),
    io.nl(!IO),

    % Create symlinks to all the attachments in all the selected pages.
    create_full_attachments(M2T, !IO),

    % Create symlinks to the attachments in the extra pages (if any). See create_intermediate_dir in
    % the interface section.
    create_extra_attachments(M2T, ReferencedL, ExtraTikiPages, !IO).




remove_intermediate_dir(M2T, !IO) :-
    Intermediate = M2T ^ m2t_params ^ p_tmpdir,
    tools.system("rm -r " ++ quote(Intermediate), !IO).



% Convert one page from the parse result of the MoinMoin page to a Tiki page and write it to disk.

:- pred write_tiki_page(
    m2t::in,                                    % Colleced data (see main.m)
                                                % Parse result for all selected pages
    assoc_list(pages.tikipage, parser.page(parser.target(pages.tikipage)))::in,
    pair(tikipage, page(target(tikipage)))::in, % Pair of Tiki page name and parse result
    io::di, io::uo)
is det.

write_tiki_page(M2T, ParsedPages, TikiPage - ParsedPage, !IO) :-
    Tmpdir = M2T ^ m2t_params ^ p_tmpdir,
    TikiFile = Tmpdir ++ "/pages/" ++ tikipage(TikiPage),
    io.write_string("Writing page " ++ quote_qm(tikipage(TikiPage)) ++ "...\n", !IO),
    serialiser.page1(
        ParsedPage,
        M2T,
        ParsedPages, 
        PageTxt,
        Messages),
    pretty.print_messages(Messages, !IO),
    write_contents(TikiFile, PageTxt, !IO).


% Full path of the attachments directories (under "attachments/") inside the intermediate directory.

:- func tiki_att_dir(
    m2t,            % Static data. Needed for the tmpdir.
    tikipage)       % Tiki page name
   = string.        % Path for Tiki page

tiki_att_dir(M2T, TikiPage) =
    (M2T ^ m2t_params ^ p_tmpdir)
    ++ "/attachments/" ++ tikipage(TikiPage).



% Path for the symlink target to an attachment, in the "attachments" directory in a MoinMoin page directory.

:- func moin_att_path(
    m2t,                      % All the data
    pair(tikipage,filename))  % Tiki page name and attache file name
   = string.                  % Path for Tiki page

moin_att_path(M2T, TikiPage - Att) = Path :-
    MoinPage = (M2T ^ m2t_backwards_resolve)(TikiPage),
    MoinDir = pagenames.moin(M2T ^ m2t_pages_dirs, MoinPage),
    Path = (M2T ^ m2t_params ^ p_pagesdir) ++ "/" ++
           MoinDir ++
           "/attachments/" ++
           filename(Att).


% Create symlinks for all the attachments of a Moin page, regardless of if they are referenced in the
% selected Moin pages or not. This is for the Moin pages selected on the command line.

:- pred create_full_attachments(
    m2t::in,                                            % All the data
    io::di, io::uo)
is det.

create_full_attachments(M2T, !IO) :-
    foldl(create_full_attachments_1(M2T), M2T ^ m2t_selectedmoinpages, !IO).


% Create the attachment symlinks for one selected Moin page. Symlinks are created for all
% attachments including attachments that aren't referenced by selected pages.

:- pred create_full_attachments_1(
    m2t::in,          % All the data
    moinpage::in,     % The Moin page name in question
    io::di, io::uo)
is det.

create_full_attachments_1(M2T, MoinPage, !IO) :-
    TikiPage = (M2T ^ m2t_resolve)(MoinPage),
    io.write_string("Adding all attachments of the selected Tiki page " ++ quote_qm(tikipage(TikiPage)) ++ "...\n",
                    !IO),
    AttL = map.lookup(M2T ^ m2t_moinpartsmap, MoinPage) ^ mp_attachments,
    (  AttL = []
    ;  AttL = [_|_],
       mkdir(tiki_att_dir(M2T, TikiPage), !IO),
       Command = "ln -s "
           ++ string.join_list(" ",
                  map(func(Att) = shell_quote(moin_att_path(M2T, TikiPage - Att)),
                      AttL))
           ++ " " ++ shell_quote(tiki_att_dir(M2T, TikiPage)),
       system(Command, !IO)
   ).



% Create symlinks to some of the attachments of the extra pages. Extra pages are pages which have
% attachments referenced by selected pages, but aren't selected themselves. Only the symlinks to
% attachments which are referenced by selected pages, are created.

:- pred create_extra_attachments(
    % All the data
    m2t::in,

    % List of the attachments in each page
    assoc_list(tikipage, list(attachment(tikipage)))::in,

    % The extra Tiki pages' names
    list(tikipage)::in,
    io::di, io::uo)
is det.

create_extra_attachments(M2T, ReferencedL, ExtraPages, !IO) :-
    foldl(create_extra_attachments_1(M2T, ReferencedL), ExtraPages, !IO).



% Create symlinks to some of the attachments of one extra page.

:- pred create_extra_attachments_1(
    m2t::in,                                               % All the data
    assoc_list(tikipage, list(attachment(tikipage)))::in,  % List of the attachments in each page
    tikipage::in,                                          % The extra Tiki page name in question
    io::di, io::uo)
is det.

create_extra_attachments_1(M2T, ReferencedL, TikiPage, !IO) :-
    io.write_string("Adding only the referenced attachments of the extra page " ++ quote_qm(tikipage(TikiPage)) ++
                    "...\n", !IO),

    assoc_list.lookup(ReferencedL, TikiPage, AttL),
    (  AttL = []
    ;  AttL = [_|_],
       Tmpdir = M2T ^ m2t_params ^ p_tmpdir,
       mkdir(Tmpdir ++ "/attachments/" ++ tikipage(TikiPage), !IO),
       Command = "ln -s "
           ++ string.join_list(" ",
               map(create_extra_attachments_2(M2T, TikiPage), AttL))
           ++ " " ++ shell_quote(tiki_att_dir(M2T, TikiPage)),
       system(Command, !IO)
   ).

:- func create_extra_attachments_2(m2t, tikipage, attachment(tikipage)) = string.

create_extra_attachments_2(M2T, TikiPage, Att) = Path :-
    ( Att = straight(_, Filename),
      MoinPage = (M2T ^ m2t_backwards_resolve)(TikiPage),
      Path0 = (M2T ^ m2t_params ^ p_pagesdir) ++ "/" ++
             pagenames.moin(M2T ^ m2t_pages_dirs, MoinPage) ++ "/attachments/" ++ filename(Filename),
      Path = shell_quote(Path0)
    ; Att = dangling(_, _),
      unexpected($pred, "Dangling attachment")
    ).




% Report any dangling attachment references to the user, if any.

report_dangling_attachments(AttLL_dangling, !IO) :-
    (
        if
            list.any_false(
                (pred((_ - AttL)::in) is semidet :- is_empty(AttL)),
                AttLL_dangling)
        then
            io.write_string("Found dangling attachment reference(s):\n", !IO),
            report_dangling_attachments_1(AttLL_dangling, !IO),
            io.nl(!IO)
        else
            true
    ).

:- pred report_dangling_attachments_1(
    assoc_list(tikipage, list(attachment(tikipage)))::in,
    io::di, io::uo)
is det.

report_dangling_attachments_1(AttLL_dangling, !IO) :-
    rollout_attlist_dangling(AttLL_dangling, RolledOut),
    map_state(
        (pred({ TikiPage, AttTikiPage, AttFilename }::in, IOin::di, IOout::uo) is det :-
            io.format("In page %s: reference to %s / %s\n",
                [ s(quote_qm(tikipage(TikiPage))), s(quote_qm(tikipage(AttTikiPage))),
                  s(quote_qm(filename(AttFilename))) ],
                IOin, IOout)),
        RolledOut,
        !IO).


:- pred rollout_attlist_dangling(
    assoc_list(tikipage, list(attachment(tikipage)))::in,
    list({tikipage, tikipage, filename})::out)
is det.

rollout_attlist_dangling(AttLL, RolledOut) :-
    solutions(rollout_attlist_dangling_1(AttLL), RolledOut).


:- pred rollout_attlist_dangling_1(
    assoc_list(tikipage, list(attachment(tikipage)))::in,
    { tikipage, tikipage, filename }::out)
is nondet.

rollout_attlist_dangling_1(AttLL, { TikiPage, AttTikiPage, AttFilename } ) :-
    member((TikiPage - AttL), AttLL),
    member(Att, AttL),
    Att = dangling(AttTikiPage, AttFilename).
