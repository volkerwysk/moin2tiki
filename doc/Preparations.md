
This document describes the things you have to do with your Tiki server in order to use Moin2Tiki.

## The Tiki User

Create your Tiki user. It's in Settings -> Groups. You can have only one user at a time. See 
[Limitations](Limitations.html#only-one-user).


## Things to Enable

* The Font Plugin (wikiplugin_font) must be enabled. 

* The Anchor Name Plugin (wikiplugin_aname) must be enabled. 

* The List Pages Plugin (wikiplugin_listpages) must be enabled. 

* The Indentation Plugin (wikiplugin_indent) must be enabled.

* If you want syntax highlighting, you need to enable "CodeMirror" in the "Editing and Plugins" control panel.


## Make Plugins Searchable

In Tiki, up to at least version 25.0, the bodies of plugins won't get searched in the default configuration. This
holds for all plugins, even ones which should definitely be searchable, such as the CODE, DIV and FONT plugins. In
order to enable search in the bodies of such plugins, it must be explicitly enabled in the Search control panel.
Not all plugins have bodies. For the ones without, the searchablility meaningless.

Moin2Tiki uses the following plugins with a body: `code,div,file,font,indent,sub,sup`. At least those must be
configured to be searchable. Here's a more complete list, which also encompasses some other plugins:
`attach,box,center,code,div,file,files,font,footnote,indent,markdown,quote,sort,sub,sup`.

To make them searchable, go to the Search control panel (in Tiki 25.0, that's under "Global Setup"). There's the
"General Settings" tab. Leave "Exclude all plugins" enabled and fill in the list of plugins in the "Except included
plugins" setting.

After that, rebuild the search index with the Rebuild Index button.


## Allowed Charaters in Tiki page names

The Wiki Link Format setting in Tiki must be "Complete". This means that as many characters as
possible can occur in Tiki page names. This is still much less than in MoinMoin page names, where
all characters except for `/` can occur. Search the control panels for "Wiki Link Format".


## The MoinMoin style sheet

The `moin.css` style sheet must be installed. Copy it to the Tiki root directory of the web
server. Then change the permissions, so the web server can access it. For Apache on Ubuntu, you
can do it like follows. Change your Tiki root directory (here "tiki") as needed:

```
sudo cp moin.css /var/www/html/tiki/
sudo chown www-data:www-data /var/www/html/tiki/moin.css
```

Then locate the "Custom HTML head content" field. In Tiki 24, it is in the "Wiki" control panel. In Tiki 25, it is
in the "Look & Feel" control panel, under "General Settings". Add this line to the field:

```
<link rel="stylesheet" href="moin.css">`
```

If you want to place `moin.css` elsewhere else, change the path as needed.


## Namespaces

Moin2Tiki uses Tiki namespaces as a counterpart for MoinMoin hierarchical page names. This means that the
Namespaces feature in Tiki must be enabled. In the "Wiki" control panel, in the "Features" tab, under
"Namespaces", enable "Namespace".

The default namespace separator in Tiki is "`:_:`". This can be changed. If you want to change it, Moin2Tiki
needs to be told what is to serve as the namespace separator. Use the `--pathsep` option (see
[Invocation](Invocation.html). Also see [The Namespace Separator](Overview.html#the-namespace-separator) in
[Overview](Overview.html).

The separator could be changed to `::`. Then you might get problems because `::` ... `::` is also Tiki syntax for
centered text. (MoinMoin doesn't seem to support centered text.) Tiki supports changing the `::`" text center
indicator to "`:::`". See "Center text using ::: instead of ::" in the "Wiki" control panel. This should always
be turned on, when `::` is used as the namespace separator. Therefore, in this case, Moin2Tiki assumes that it is
turned on and regards `:::` ... `:::` as the text center syntax. Because the text center syntax has no
counterpart in MoinMoin, this affects only the case when `:::` is used in MoinMoin text and needs to be escaped
so Tiki doesn't unintentionally interpret it. In order to handle this case, Moin2Tiki assumes that the text
center tag has been set to `:::`, when the path separator has been set to `::`. There's currently no way to turn
that off.


## The `moin-import.php` Script

The file `moin-import.php` must be placed in the Tiki root directory, under the web server's document root.

If the Tiki root directory is `/var/www/html/tiki`, this command does that:

```
sudo cp moin-import.php /var/www/html/tiki/moin-import.php
sudo chown www-data:www-data /var/www/html/tiki/moin-import.php
```
