This file lists the documentation for Moin2Tiki, the MoinMoin to Tiki migration tool,
version 1.1.1, released 2023-02-28. It is hosted on Gitlab, here:<br>
[https://gitlab.com/volkerwysk/moin2tiki](https://gitlab.com/volkerwysk/moin2tiki)

## License

Copyright 2023 Volker Wysk &lt;post at volker hyphen wysk dot de>.

Moin2Tiki is distributed under the terms of the GNU General Public License,
Version 3 or any later version. See the file [COPYING](COPYING) for details.

The Moin2Tiki documentation is released under the terms of the GNU Free Documentation License, Version 1.3 or any
later version. See [COPYING.FDL](COPYING.FDL) for details.


## Overview

* [Quick Start Guide](Quick Start Guide.html)<br>
  You need to read at least this and [Preparations](Preparations.html).

* [Overview](Overview.html)<br>
  How the moin2tiki program works, what you need to know about it.

* [Limitations](Limitations.html)<br>
  Limitations in the current version of Moin2Tiki.

* [Complaints about MoinMoin](Complaints about MoinMoin.html)<br>
  What induced me to write Moin2Tiki.


## Building Moin2Tiki

* [Compiling Moin2Tiki](Compiling Moin2Tiki.html)<br>
  Useful when you need to compile Moin2Tiki from the sources.


## Tiki Installation

Moin2Tiki doesn't teach you how to use or install Tiki. But here are some pointers.

* [Instructions for installing on Ubuntu systems](https://doc.tiki.org/Ubuntu-Install). <br>
  Please note, that Ubuntu
  22.04 includes only PHP 8, but up to Tiki version 24.2, Tiki works only with PHP 7.4. Fortunately, there's
  a good PPA for the PHP 7.4 packages. See the above link.

* [Here](https://doc.tiki.org/Installation) are the generic install instructions.

* For Tiki newcomers, there's [Tiki for Smarties](https://twbasics.tikiforsmarties.com/Tiki-for-Smarties).


## Usage

* [Preparations](Preparations.html)<br>
  What you need to do with the Tiki server to make Moin2Tiki work.

* [Invocation](Invocation.html)<br>
  How the moin2tiki program is called.


## Design Documents

These documents are useful when you want to hack Moin2Tiki, or want details on how it works.

* [Converting Links and Embeddings](Converting Links and Embeddings.html)<br>
  This document addresses the various forms of links and embeddings in MoinMoin and Tiki, and how they are
  converted.

* [Text Styles](Text Styles.html)<br>
  This lists the text style tags of MoinMoin and Tiki, and how they are converted.

* [Anchors in Tiki](Anchors in Tiki.html)<br>
  About handling of anchors in Tiki and the problems with it.

* [Attachments](Attachments.html)<br>
  How attachments are handled in MoinMoin and Tiki, and what that follows for the design.

* [Links Handling in Moin2tiki](Links Handling in Moin2tiki.html)<br>
  How Moin2tiki handles links internally.
