#! /bin/bash
echo "$0"
REAL="`realpath \"$0\"`"
DIR="`dirname \"$REAL\"`"
cd "$DIR" || exit 1

rm -rf AppDir
rm -f moin2tiki-*-x86_64.AppImage

linuxdeploy --appdir=AppDir -e ../moin2tiki -d moin2tiki.desktop --icon-file=moin2tiki.png -o appimage  \
            -l/lib/x86_64-linux-gnu/libc.so.6 \
            -l/lib/x86_64-linux-gnu/libm.so.6 \
            -l/lib64/ld-linux-x86-64.so.2 \
        || exit 1


# About that "linux-vdso.so.1" library in the moin2tiki executable: "It's a virtual shared object that doesn't have
# any physical file on the disk; it's a part of the kernel that's exported into every program's address space when
# it's loaded."
# https://stackoverflow.com/questions/58657036/where-is-linux-vdso-so-1-present-on-the-file-system


# /lib64/ld-linux-x86-64.so.2 is the dynamic linker.


# Mount AppImage: --appimage-mount
