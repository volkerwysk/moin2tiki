This is the home of the Moin2Tiki tool. It lets you migrate your pages from the [MoinMoin](https://moinmo.in/) wiki
engine to the [Tiki](https://tiki.org/) wiki engine.

The documentation is included in the sources (in the subdirectory `doc/`). See `doc/Moin2Tiki Documentation.html`.
Alternatively, you can read it online [here](https://volker-wysk.de/moin2tiki/doc/).

Moin2Tiki comes with an [AppImage](https://appimage.org/) of the program. This means that you don't need to compile
it yourself. See the [Quick Start Guide](http://volker-wysk.de/moin2tiki/doc/Quick%20Start%20Guide.html) for
details.

To download Moin2Tiki, you can either clone the Git repository. Or download the first (and only, so far)
release. See Deployments -> Releases to the left, or "1 Release" at the middle at the top.

There's also a [page about Moin2Tiki](https://tiki.org/ConversionFromMoinMoin) on the [Tiki](https://tiki.org/)
website.

If you've used Moin2Tiki for migrating your wiki, or are trying to, I'd like to hear from you! Please drop me a
note to &lt;post at volker-wysk dot de>.
