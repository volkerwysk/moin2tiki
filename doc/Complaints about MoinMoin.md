This lists complaints I have of MoinMoin, and a comparision with Tiki. It lists the reasons why I wrote Moin2Tiki.
This applies to MoinMoin version 1.9.9, which is the version I had installed when I started to write Moin2Tiki.

* It was hard to nest page elements in MoinMoin 1.9.9. For instance, a code block inside a list. Things seem
  to have improved in 1.9.11.

* Indented text greedily swallows all following text paragraphs with the same indentation level, provided the
  indentation is greater than zero. Empty lines don't stop it. The same holds for list items and definition list
  items. You need to make a list item without bullet, with ".".

* It isn't very stable. Sometimes, there are uncaught exceptions when a page is to be displayed. Then you can't do
  anything with this page, for instance, going back to the previous revision or delete it. You have to manually
  remove or repair the page on disk.

* MoinMoin doesn't rectify the links to a page, when that page is renamed. You can chose to replace the old page
  with a forwarding to the new one. But then again, the old page isn't accessable. You need to remove it manually
  from disk, if you want to create a page with the old page name again. In Tiki, the links to a page are updated,
  when you rename a page.

* There's a WYSIWYG editor, but it's very buggy and sometimes loses data.

* Zombie pages. When you delete a page, only its last revision is deleted. When you create the page anew, then
  the undeleted revisions come back to life.

* There's no support for embedded databases. Tiki has so-called "trackers".

* There's no support for documents which encompass several pages, like Tiki's "structures". But you can achieve
  something similar with the "&lt;&lt;ListPages>>" macro.

* Stemming doesn't work.

* CamelCase is used, and there's no way to turn it off. I hate it... In Tiki, CamelCase ("WikiWords") can be
  turned off.

* The &lt;&lt;ListPages>> macro doesn't sort the generated page list in a Unicode-aware manner. You get German
  umlauts at the end of the list. It also places pages with a lower-case letter at the end of the
  list.

* There's the "Surge Protection" feature, which prevents you from doing many queries in a short time, and it can't
  be turned off.

* The line distance of the displayed pages often are wrong, for instance in lists.

* All list items, which come one after another, are combined, even when they don't belong to the same list. You
  can't have a dotted list and a numbered list in series. You need to make paragraph in between, at indentation
  level zero.


There are also some things which I like more in MoinMoin:

* You can have all characters in page names, except for the dash.

* MoinMoin is much easier to install and configure, when you use it as a desktop wiki.

* Anchors aren't screwed up, like [in Tiki](Anchors in Tiki.html).

* For MoinMoin, there's a script which makes a dump of the entire wiki. There's also a feature like that in Tiki,
  but it's broken at the moment.
