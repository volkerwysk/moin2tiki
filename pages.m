%================================================================================
% Page names and link targets
%================================================================================

:- module pages.
:- interface.
:- import_module io, list, string, map, assoc_list, char, set, maybe.
:- import_module pagenames, moin2tiki, parser.


% The name of a MoinMoin page
:- type moinpage ---> moinpage(moinpage :: string).

% The name of a Tiki page
:- type tikipage ---> tikipage(tikipage :: string).

% An attachment file name
:- type filename ---> filename(filename :: string).


:- inst tikipage ---> tikipage(ground).


% A type class for pagename types.
:- typeclass pagename(P) where [
       func to_str(P) = string
%%%       func from_str(string) = P
   ].

:- instance pagename(moinpage).
:- instance pagename(tikipage).



% What is found in a MoinMoin page directory.

:- type moinparts
   ---> moinparts(

       % MoinMoin page name
       mp_moinpagename :: moinpage,

       % File names of the revisions of the page, relative to the "revisions" directory inside the
       % MoinMon page directory.
       mp_revisions    :: list(string),

       % The name of the file of the current revision of the page, relative to the "revisions"
       % directory.
       mp_current      :: string,

       % File names of the attachments, relative to the "attachments" directory (if present).
       mp_attachments  :: list(filename)).



%--------------------------------------------------------------------------------
% The data about the MoinMoin and Tiki pages


% Read all the data about Moin and Tiki pages. The list of the Moin pages selected on the command
% line (argument 4) gets sorted. It does NOT contain any zombie pages. The Moinparts map, also, does
% not contain zombies. This predicate takes a set of Moin page names, which are to be ignored. They
% aren't included in the result arguments.

:- pred pages_data(
    params::in,                         % Command line arguments
    set(moinpage)::in,                  % MoinMoin pages to ignore
    maybe(io.text_output_stream)::in,   % Handle for page ignore list. Needed for zombies.

    pages_dirs::out,                    % The two mappings between all Moin page names and Moin directory
                                        % names.
    list(moinpage)::out,                % The sorted list of the selected Moin pages (with
                                        % --page/--prefix/--all) (except for zombie pages).
    map(moinpage, moinparts)::out,      % The Moin page name -> Moin page parts mapping for all pages
                                        % (except for zombie pages).

    % Moin page name to Tiki page name resolver
    (func(moinpage) = tikipage)::out(func(in) = out is det),

    % Tiki page name to Moin page name backwards resolver
    (func(tikipage) = moinpage)::out(func(in) = out is det),

    io::di, io::uo
) is det.


% Parse all the selected Moin pages and convert the link targets from raw to Tiki pagenames.

:- pred parse_pages_full(
    % All the static data
    m2t::in,

    % The parsed pages, pairs of Moin page name and parse result
    assoc_list(tikipage, page(target(pages.tikipage)))::out,

    io::di, io::uo)
is det.



%--------------------------------------------------------------------------------
% Sorting and comparision

:- func moinpage_collate(moinpage, moinpage) = comparison_result.



%--------------------------------------------------------------------------------
% Implementation
%--------------------------------------------------------------------------------

:- implementation.
:- import_module exception, bool, pair.
:- import_module posix, posix.opendir, posix.closedir, posix.readdir, posix.strerror.
:- import_module tools, dir_contents, unicode, parsertools, actions.


:- instance pagename(moinpage) where [
    to_str(moinpage(Str)) = Str
%%%    from_str(Str) = moinpage(Str)
].

:- instance pagename(tikipage) where [
    to_str(tikipage(Str)) = Str
%%%    from_str(Str) = tikipage(Str)
].



pages_data(Params, Ignore, IgnoreOut,                                               % in
           Pages_Dirs, SelectedMoinPages, MoinpartsMap, Resolve, BackwardsResolve,  % out
           !IO) :-

    Prefix = p_prefix(Params),
    Pagesdir = p_pagesdir(Params),

    io.write_string("Inspecting MoinMoin pages...\n", !IO),
       
    % Get the two Moin page name <-> directory name maps.
    % Read the pages directory for all the page names
    posixerr2ex(Pagesdir, dir_contents(Pagesdir), Dirs, !IO),

    foldl((pred(Dir::in,
                ( NameDirM  - DirNameM  )::in,
                ( NameDirM1 - DirNameM1 )::out)
           is det :-
               map.det_insert(unmoin(Dir), Dir, NameDirM, NameDirM1),
               map.det_insert(Dir, unmoin(Dir), DirNameM, DirNameM1)),
          Dirs : list(string),
          null_pages_dirs,
          Pages_Dirs : pages_dirs
         ),

    % Read the Moin page parts for all pages in the pages directory, including zombies.
    % MoinpartsMap0 is the mapping of Moin page names to the moinparts, for all pages from the pages
    % directory.
    read_moinparts(Pages_Dirs, Pagesdir,
                   MoinpartsL0 : list(moinparts), % List of all moinparts, including zombie pages
                   !IO),
    list.map(pred(MP::in, (mp_moinpagename(MP) - MP)::out) is det,
             MoinpartsL0,
             MoinpartsAssocList),
    map.from_assoc_list(MoinpartsAssocList, MoinpartsMap0),

    % Filter into regular pages and zombie pages and ignored pages (via --ignore-list-read). Pages
    % in the Ignore set are silently filtered out, with no message.
    foldl4(
        (pred(
            Moinparts::in,
            RegularIn::in, RegularOut::out,
            IgnoredIn::in, IgnoredOut::out,
            ZombiesIn::in, ZombiesOut::out,
            IOin::di, IOout::uo)
         is det :-
            some [!IOx] (
                !:IOx = IOin,
                MoinPage = Moinparts ^ mp_moinpagename,
                (
                    if   set.member(MoinPage, Ignore)

                    then % Page ignored by ignore list (--ignore-list-read)
                         RegularOut = RegularIn,
                         IgnoredOut = set.insert(IgnoredIn, MoinPage),
                         ZombiesOut = ZombiesIn

                    else ( if   Moinparts ^ mp_current \= "",
                                Moinparts ^ mp_revisions \= [],
                                member(Moinparts ^ mp_current, Moinparts ^ mp_revisions)

                           then % Regular, non-ignored, non-zombie page
                                RegularOut = set.insert(RegularIn, MoinPage),
                                IgnoredOut = IgnoredIn,
                                ZombiesOut = ZombiesIn

                           else % Zombie page
                                ( if   Params ^ p_nozombiemsg = yes
                                  then true
                                  else io.format("Ignoring zombie page: %s\n", [s(moinpage(MoinPage))], !IOx),
                                       ( IgnoreOut = yes(Stream),
                                         io.write_string(Stream, moinpage(MoinPage), !IOx),
                                         io.nl(Stream, !IOx)
                                       ; IgnoreOut = no
                                       )
                                ),
                                RegularOut = RegularIn,
                                IgnoredOut = IgnoredIn,
                                ZombiesOut = set.insert(ZombiesIn, MoinPage)
                         )
                ),
                IOout = !.IOx
            )
        ),
        MoinpartsL0 : list(moinparts),       % in
        set.init, Regular : set(moinpage),   % Number of regular pages (excluding ignored pages)
        set.init, Ignored : set(moinpage),   % Number of ignored pages (which are present in pagesdir)
        set.init, Zombies : set(moinpage),   % Number of zombie pages, which aren't yet ignored by
                                             % the ignore list
        !IO),
        
    ( if   not(set.is_empty(Zombies)),
           Params ^ p_nozombiemsg = no
      then io.nl(!IO)
      else true
    ),

    % Build the map Moin page name -> Moinparts. This map contains all pages except for zombies. It
    % includes ignored pages.
    NonZombies = set.union(Regular, Ignored),

    list.filter_map(
        (pred(MoinPage1::in, (MoinPage1 - MP)::out) is semidet :-
            map.lookup(MoinpartsMap0, MoinPage1, MP)),
        set.to_sorted_list(NonZombies),
        MoinpartsAssocList1),
    map.from_assoc_list(MoinpartsAssocList1, MoinpartsMap),

    % Get the selected pages from the list of all pages
    (
      % Pages specified with "--page"
      if   p_pages(Params) \= []
      then SelectedMoinPages0 = p_pages(Params)

      % All pages with a prefix
      else if Prefix \= ""
      then filter((pred(moinpage(Pagename)::in) is semidet :-
                       string.append(Prefix, _, Pagename)),
                   set.to_sorted_list(NonZombies),
                   SelectedMoinPages0)
 
      % All pages
      else if p_all(Params) = yes
      then SelectedMoinPages0 = set.to_sorted_list(NonZombies)

      % No pages selected. That's OK.
      else SelectedMoinPages0 = []
    ),

    % Remove zombie pages and pages to ignore from the selected MoinMoin pages.
    list.delete_elems(SelectedMoinPages0,
                      set.to_sorted_list(Ignore),
                      SelectedMoinPages1),
    SelectedMoinPages = list.sort(moinpage_collate, SelectedMoinPages1),

    % Print statistics.
    io.format("Number of all pages: %i\n", [i(length(MoinpartsL0))], !IO),
    io.format("Number of regular pages (excluding ignored pages): %i\n", [i(set.count(Regular))], !IO),
    io.format("Number of ignored pages (which are present in pagesdir): %i\n", [i(set.count(Ignored))], !IO),
    io.format("Number of zombie pages, which aren't ignored yet via the ignore list: %i\n",
              [i(set.count(Zombies))], !IO),
    io.format("Number of selected pages (excluding ignored pages): %i\n", 
              [i(list.length(SelectedMoinPages))], !IO),
    io.nl(!IO),

    % Abort when there are no pages left
    ( if   SelectedMoinPages = []
      then throw("No pages to process.")
      else true
    ),

    % Transform the MoinMoin page names to Tiki page names for all the pages which should be
    % processed.
    pagenames.build_resolver(Params, set.to_sorted_list(NonZombies), Resolve, BackwardsResolve, !IO).




parse_pages_full(M2T,             % in
                 ParsedPages,     % out
                 !IO) :-
    MoinPages = M2T ^ m2t_selectedmoinpages,
    map_foldl(parse_page(M2T),
              MoinPages,
              ParsedPages,
              !IO),
    io.nl(!IO).


:- pred parse_page(
    m2t::in,
    moinpage::in,
    pair(tikipage, page(target(tikipage)))::out,
    io::di, io::uo)
is det.

parse_page(M2T, MoinPage, ParsedPage, !IO) :-
    io.write_string("Reading page " ++ quote_qm(moinpage(MoinPage)) ++ "...\n", !IO),
    File = actions.moin_file_current(M2T, MoinPage),
    contents_ex(File, Txt, !IO),
    parse_full(M2T, MoinPage, Txt, Parsed),
    TikiPage = (M2T ^ m2t_resolve)(MoinPage),
    ParsedPage = TikiPage - Parsed.



moinpage_collate(moinpage(A), moinpage(B)) = glib_utf8_collate(A,B).



% Read the moinparts (file system information) of all the MoinMoin pages in the pages directory.

:- pred read_moinparts(
       pages_dirs::in,           % The two Moin page names <-> dir names mappings
       string::in,               % Path of the MoinMoin "pages" directory
       list(moinparts)::out,     % List of all the moinparts.
       io::di, io::uo)
   is det.

read_moinparts(Pages_Dirs, Pagesdir, MoinpartsL, !IO) :-
    AllMoinPages = map.keys(fst(Pages_Dirs)),

    % Read in all the specified pages
    map_io(read_moinparts1(Pages_Dirs, Pagesdir),
           AllMoinPages,
           MoinpartsL,
           !IO).


% Read the Moin parts of one MoinMoin page. In case there is no "revisions" directory, store [] as
% the revisions. If there is no "current" file, store "" as the current revision. In both cases, we
% regard the page as a zombie page and ignore it later (in pages.pages_data).

:- pred read_moinparts1(
       pages_dirs::in,       % The two Moin page names <-> dir names mappings
       string::in,           % Pages directory
       moinpage::in,         % Page name
       moinparts::out,       % Moin parts of the page
       io::di, io::uo)
   is det.

read_moinparts1(Pages_Dirs,
                Pagesdir,
                Pagename,
                moinparts(Pagename, Revisions, Current, Attachments),
                !IO) :-

    % Name of the MoinMoin page directory (without path)
    Dirname = moin(Pages_Dirs, Pagename),

    % The list of revisions of the page
    Revdir = Pagesdir ++ "/" ++ Dirname ++ "/revisions",
    dir_contents(Revdir, Res1, !IO),
    ( Res1 = ok(Revisions)
    ; Res1 = error(Err1),
      ( if   Err1 = eNOENT
        then Revisions = []
        else strerror(Err1, Msg, !IO),
             throw(Revdir ++ ": " ++ Msg)
      )
    ),

    % The current revision
    contents(Pagesdir ++ "/" ++ Dirname ++ "/current", Res2, !IO),
    (
      Res2 = ok(Current0),
      Current = chomp(Current0)
    ;
      Res2 = error(_Err2),
      Current = ""
    ),

    % Full name of the MoinMoin page directory (with path)
    Dirname_full = Pagesdir ++ "/" ++ Dirname ++ "/attachments",

    % Read the list of attachments of the MoinMoin page. It's okay if the page doesn't have an
    % attachements directory. In this case, the list of attachments is empty.
    dir_contents(Dirname_full,
                 Result,
                 !IO),
    (
        Result = posix.ok(Att),
        Attachments = map((func(Filename) = filename(Filename)), Att)
    ;
        Result = posix.error(Err),
        ( if   Err = eNOENT
          then Attachments = []
          else strerror(Err, Msg, !IO),
               throw(Dirname_full ++ ": " ++ Msg)
        )
    ).

