These instructions lead you to the migration of your MoinMoin installation to Tiki. If something isn't 
understandable, please see the [Overview](Overview.html).


## Prepare the Tiki Server for Moin2Tiki

You need to prepare the Tiki server, so it will work correctly with the pages that Moin2Tiki generates. See
[Preparations](Preparations.html).


## Prepare the MoinMoin Server

MoinMoin stores all pages in the `pages` directory. If you're using MoinMoin as a desktop wiki (no installation),
then you start the MoinMoin server by calling the `wikiserver.py' script. In this case, the pages directory is
`wiki/data/pages`, under your MoinMoin directory. Your MoinMoin directory tree is then owned by your normal Unix
user.

Moin2Tiki must be called as the (Apache) WWW user (normally "www-data"). Therefore, you must grant this user read
rights to the `pages` directory. You can do this, for instance, by changing the user of the pages directory (and
its contents) to "www-data". Or you add "www-data" to your Unix user's group, in /etc/group.

When you get a "permission denied" error when calling Moin2Tiki, this probably means that it doesn't have read
permission for the "pages" directory.


## Compile Moin2Tiki (optional)

With Moin2Tiki comes with an AppImage. This means that it brings along all needed libraries and will run on any
Linux machine. Just rename the `appimage/moin2tiki-*-x86_64.AppImage` file to `moin2tiki` and you're fine.

If you want to compile it yourself, see [Compiling Moin2Tiki](Compiling Moin2Tiki.html).


## Prepare Moin2Tiki

The following commands are, ready to use, in the `create.sh` script.

Moin2Tiki must be run as the WWW user (normally "www-data"). So you must place the needed support files somewhere
the WWW user can access them. Let's make a directory `/tmp/moin2tiki` to store `moin2tiki` and all extra files
there.

```
sudo mkdir -p /tmp/moin2tiki
```

Now let's copy the main program, `moin2tiki`, and the support files there. There's a script called `moin2tiki.sh`,
which we'll use later to call Moin2Tiki.

```
sudo cp -f moin2tiki moin2tiki.sh pnrepl /tmp/moin2tiki
sudo touch /tmp/moin2tiki/ignore-list
```

These files all need to be readable and writeable by the `moin2tiki` program. Therefore we hand them over to the
WWW user:

```
sudo chown -R www-data:www-data /tmp/moin2tiki
sudo chmod -R u=rwX,go= /tmp/moin2tiki
```


## Prepare the `moin2tiki.sh` Script

Edit the shell script `/tmp/moin2tiki/moin2tiki.sh`. You need to fill in the following information:

* The full path to MoinMoin's "pages" directory. That's the place from which the MoinMoin pages will be read.
* The Tiki user name, see [Preparations](Preparations.html).
* The language of the pages which are being migrated, see [Preparations](Preparations.html).

You can also change the path separator. The script uses `::`. Just edit the `--pathsep=::` line.


## Run Moin2Tiki

Now we're ready to run Moin2Tiki! First we test the setup, with the following command, which should list all the
Tiki page names, which correspond to the MoinMoin page names.

```
/tmp/moin2tiki/moin2tiki.sh listtiki --all
```

You should have gotten some messages about zombie pages, some statistics and the list of Tiki page names. When you
call the script again, you won't see the zombie pages any longer, because they have been placed in the ignore list.

When everything works as expected, you're ready to migrate your MoinMoin pages. But first you should make a
backup of your Tiki database. Run this command as root:

```
mysqldump tiki > tiki.sql.bak
```

You can restore the backup with (as root):

```
mysql tiki < tiki.sql.bak
```

Now we're ready to migrate your pages from MoinMoin to Tiki:

```
/tmp/moin2tiki/moin2tiki.sh migrate --all
```

Check your Tiki, the pages should be there!
