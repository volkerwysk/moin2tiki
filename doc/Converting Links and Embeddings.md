The documentation about link end embed parameters in MoinMoin and Tiki is scattered and probably
incomplete. So I'm collecting here what I've found. 

## Forms of Links and Embeddings in MoinMoin

* Form no. 1 - MoinMoin links - `[[`<i>link target</i>`|`<i>link text</i>`|`<i>parameters</i>`]]`. The link 
  target can be a MoinMoin page or an external URL.
* Form no. 2 - MoinMoin embeddings - `{{`<i>embed target</i>`|`<i>embed text</i>`|`<i>parameters</i>`}}`
* Form no. 3 - MoinMoin links with an embeded object as the link text (such as a clickable image) - 
  `[[`<i>link target</i>`|{{`<i>embed target</i>`|`<i>embed text</i>`|`<i>embed parameters</i>`}}|`<i>link parameters</i>`]]`

## Forms of Links and Embeddings in Tiki

* Tiki internal links - `((`<i>page</i>`|`<i>link text</i>`))` or `((`<i>page</i>`|#`<i>subsection</i>`|`<i>link text</i>`))`. Tiki
  does **not** (directly) support any link parameters for internal links.
* Tiki external links - `[`<i>URL</i>`|`<i>link text</i>`|`<i>link parameters</i>`]`. The *URL* can be a fully qualified URL or
  the local part, relative to the web server's document root.

## Places to Look

* [https://moinmo.in/HelpOnLinking](https://moinmo.in/HelpOnLinking)
* [https://moinmo.in/HelpOnImages](https://moinmo.in/HelpOnImages)
* [https://html.spec.whatwg.org/multipage/text-level-semantics.html#the-a-element](https://html.spec.whatwg.org/multipage/text-level-semantics.html#the-a-element)
* [https://wiki.selfhtml.org/wiki/HTML/Elemente/a](https://wiki.selfhtml.org/wiki/HTML/Elemente/a)
* [https://doc.tiki.org/Wiki-Syntax-Links](https://doc.tiki.org/Wiki-Syntax-Links)
* [https://doc.tiki.org/Wiki-Syntax-Lists](https://doc.tiki.org/Wiki-Syntax-Lists)
* [https://doc.tiki.org/PluginImg](https://doc.tiki.org/PluginImg)
* [https://doc.tiki.org/PluginDiv](https://doc.tiki.org/PluginDiv)
* [https://wiki.selfhtml.org/wiki/HTML/Elemente/div](https://wiki.selfhtml.org/wiki/HTML/Elemente/div)
* [https://html.spec.whatwg.org/multipage/grouping-content.html#the-div-element](https://html.spec.whatwg.org/multipage/grouping-content.html#the-div-element)



## MoinMoin and Tiki

MoinMoin paremters are case sensitive (so it seems).

Contrary to what the HelpOnLinking MoinMoin page says, embed parameters in MoinMoin **must be
separated by commas** and not by spaces.

In Tiki, the link parameters of an external link are separated with commas. Internal links don't
have parameters.

Only images can be embedded in Tiki, by using the IMG plugin. This plugin accepts paramters for
linking to an URL, or to a Tiki page, with the image. Such that the URL or Tiki page is opened when
the user clicks on the image.



## MoinMoin Link Parts and their Transformation to Tiki

This applies to forms no. 1 and 3.

**link target**: The page which is to be opened. Can be a MoinMoin page or an external URL.

The link target is translated to the first part of the Tiki link. This holds for internal and
external Tiki links. Note that, if there's a subsection part in the MoinMoin link, it goes to the
*second* part of the internal Tiki link, which then has three parts (not two).


**link text**: The label/description of the link. A MoinMoin link has a link text only in form no.
1. In form no. 3, there's the embedding instead.

The link text (form no. 1 only) becomes the Tiki link's link text. This means it goes
to the second or third part of the Tiki Link. When there's a subsection, it goes to the third part,
when not, it goes to the second part. 


**target** (the parameter): See
[HTML/Attribute/target](https://wiki.selfhtml.org/wiki/HTML/Attribute/target) (Self-HTML).
`target=_blank` opens in a new window or tab. This the only reasonable value for `target`, for
MoinMoin and Tiki, because `target` is for framesets and iframes otherwise, which (seemingly) aren't
used by both. 

In Tiki, the `target` link parameter opens the link in a new window. But
it is supported for fully qualified external links only. It would be possible to work around this
limitation with "jQuery" (Plugin JQ), but that would be too complicated for a limited cause. So I'm
transforming the MoinMoin `target` parameter to the Tiki `target` parameter only for external links.

When adding the `target` parameter in Tiki, it's part of the link, but it doesn't take a value. It
looks like this: `[`<i>URL</i>`|`<i>Description</i>`|target]`.


**&do=get**: "A direct download link". This bypasses the MoinMoin download screen which would
normally occur. It seems to make sense for links to attachments only. For external links, it
doesn't seem to have any effect.

A "direct download link" doesn't make sense for Tiki - all links to attachments are
"direct download links". So how are links to attachments to be done? It could be by invoking the
File Plugin or the Attachment Plugin. I'm using the former. This is the code which is produced:

`{file type="attachment" name="`<i>attachment file name</i>`" page="`<i>page name</i>`" desc="`<i>link text</i>`"}`

*Page name* is the name of the Tiki page which holds the attachment. *Attachment file name* is (of
course) the filename of the attachment. I couldn't find a way to specify the file by attachment ID,
so I use the filename.



**class**: This is used as a "style sheet selector". It is used to apply style, as specified in a
CSS style sheet, to the link. See [7.5.2 Element identifiers: the id and class
attributes](https://www.w3.org/TR/html401/struct/global.html#h-7.5.2) in the HTML 4.01
Specification. Several classes can be specified by separating them with spaces.

In Tiki, the class(es) can be specified by wrapping the link in an invocation of the Tiki Div Plugin:

`{DIV(class="`<i>class(es)</i>`")}((`<i>link target</i>`|`<i>link text</i>`)){DIV}`
<br>`{DIV(class="`<i>class(es)</i>`")}[`<i>link target</i>`|`<i>link text</i>`|`<i>parameters</i>`]{DIV}`

Multiple classes are to be separated by white space.

In Tiki, internal links can't have parameters. And the
[Wiki-Syntax Links](https://doc.tiki.org/Wiki-Syntax-Links) page says nothing about a `class`
parameter for external links. So I'm just wrapping both in an invocation of the Div Plugin.


**title**: "Advisory information for the element". When added to a link, "this could be the title or
a description of the target resource". See [3.2.6.1 The title
attribute](https://html.spec.whatwg.org/multipage/dom.html#the-title-attribute) in the HTML
standard. This is rendered as a tool tip, which appears when the mouse hovers over the link.

In Tiki, this can be transformed like the *class* parameter:

`{DIV(title="`<i>title</i>`")}((`<i>link target</i>`|`<i>link text</i>`)){DIV}`
<br>`{DIV(title="`<i>title</i>`")}[`<i>link target</i>`|`<i>link text</i>`|`<i>parameters</i>`]{DIV}`


**accesskey**: "The accesskey attribute's value is used by the user agent as a guide for creating a keyboard shortcut that activates or focuses the element.". See [6.7.2 The accesskey attribute](https://html.spec.whatwg.org/multipage/interaction.html#the-accesskey-attribute).

This isn't supported by Tiki and therefore ignored.


### Creating a query string for the target URL

*This isn't supported by moin2tiki.*

See [HelpOnLinking](https://moinmo.in/HelpOnLinking).

"What is possible for this depends on the target site."

This creates a link with "?" and the parameters at the end, separated with ampersands. This works
for non-external links too, where you normally have no possibility to specify the query string
directly, as part of the target URL. The parameter names must begin with an ampersand.

"Example: '[[MoinMoin:MoinMoinWiki|MoinMoin Wiki|&action=diff,&rev1=1,&rev2=2]]'"

produces: [MoinMoin Wiki](https://moinmo.in/MoinMoinWiki%3Frev1%3D1%26action%3Ddiff%26rev2%3D2)

* "Giving query string items does not work when you give a URL as target (but for links to pages or attachments)."
* "If you give a URL as target, you can include a query string directly in that target, no need for params."



## MoinMoin Embedding Parts and their Transformation to Tiki

This applies to forms no. 2 and 3. 

**embed target**: This is the address of the embedded object. Most likely, it begins with `attachment:`.

This is translated to the `attId` parameter of the IMG plugin, which holds the 
attachment ID. See [Attachments](Attachments.html).


**embed text**: The second part of an embed statement (the description). It holds content, which
will go into the `title` attribute of the `<a>` element on the HTML page. "It will display when you
hover the mouse over the image". See [here](https://moinmo.in/HelpOnImages#Title_Text).

This is translated to the `title` parameter.



**align**: "one of `top`, `middle`, `bottom`, `right`, `left` or `center`" (See
[HelpOnImages](https://moinmo.in/HelpOnImages)). `center` is a synonym for `middle`, but it isn't
valid HTML, strictly speaking. But it's understood by most (all?) web browsers.

The `right` and `left` values directly go to the Tiki `align` argument of the IMG plugin.
The `top`, `middle` and `bottom` values are tranformed to `stylebox="vertical-align:top;"`,
`stylebox="vertical-align:middle;"` and `stylebox="vertical-align:bottom;"`. `center` is translated
to `middle`.


**width**: Width of the embedded object, usually an image

Same in Tiki as in MoinMoin.


**height**: Height of the embedded object, usually an image

Same in Tiki as in MoinMoin.


**class**: This isn't documented, but embedded objects can have one more a classes (separated by
space characters). This is the same as the link parameter `class` (see above).

In Tiki, this is directly given to the `class` parameter of the IMG plugin.



