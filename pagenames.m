% Here is the conversion of MoinMoin directory names to MoinMoin page names and back. Here's also
% the resolving of MoinMoin links to Tiki links and vice versa.
%
% In MoinMoin, the pages are stored in directories, whose names are derived form the names of the
% pages. In doing so, a lot of characters get represented by hexadecimal byte sequences in round
% brackets. The module pagenames undoes this and convers the directory names back to page names.
%
% How UTF-8 encoding works:
% https://www.johndcook.com/blog/2019/09/09/how-utf-8-works/

:- module pagenames.

%================================================================================
% Interface
%================================================================================

:- interface.
:- import_module string, io, map, list, char, pair.
:- import_module moin2tiki, pages.


% Mapping of page names <-> directory names

:- type pages_dirs ==
    pair(map(moinpage,          % MoinMoin page name
             string),           % MoinMoin directory name
         map(string,            % MoinMoin directory name
             moinpage)).        % MoinMoin page name

:- func null_pages_dirs = pages_dirs.


%--------------------------------------------------------------------------------
% MoinMoin directory names and the page names which they encode


% Convert a directory name back to a MoinMoin page name.

:- pred unmoin(string::in,        % Directory name
               moinpage::out)     % Page name
   is det.

:- func unmoin(string) = moinpage.


% Convert a MoinMoin page name back to a MoinMoin directory name

:- func moin(pages_dirs, % Pages mapping
             moinpage)     % MoinMoin page name
   = string.             % MoinMoin directroy name



% Get the page name by directory name. Fail if it isn't found.

:- func dir_to_page(pages_dirs, string) = moinpage is semidet.


% Get the page name by directory name. Throw an exception if it isn't found.

:- func dir_to_page_ex(pages_dirs, string) = moinpage is det.


% Get the directory name by page name. Fail if it isn't found.

:- func page_to_dir(pages_dirs, moinpage) = string is semidet.


% Get the directory name by page name. Throw an exception if it isn't found.

:- func page_to_dir_ex(pages_dirs, moinpage) = string is det.



%--------------------------------------------------------------------------------
% Tiki page names


% The list of "problematic" characters inside Tiki page names.
:- func problematic = list(char).


% This generates the Tiki page names from the Main page names, with the help of the replacements
% file. The replacement rules in the replacements file are used to transform the characters, which
% are allowed in MoinMoin page names, but not allowed in Tiki page names. The predicate also takes
% care of name clashes in the Tiki page names, by appending a number to ambigious Tiki page names.
%
% MoinMoin allows more characters in page names than Tiki. In MoinMoin all characters, except for
% the slash, can be used. Tiki has much more restricted rules for the characters allowed in page
% names. The "problematic" contstant lists the characters which can't be used in Tiki page names.
%
% This means that name clashes can occur, when two different MoinMoin page names are translated to
% the same Tiki page name. In order to disambiguate the Tiki version of the page name, numbers are
% added, such that each MoinMoin page name becomes one unique Tiki page name. For this to work, all
% MoinMoin page names must be known when they are translated to Tiki page names, even page names
% which aren't selected on the command line. The entire directory, which is specified with the
% --pagesdir command line argument, is used for generating the Tiki from MoinMoin page names.
%
% The folloging predicate creates a unique Tiki page name (with numbers at the end, when needed) for
% each MoinMoin page name. It returns a resolver predicate, which returns the Tiki Page name from
% the MoinMoin page name.

:- pred build_resolver(
       params::in,                    % Command line arguments
       list(moinpage)::in,            % All Moin page names in the pages directory, must be unique

       % Moin page name to Tiki page name resolver
       (func(moinpage) = tikipage)::out(func(in) = out is det),

       % Tiki page name to Moin page name backwards-resolver
       (func(tikipage) = moinpage)::out(func(in) = out is det),

       io::di, io::uo)
   is det.



% Resolve a Moin page link, which may be relative, to a absolute Moin page link. Relative MoinMoin
% links can start with a slash, then it leads to a subpage. They can also start with one or more
% ".." components. Then they refer to a sister page (with one "..") or a parent page (with several
% ".." components). ".."-components can occur only at the beginning in MoinMoin.

:- func make_link_absolute(
    % The Moin page in which the link occurs (for relative links). This must be absolute.
    moinpage,

    % Moin page name to be resolved, MAY BE RELATIVE
    string

) = moinpage.  % Same page name as an absolute link


%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module int, enum, exception, set, bool, assoc_list.
:- import_module posix, posix.strerror.
:- import_module dir_contents, tools, repl_parser, create.


null_pages_dirs = ( map.init - map.init ).


% Determine the number of bytes, which encode the Unicode character that follows in the list of
% bytes. The argument is the first byte of the sequence of bytes which encode the character. It can
% be one to four bytes long.

:- func numbytes(int) = int.

numbytes(I) = N :-
    ( if      I >> 3 = 0b11110  then N = 4
      else if I >> 4 = 0b1110   then N = 3
      else if I >> 5 = 0b110    then N = 2
      else                           N = 1
    ).


% Convert the hexadecimal two-character represenation of a byte to an integer. For instance,
% hex2byte('f', 'f', 255). MoinMoin uses lower case characters, so this also uses lower case
% characters, exclusively.

:- pred hex2byte(char::in, char::in, int::out) is semidet.

hex2byte(A, B, Byte) :-
    hex(A, X),
    hex(B, Y),
    Byte = X * 16 + Y.


:- pred hex(char::in , int::out) is semidet.

hex('0', 0).
hex('1', 1).
hex('2', 2).
hex('3', 3).
hex('4', 4).
hex('5', 5).
hex('6', 6).
hex('7', 7).
hex('8', 8).
hex('9', 9).
hex('a', 10).
hex('b', 11).
hex('c', 12).
hex('d', 13).
hex('e', 14).
hex('f', 15).


% Convert a sequence of bytes, which UTF-8 encodes a Unicode character, to the Unicode code point (a
% 32 bit integer). The MoinMoin directory names which represent MoinMoin page names, contain many
% such encoded characters, both within the ASCII range and without. They are enclosed in round
% brackets and represent one or several characters.
%
% For unhex/3, the two-character representations of bytes are already converted to 8-bit integers.
% It decodes the Unicode character at the beginning of the list, which is specified as the first
% argument. It returns the unicode code point and the rest of the list.

:- pred unhex(list(int)::in, int::out, list(int)::out) is semidet.

unhex(L, I, Rest) :-
    A = head(L),
    N = numbytes(A),
    ( N = 4,
      L = [A,B,C,D|Rest],
      I =    ((A /\ 0b111) << 18)
          \/ ((B /\ 0b111111) << 12)
          \/ ((C /\ 0b111111) << 6)
          \/ (D /\ 0b111111)

    ; N = 3,
      L = [A,B,C|Rest],
      I =    ((A /\ 0b1111) << 12)
          \/ ((B /\ 0b111111) << 6)
          \/ (C /\ 0b111111)

    ; N = 2,
      L = [A,B|Rest],
      I =    ((A /\ 0b11111) << 6)
          \/ (B /\ 0b111111)

    ; N = 1,
      I = A,
      Rest = tail(L)
    ).




% Apply a predicate to all pairs of elements in a list.
%
% For instance:  map_pairs(pred, [a,b,c,d], [A, B]) ,
% such that      pred(a,b,A), pred(c,d,B).
%
% Fails when the list contains an odd number of elements.

:- pred map_pairs(pred(A, A, B), list(A), list(B)).
:- mode map_pairs(pred(in, in, out) is semidet, in, out) is semidet.

map_pairs(_, [], []).

map_pairs(Pred, [A,B|Rest], [X|Rest1]) :-
    Pred(A,B,X),
    map_pairs(Pred, Rest, Rest1).


% Convert a sequence of bytes, which UTF-8 encodes one or several Unicode characters, to the Unicode
% code points (some 32 bit integers). The MoinMoin directory names which represent MoinMoin page
% names, contain many such encoded characters, both within the ASCII range and without. They are
% enclosed in round brackets and represent one or several characters.
%
% For unhexes/2, the two-character representations of bytes are already converted to 8-bit integers.
% It decodes the Unicode characters in the list, which is specified as the first argument. It
% returns the list of unicode code points.
%
% If the supplied list doesn't correctly encode one or several Unicode characters, then unhexes/2
% fails.

:- pred unhexes(list(int)::in, list(int)::out) is semidet.
unhexes([], []).

unhexes(L1, L2) :-
    L1 = [_|_],
    L2 = [I|Rest2],
    unhex(L1, I, Rest1),
    unhexes(Rest1, Rest2).


moin(Pages, MoinPage) = Dirname :-
    ( if   map.search(fst(Pages), MoinPage, Dirname0)
      then Dirname = Dirname0
      else throw("Error: Page " ++ quote_qm(moinpage(MoinPage)) ++ " not found.")
    ).



:- pred unmoin_l(list(char)::in, list(char)::out) is semidet.
:- pred unmoin_b(list(char)::in, list(char)::out, list(char)::out) is semidet.


unmoin(Str1, moinpage(Str2)) :-
    (if   to_char_list(Str1, L1),
          unmoin_l(L1, L2)
     then from_char_list(L2, Str2)
     else throw(string.format("Error: The following directory name doesn't correctly " ++
                              "encode a page name:\n\"%s\"", [s(Str1)])
                : string)
    ).


unmoin_l([], []).

unmoin_l(L1, L2) :-
    L1 = [X|Xs],
    ( if    X = '('
      then  unmoin_b(Xs, Chars1, [')'|Rest]),
            unmoin_l(Rest, Chars2),
            append(Chars1, Chars2, L2)
      else  L2 = [X|Rest],
            unmoin_l(Xs, Rest)
    ).

unmoin_b(L1, L2, Rest) :-
    list.take_while((pred(Z::in) is semidet :- Z \= ')'),
                    L1,
                    Hexdigits,
                    Rest),

    map_pairs(hex2byte, Hexdigits, Bytes),
    unhexes(Bytes, Codepoints),
    map( (pred(I::in, Z::out) is semidet :- Z = enum.from_int(I)),
         Codepoints,
         L2).


unmoin(A) = B :-
    unmoin(A, B).






dir_to_page(Pages_Dirs, Dir) =
    map.search(snd(Pages_Dirs), Dir).

dir_to_page_ex(Pages_Dirs, Dir) = Name :-
    ( if   Name0 = dir_to_page(Pages_Dirs, Dir)
      then Name = Name0
      else throw("Unknown page directory " ++ quote_qm(Dir))
    ).



page_to_dir(Pages_Dirs, Page) =
    map.search(fst(Pages_Dirs), Page).

page_to_dir_ex(Pages_Dirs, Page) = Dir :-
    ( if   Dir0 = page_to_dir(Pages_Dirs, Page)
      then Dir = Dir0
      else throw("Unknown page name " ++ quote_qm(moinpage(Page)))
    ).


% All "problematic" characters:
%problematic = [(':'), ('/'), ('?'), ('#'), ('@'), ('!'), ('$'), ('&'),
%               ('\''), ('('), (')'), ('*'), ('+'), (','), (';'), ('='), ('<'), ('>')].


% Only the forbidden ones. The quotation mark seem to be an allowed character. But Tik 24 has a bug
% which won't let you open pages, with quotation marks in the name, from the pages list. So I
% include it here. See this bug report:
% https://dev.tiki.org/item8172-Page-names-with-quotation-marks-can-t-be-opened-in-Pages-List

problematic = [('/'), ('?'), ('#'), ('['), (']'), ('@'), ('$'), ('&'), ('+'), (';'), ('='),
               ('<'), ('>'),
               ('"')].



% Do all the string replacements in a MoinMoin page name to create a Tiki page name.
:- pred replacements(
       assoc_list(string, string)::in, % Replacement rules - list of substring to be replaced, with
                                       % string to replace it with
       moinpage::in,                     % String on which the replacements are donw
       tikipage::out)                    % String with substrings replaced
   is det.

replacements(Repl, MoinPage, TikiPage) :-
    to_char_list(moinpage(MoinPage), MoinPageChr),

    % Do the rules in the replacement file
    map_state(replace, Repl, MoinPageChr, IntermChr),

    % Transform the problematic characters, which haven't been coped with a replacement rule. This
    % involves the To-side of the replacements, too.
    map_state((pred(Char::in, In::in, Out::out) is det :-
                   replace([Char], ['_'], In, Out)),
              problematic,
              IntermChr,
              TikiPageChr),
    TikiPage = tikipage(from_char_list(TikiPageChr)).


% Simple frontend to tools.replace/4
:- pred replace(pair(string, string)::in, list(char)::in, list(char)::out) is det.

replace(Rep, !Pagename) :-
    replace(to_char_list(fst(Rep)), to_char_list(snd(Rep)), !Pagename).




% Read the replacement rules file.
:- pred read_repl_file(
       params::in,                    % Command line arguments
       assoc_list(string, string)::out,   % Replacement rules - list of substring to be replaced, with
                                      % string to replace it with
       io::di, io::uo)
   is det.

read_repl_file(Params, Repl, !IO) :-
    contents_ex(p_pnreplfile(Params), Cnt, !IO),
    ( if   parse_repl_file(Cnt, Repl0)

      then
           % We handle the path separator as another replacement rule. It comes first, which means
           % that it shadows any replacepent rule of the Tiki path separator, "/". This means that
           % the path separator can be chosen on the command line, with "--pathsep", only.
           Repl = [ "/" - p_pathsep(Params) | Repl0]

      else throw("Parse error in the replacement rules file " ++ quote_qm(p_pnreplfile(Params)))
    ).





build_resolver(Params, AllMoinPages, Resolve, BackwardsResolve, !IO) :-
    read_repl_file(Params, Repl, !IO),
    foldl(build_resolver_1(Repl),
          AllMoinPages,
          ( map.init - set.init ),
          ( MoinTikiMap - _)),
    Resolve = resolve(MoinTikiMap, Repl),
    TikiMoinMap =
        map.from_assoc_list(
            assoc_list.reverse_members(
                map.to_assoc_list(MoinTikiMap))),
    BackwardsResolve = backwards_resolve(TikiMoinMap).



:- pred build_resolver_1(
    assoc_list(string,string)::in,               % Replacements list
    moinpage::in,                                % Moin page name

    % Moin-Tiki-page names map and the set of Tiki page names already in use - in
    pair(map(moinpage,tikipage), set(tikipage))::in,

    % Moin-Tiki-page names map and the set of Tiki page names already in use - out
    pair(map(moinpage,tikipage), set(tikipage))::out)
is det.

build_resolver_1(Repl, MoinPage, (MoinTikiMap0 - TikiSet0), (MoinTikiMap1 - TikiSet1)) :-
    replacements(Repl, MoinPage, TikiPage),
    (
      if   set.member(TikiPage, TikiSet0)

      then % The Tiki page name is already in use. Make it unique by appending a number.
           mointikimap_insert_with_number(
               MoinTikiMap0, TikiSet0, MoinPage, TikiPage, 2, MoinTikiMap1, TikiSet1)

      else % The Tiki page name is unique (so far). Insert it unchanged.
           map.det_insert(MoinPage, TikiPage, MoinTikiMap0, MoinTikiMap1),
           set.insert(TikiPage, TikiSet0, TikiSet1)
    ).


% Insert a key and value in the Moin-Tiki-Map, appending a number to make the Tiki page name unique.
:- pred mointikimap_insert_with_number(
       map(moinpage, tikipage)::in,          % MoinTikiMap in
       set(tikipage)::in,                 % Set of Tiki page names in
       moinpage::in,                      % Moin page name
       tikipage::in,                      % Tiki page name, without appended number
       int::in,                         % Next number to try
       map(moinpage,tikipage)::out,         % MoinTikiMap out
       set(tikipage)::out).               % Set of Tiki page names out

mointikimap_insert_with_number(MoinTikiMap0, TikiSet0,
                               MoinPage, TikiPage, Number,
                               MoinTikiMap1, TikiSet1) :-
    TikiPage1 = tikipage(tikipage(TikiPage) ++ " " ++ string.string(Number)),
    (
      if   set.member(TikiPage1, TikiSet0)
      then mointikimap_insert_with_number(MoinTikiMap0, TikiSet0,
                                          MoinPage, TikiPage, Number + 1,
                                          MoinTikiMap1, TikiSet1)
      else map.det_insert(MoinPage, TikiPage1, MoinTikiMap0, MoinTikiMap1),
           set.insert(TikiPage, TikiSet0, TikiSet1)
    ).



% Resolve Moin page name to Tike page name.

:- func resolve(
    % Moin page name to Tike page name map. Absolute page names.
    map(moinpage, tikipage),

    % Replacements list
    assoc_list(string, string),

    % Moin page to be resolved
    moinpage

) = tikipage.  % Tiki page

resolve(MoinTikiMap, Repl, MoinPage) = TikiPage :-
    ( if   map.search(MoinTikiMap, MoinPage, TikiPage0)

      then % Moin page is in the list of all pages in the pages directory.
           TikiPage = TikiPage0

      else % Moin page isn't in the list of all pages in the pages directory. This means it's
           % a dangling link. Convert it to Tiki anyway.
           replacements(Repl, MoinPage, TikiPage)
    ).


% Resolve the leading ".." components in a link, interpreting it relative to a page. ".."-COMPONENTS
% CAN OCCUR ONLY AT THE BEGINNING IN MOINMOIN.

:- pred resolve_dotdot(
       list(string)::in,        % The link as path components (see tools.slice_path/4), WITHOUT the
                                % leading ".." components
       list(string)::in,        % The page which the link is relative to, as path components (see
                                % tools.slice_path/4)
       int::in,                 % The number of leading ".." components, if any
       list(string)::out)       % The page referred to by the link, as path components
   is det.

resolve_dotdot(LinkC, PageC, Num, MoinLinkC) :-
    ( LinkC = [],
      ( if   Num = 0
        then MoinLinkC = PageC
        else resolve_dotdot(LinkC, skip_last(PageC), Num - 1, MoinLinkC)
      )
    ;
      LinkC = [_|_],
      (
        if   Num = 0
        then append(PageC, LinkC, MoinLinkC)
        else resolve_dotdot(LinkC, skip_last(PageC), Num - 1, MoinLinkC)
      )
    ).



% Resolver from Tiki page names to (absolute) Moin page names.
:- func backwards_resolve(
    map(tikipage, moinpage),
    tikipage
   ) = moinpage.

backwards_resolve(TikiMoinMap, TikiPage) =
    map.lookup(TikiMoinMap, TikiPage).




make_link_absolute(Parent, Link) = MoinPage :-
    slice_path(Link, LinkC, Relative, DotDotNum),

    ( if   LinkC = [], DotDotNum = 0

      then % Empty link. Refers to the same page.
           MoinPage = Parent

      else % Link with some page part.
           ( if   Relative = yes
             then slice_path(moinpage(Parent), ParentC, _, _),
                  resolve_dotdot(LinkC, ParentC, DotDotNum, MoinLinkC)
             else MoinLinkC = LinkC
           ),
           MoinPage = moinpage(join_list("/", MoinLinkC))
    ).
