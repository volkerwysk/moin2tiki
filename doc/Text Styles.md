See:<br>
[Help On Formatting](https://moinmo.in/HelpOnMoinFormatting) (MoinMoni)<br>
[Quick Reference - Basic Text Formatting](https://doc.tiki.org/tiki-index.php?page=Wiki-Syntax+Text&structure=Documentation#Quick_Reference_-_Basic_Text_Formatting) (Tiki)

|                 | MoinMoin     | Tiki                | Comments                               |
| --              | --           | ----                | ------------                           |
| italics         | `''`         | `''`                |                                        | 
| bold            | `'''`        | `__`                |                                        |
| underline       | `__`         | `===`               |                                        |
| subscript       | `,,`         | `{SUB()}`...`{SUB}` |                                        |
| superscript     | `^`          | `{SUP()}`...`{SUP}` |                                        |
| typewriter      | `` ` ``         | `{FONT(family=monospace)}`...`{FONT}` |  |
| inline code     | `{{{`...`}}}`| <span style="background-color: #DDDDDD;">&nbsp;` -+`</span>...`+-`  | There is a necessary space in front of the opening tag (see below).|
| big text        | `~+`...`+~`  | `{FONT(size=130%)}`...`{FONT}`    | Can't be nested in MoinMoin.  |
| small text      | `~-`...`-~`  | `{FONT(size=77%)}`...`{FONT}` | Can't be nested in MoinMoin.  |
| centered text   | not supported | `::`...`::`      | This is the default. See below. |
| strikethrough text | `--(`...`)--` | `--`...`--`  | | 

The typewriter opening tag, `-+`, works in Tiki only at the beginning of a line or word. Therefore
moin2tiki outputs a space in front of the `-+` tag. This will break up a word, when typewriter is to
begin *inside* a word, but this can't be avoided and should be very rare.

MoinMoin inline code can't span lines. It must be terminated on the same line, otherwise it isn't 
recognized.

The tags for big text can't be nested in MoinMoin (to make it even bigger). A "`~+`" tag won't be
recognised inside of already big text. It's the same for small text.

Tiki doesn't provide syntax for big or small text. In order to achive it, the Font Plugin is used.

MoinMoin doesn't have syntax for centered text.

The Tiki namespace separator can be set to `::`, in the control panels. But that, by default, is
used for centering text. In this case, it's advised to change the tag for centered text to `:::`.
So, when the namespace separator is set to `::`, then Moin2Tiki automatically assumes that `:::` is the
centered text tag. This affects quoting of MoinMoin text for Tiki.

The Tiki strikethrough syntax `--`...`--` seems to work only at the beginning of a word, for the
start tag. The end tag can occur anywhere. So it seems - it isn't documented. I translate the
MoinMoin strikethrough syntax directly to `--`...`--` nevertheless. This means that it won't work
when it doesn't begin at the beginning of a word, since this isn't supported by Tiki.

Scopes of text styles can overlap. They don't need to be nested in Tiki.
