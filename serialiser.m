%================================================================================
% Serialiser for converting Moin parse result ot Tiki wiki-syntax
%
% This takes the MoinMoin wiki syntax parse result and outputs it as Tiki wiki syntax.
% See https://doc.tiki.org/Wiki-Syntax
%================================================================================

:- module serialiser.

%================================================================================
% Interface
%================================================================================

:- interface.
:- import_module moin2tiki, pages, pretty, parser.
:- import_module list, assoc_list.


% Convert a page to Tiki syntax.

:- pred page1(
    parser.page(target(tikipage))::in,   % Page (parse result)
    m2t::in,                             % Collected data
                                         % Parse result for all selected pages
    assoc_list(tikipage, parser.page(parser.target(tikipage)))::in,
    string::out,                         % Tiki version of the page
    list(string)::out)                   % Messages for the user
is det.


% % Convert a list of inlines (continous text) to Tiki syntax.

% :- pred inlines1(
%     list(parser.inline(target(tikipage)))::in,  % The text to convert
%     m2t::in,                                    % Collected data
%     pretty.context::in,                         % The context, in which the inlines occur
%     string::out,                                % Tiki version of the text
%     list(string)::out)                          % Messages for the user
% is det.


% MoinMoin links can have ".." path components at the beginning. Those lead one level upwards, each.
% When we are already at the top directoty of the MoinMoin site, then those links lead OUT OF the
% MoinMoin directory on the web server. When there are no more path components in the URL of the
% MoinMoin site, they all lead to the same URL, with no path in it. We're NOT ALLOWING Tiki links to
% lead out of the Tiki site, so we resolve all extra ".." path components to lead to the top of the
% Tiki site.
%
% https://moinmo.in/HelpOnLinking
% https://html.spec.whatwg.org/multipage/text-level-semantics.html#the-a-element

:- pred do_link(
    parser.link(target(tikipage))::in,           % The Link
    ppstate::in, ppstate::out)
is det.


% List of the image file name extensions, which are supported by Tiki. In lower-case.
:- func img_exts = list(string).


%================================================================================
% Implementation
%================================================================================

:- implementation.
:- import_module exception, string, char, solutions, bool, int, maybe, map, require, pair.
:- import_module tools, macros, insts.


img_exts = [ ".bmp", ".jpg", ".gif", ".png", ".jpeg" ].



% Write a page to a Tiki-syntax string

page1([], _, _, "", []).

page1(Page@[_|_], M2T, ParsedPages, Str, Messages) :-
     pretty.write(paras(Page), M2T, ParsedPages, pretty.c_text, no, Str, Messages).



% % Write a list of inlines to a Tiki-syntax string

% inlines1([], _, _, "", []).

% inlines1(Inlines@[_|_], M2T, Context, Str, Messages) :-
%     pretty.write(inlines(Inlines), M2T, Context, no, Str, Messages).


:- pred inlines(
    list(parser.inline(target(tikipage)))::in,
    pretty.ppstate::in,
    pretty.ppstate::out)
is det.

inlines([], !S).

inlines([Inline|Inlines], !S) :-
    inline(Inline, !S),
    inlines(Inlines, !S).


%------------------------------------------------------------------------------------------------------------------
% Rendering a page. This copes with indentation.


% Format a page (a list of paragraphs) to text.
%
% This splits the list of paragraphs in non-indentable paragraphs and sequences of indentable
% paragraphs. The latter are processed such that the indentation in the output is generated.

:- pred paras(list(paragraph(target(tikipage)))::in,      % The paragraphs
              ppstate::in, ppstate::out) is det.

paras([], !S) :-
    true.

paras(Paragraphs@[Para1|Paras], !S) :-

    % Split the list of paragraphs in the indentable ones at the beginning and the rest, which starts with the
    % first non-indentable paragraph and goes to the end. Both can be empty.
    take_while(indentable,
               Paragraphs,
               Indentable,
               Rest),
    (
        % Doesn't begin with an indentable paragraph. Process and continue after this paragraph.
        Indentable = [],
        paragraph(Para1, 0, "", "", !S),
        paras(Paras, !S)
    ;
        % The paragraph list begins with a (non-empty) list of indentable paragraphs. Render those indentable
        % paragraphs, keeping track of indentation.

        % MoinMoin indents in the HTML page, by one level, paragraphs at the beginning of the page,
        % when they are indented. It does the same with paragraphs at the beginning of every block
        % of indentable paragraphs. It's as if there was an empty, non-indented text paragraph
        % at the beginning of each block. We simulate this by actually adding an empty paragraph,
        % if necessary.
        Indentable = [_|_],
        ( if   indentable(Para1),
               indentation(Para1) > 0
          then Indentable1 = [text(0,[])|Indentable]
          else Indentable1 = Indentable
        ),

        % Render the sequence of indentable paragraphs.
        indent_paras(0, 0, no, Indentable1, !S),

        % Continue with the rest
        paras(Rest, !S)
    ).


% Render a non-empty list of indentable paragraphs. The list is split to fragments. A fragment is a list of
% paragraphs, such that first paragraph (the head) has an indentation I, and the rest has an indentation bigger
% than I. That is, a fragment is a paragraph with a list of sub-paragraphs. Each fragment is rendered by
% indent_fragment/6. That predicate does that by rendering the head of the fragment with paragraph/6. The
% fragment's sub-paragraphs are rendered, again, by indent_paras/6.

:- pred indent_paras(
       % Real indentation level
       int::in,

       % How many opening indentation tags ("{INDENT()}") are to be finished in this call to indent_paras.
       int::in,

       % If yes, specifies that the caller is inside a list of the given (real) indentation level. If no, then we
       % aren't insode a list.
       maybe(int)::in,

       % The list of paragraphs to indent and render
       list(paragraph(target(tikipage)))::in(non_empty_list),

       ppstate::in, ppstate::out)
is det.

indent_paras(Level, Finish, WithinList, Paras, !S) :-

    % This splits up the non-empty list of paragraphs into one or several fragments.
    fragment_paras(Paras, Fragments),

    % Split off the last fragment.
    split_last1(Fragments, FragsFront, FragLast),

    % Render all fragments.
    foldl(indent_fragment(Level, 0, WithinList), FragsFront, !S),
%%%    foldl1(indent_fragment(Level, 0, WithinList), FragsFront, !S),
    indent_fragment(Level, Finish, WithinList, FragLast, !S).



% Render one fragment.
%
% The indentation level of the sub-paragraphs is the indentation level of the head paragraph plus one.

:- pred indent_fragment(
       % Real indentation level
       int::in,

       % How many opening indentation tags ("{INDENT()}") are to be finished in this call to indent_paras.
       int::in,

       % Iff we are inside a list - meaning after a list item - its (real) indentation level
       maybe(int)::in,

       % The fragment to render
       list(paragraph(target(tikipage)))::in,

       ppstate::in, ppstate::out)
is det.

indent_fragment(_Level, _Finish, _WithinList, [], !S) :-
    unexpected($pred, "empty fragments list").

indent_fragment(Level, Finish, WithinList, [Para1|SubParas], !S) :-
    (
        if
            Para1 = list_item(_,_,_)
        then
            WithinList1 = yes(Level1),
            Before = "",
            After = "",
            Finish1 = Finish,
            Level1 = Level - Finish
        else
            WithinList1 = WithinList,
            Before =
                (
                    if   Level > 0,
                         WithinList = yes(Lev0)
                    then tools.repeat("+", Lev0+1) ++ " "
                    else ""
                ),
            After =
                (
                    if   SubParas = []
                    then repeat("{INDENT}", Finish)
                    else "{INDENT()}"
                ),
            Finish1 =
                (
                    if   SubParas = []
                    then 0    % not used
                    else Finish + 1
                ),
            Level1 = Level
    ),

    % Render the first paragraph.
    paragraph(Para1, Level1, Before
        , After, !S),

    % Render the subparagraphs.
    (   SubParas = []
    ;   SubParas = [_|_],
        indent_paras(Level+1, Finish1, WithinList1, SubParas, !S)
    ).




% Split a list of paragraphs into fragments. That's just frontend to split_into_fragments/4. See there.

:- pred fragment_paras(
    list(paragraph(target(tikipage)))::in(non_empty_list),
    list(list(paragraph(target(tikipage))))::out(non_empty_list(non_empty_list)))
is det.

fragment_paras(ParaL, ParaLL) :-
    split_into_fragments(fragment1, ParaL, [], ParaLL).



% Split off one fragment of a list (of paragraphs) and return it, together with the rest of the list. A fragment is
% a paragraph together with its (direct or indirect) subparagraphs. Subparagraphs are the following paragraphs with
% a higher indentation than the first paragraph.

:- pred fragment1(
    list(paragraph(target(tikipage)))::in(non_empty_list),      % Paragraph list to split
    list(paragraph(target(tikipage)))::out(non_empty_list),     % First fragment
    list(paragraph(target(tikipage)))::out)                     % Rest of the list
is det.

fragment1([Para1|Paras], Sub, Sisters) :-
    Sub = [Para1|Sub1],
    take_while((pred(Para::in) is semidet :-
                    indentable(Para),
                    indentation(Para) > indentation(Para1)),
               Paras,
               Sub1,
               Sisters).


% Split a list (of paragraphs) into fragments. The provided predicate splits off the first fragment and returns it,
% together with the rest of the list. The split_into_fragments/4 predicate collects the split off fragments and
% makes the complete list be split into fragments.

:- pred split_into_fragments(
    pred(list(P), list(P), list(P))::pred(in(non_empty_list), out(non_empty_list), out) is det,
    list(P)::in(non_empty_list),
    list(list(P))::in(list(non_empty_list)),
    list(list(P))::out(non_empty_list(non_empty_list)))
is det.

split_into_fragments(Pred, Paras@[_|_], Akku, Frags) :-
    Pred(Paras, Frag, Rest),
    append1(Akku, [Frag], Akku1),
    (
        Rest = [],
        Frags = Akku1
    ;
        Rest = [_|_],
        split_into_fragments(Pred, Rest, Akku1, Frags)
    ).






%--------------------------------------------------------------------------------
% Paragraphs
%
% It depends on the type of the paragraph, if a trainling empty line is needed. Therefore this is
% done here.

%% :- pred paragraph0(paragraph::in, ppstate::in, ppstate::out) is det.
%% paragraph0(Para, !S) :-
%%     trace [ io(!IO) ]
%%           ( io.write_string("Para = ", !IO), io.write(Para, !IO), io.nl(!IO),
%%             io.write_string("!.S = ", !IO), io.write(!.S, !IO), io.nl(!IO)
%%           ),
%%     (paragraph(Para, !S),
%%      trace [ io(!IO) ]
%%            ( io.write_string("Para nachher = ", !IO), io.write(Para, !IO), io.nl(!IO),
%%              io.write_string("!.S = ", !IO), io.write(!.S, !IO), io.nl(!IO), io.nl(!IO)
%%            ),
%%      true).


:- pred paragraph(
       paragraph(target(tikipage))::in, % Paragraph to be rendered
       int::in,                         % Real indentation level
       string::in,                      % Output before the body of the paragraph (for indentable paragraphs)
       string::in,                      % Output after the body of the paragraph (for indentable paragraphs)
       ppstate::in, ppstate::out) is det.

paragraph(heading(Level, Txt), _, _, _, !S) :-
    pretty.context(c_heading, Ctx, !S),
    str(repeat("!", Level) ++ " " ++ Txt, !S),
    nl(!S),
    pretty.context(Ctx, _, !S).



% Code blocks with optional syntax highlighting
%
% See http://moinmo.in/HelpOnFormatting#Colorized_Code

paragraph(code_block(code_block(_, Lines, MaybeHighlight)), _Level, Before, After, !S) :-
    pretty.context(c_code_block, Ctx, !S),
    str(Before, !S),
    str("{CODE(wrap=1", !S),
    ( if   MaybeHighlight = yes({Syntax, Linenumbers})
      then str(", colors=" ++ Syntax ++ ", ln=" ++
               (if Linenumbers = yes then "1" else "0"),
               !S)
      else true
    ),
    str(")}", !S),
    foldl((pred(Line::in, In::in, Out::out) is det :-
              some [!S1] (
                  !:S1 = In,
                  pretty.text(Line, !S1),
                  pretty.nl(!S1),
                  Out = !.S1)),
          Lines,
          !S),
    str("{CODE}", !S),
    str(After, !S),
    nl(!S),
    pretty.context(Ctx, _, !S).


% A text paragraph must be terminated with two newlines, if it is to be rendered as a independent
% paragraph by Tiki. This means, there is an empty line. But this holds only for the indentation
% level of zero. If it is indented, then the code generated by the indentation plugin makes empty
% lines around the paragraph. Only one newline is needed.
%
% The case that the paragraph is empty is special. The parser doesn't return any empty paragraphs.
% An empty paragraph is introduced by the serialiser, in the paras/3 predicate, in the case that
% there are indented paragraphs at the beginning. It is done to get the behaviour of MoinMoin. See
% the comments in the paras/3 predicate.
%
% For the special case of an empty text paragraph (Inlines = []), the one or two newlines after
% must't be generated. It would create a gap at the beginning of the block of indentable paragraphs
% (such as at the beginning of the page or as at after a heading).
paragraph(text(_, Inlines), Ind, Before, After, !S) :-
    pretty.context(c_text, Ctx, !S),
    str(Before, !S),
    pretty.list(inline, noop, Inlines, !S),
    str(After, !S),
    ( if   Inlines = []
      then true
      else nl(!S),
           ( if   Ind = 0
             then nl(!S)
             else true
           )
    ),
    pretty.context(Ctx, _, !S).


paragraph(comment_block(Lines), _, Before, After, !S) :-
    pretty.context(c_comment_block, Ctx, !S),
    str(Before, !S),
    str("~hc~", !S),
    nl(!S),
    pretty.list((pred(L::in, A::in, C::out) is det :-
                     str(L, A, B),
                     nl(B, C)),
                noop,
                Lines,
                !S),
    str("~/hc~", !S),
    str(After, !S),
    nl(!S),
    pretty.context(Ctx, _, !S).


% Not needed. All the empty lines in the output of the parser are removed in postprocess.m. See
% postprocess_remove_empty_lines/2.
paragraph(empty_line, _, _, _, !S).


% Lists have their own indentation management. It would be possible to use the indentation plugin
% for nesting lists (generating only list items of level 0), but that leads to problems.
%
% See https://doc.tiki.org/Wiki-Syntax-Lists
paragraph(list_item(_, List_type, Inlines), Level, Before, After, !S) :-
    pretty.context(c_list_item, Ctx, !S),

    % The indentation of the list item
    translate_list_type(List_type, Tiki_list_type),
    ( Tiki_list_type = bullet,
      Char = ('*')
    ; Tiki_list_type = number,
      Char = '#'
    ),
    str(tools.repeat(string.from_char(Char), Level+1) ++ " ", !S),

    str(Before, !S),
    % The body of the list item
    pretty.list(inline, noop, Inlines, !S),
    str(After, !S),
    nl(!S),

    pretty.context(Ctx, _, !S).


% Definition list items (like normal list items) are recognized at the beginning of the line only.
% Therefore there must be a newline, so the ";" comes at the beginning. This only holds when there
% actually is any indentation. List items are different, in that they have their own nesting, and
% aren't nested with the help of the indentation plugin.
paragraph(def_list_item(_, Def, Inlines), Ind, Before, After, !S) :-
    pretty.context(c_def_list_item, Ctx, !S),
    ( if Ind > 0 then nl(!S) else true ),
    str(";" ++ Def ++ ":", !S),

    % Tiki doesn't allow definition list item with an empty definition. So we insert a space, if
    % needed.
    str(Before, !S),
    ( if   Inlines = []
      then str("~032~", !S)
      else pretty.list(inline, noop, Inlines, !S)
    ),
    str(After, !S),

    pretty.context(Ctx, _, !S),
    nl(!S).


% A horizontal rule. That's the HTML "<hr>" element. Tiki supports only one thickness, whereas
% MoinMoin supports varyious thicknesses. The "---" must be on a line by itself.
paragraph(horizontal_rule(_, Level), _, Before, After, !S) :-
    str("{DIV(class=>""moin hr" ++ string.string((Level - 3) : int) ++ """)}", !S),
    str(Before, !S),
    nl(!S),
    str("---", !S),
    nl(!S),
    str(After, !S),
    str("{DIV}", !S).


% A table.
paragraph(table(_, LLL), _Ind, Before, After, !S) :-
    pretty.context(c_table_cell, Ctx, !S),
    str(Before, !S),
    str("{DIV(class=>moin)}||", !S),
    pretty.list(listline, nl, LLL, !S),
    str("||{DIV}", !S),
    str(After, !S),
    pretty.context(Ctx, _, !S),
    nl(!S),
    nl(!S).

:- pred listline(list(list(inline(target(tikipage))))::in, ppstate::in, ppstate::out) is det.
listline(LL, !S) :-
    pretty.list(listcell, str("|"), LL, !S).

:- pred listcell(list(inline(target(tikipage)))::in, ppstate::in, ppstate::out) is det.
listcell(Inlines, !S) :-
    pretty.list(inline, noop, Inlines, !S).




% Get the indentation level of paragraphs.
:- func indentation(paragraph(target(tikipage))) = int.

indentation(heading(_,_))                       = 0.
indentation(code_block(code_block(Ind, _, _)))  = Ind.
indentation(text(Ind, _))                       = Ind.
indentation(comment_block(_))                   = 0.
indentation(empty_line)                         = 0.
indentation(list_item(Ind, _, _))               = Ind.
indentation(def_list_item(Ind, _, _))           = Ind.
indentation(horizontal_rule(Ind,_))             = Ind.
indentation(table(Ind, _))                      = Ind.


% If the paragraph can be indented in Moin pages
:- pred indentable(paragraph(target(tikipage))::in) is semidet.

indentable(heading(_,_))         :- false.
indentable(code_block(_))        :- true.
indentable(text(_,_))            :- true.
indentable(comment_block(_))     :- false.
indentable(empty_line)           :- false.
indentable(list_item(_,_,_))     :- true.
indentable(def_list_item(_,_,_)) :- true.
indentable(horizontal_rule(_,_)) :- true.
indentable(table(_,_))           :- true.


% Translation of MoinMoin list types to Tiki list types. Tiki has only two list type: bullet and
% number.

:- type list_type
   ---> bullet
   ;    number.

:- pred translate_list_type(parser.list_type::in, serialiser.list_type::out) is det.

translate_list_type(bullet, bullet).
translate_list_type(number, number).
translate_list_type(uppercase_letter, number).
translate_list_type(lowercase_letter, number).
translate_list_type(uppercase_roman, number).
translate_list_type(lowercase_roman, number).

translate_list_type(dot, _) :-
    throw("Bug in serialiser.m: Can't translate dot list type").




%--------------------------------------------------------------------------------
% Inlines (see parser.m)


:- pred inline(
    inline(target(tikipage))::in,
    ppstate::in,
    ppstate::out) is det.

inline(Inline, !S) :-
    ( if   active(ppstate_style_info(!.S)) = no
      then activate_styles(!S)
      else true
    ),
    inline_1(Inline, !S).

:- pred inline_1(inline(target(tikipage))::in, ppstate::in, ppstate::out) is det.

inline_1(word(Word), !S) :-
    word(Word, !S).

inline_1(macro(Macro), !S) :-
    call_macro_pred(!.S ^ ppstate_m2t, Macro, !S).

inline_1(link(Link), !S) :-     % An ordinary link or a link with an embedding as the link text.
    do_link(Link, !S).

inline_1(embed(Embed), !S) :-
    do_embed(Embed, !S).

inline_1(space, !S) :-
    space_natbotl(!S).

inline_1(italics, !S) :-
    apply_style_italics(!S).

inline_1(bold, !S) :-
    apply_style_bold(!S).

inline_1(underline, !S) :-
    apply_style_underline(!S).

inline_1(subscript, !S) :-
    apply_style_subscript(!S).

inline_1(superscript, !S) :-
    apply_style_superscript(!S).

inline_1(begin_small, !S) :-
    apply_style_begin_small(!S).

inline_1(end_small, !S) :-
    apply_style_end_small(!S).

inline_1(begin_big, !S) :-
    apply_style_begin_big(!S).

inline_1(end_big, !S) :-
    apply_style_end_big(!S).

inline_1(begin_stroke, !S) :-
    apply_style_begin_stroke(!S).

inline_1(end_stroke, !S) :-
    apply_style_end_stroke(!S).

inline_1(inline_code(Str), !S) :-
    str(" -+", !S),
    text(Str, !S),      % MoinMoin doesn't interpret markup inside inline code
    str("+-", !S).


%--------------------------------------------------------------------------------
% Words
%
% In words, all the Tiki markup must be quoted, such that words from the MoinMoin parse result don't
% do unexpected things in Tiki.

:- pred word(string::in, ppstate::in, ppstate::out) is det.

word(Word, !S) :-
    text(Word, !S).



%--------------------------------------------------------------------------------
% Converting Links from MoinMoin to Tiki.
%
% https://moinmo.in/HelpOnLinking
% https://doc.tiki.org/Wiki-Syntax-Links
% https://html.spec.whatwg.org/multipage/text-level-semantics.html#the-a-element

% :- pred do_link(
%     parser.link(target(tikipage))::in,           % The Link
%     ppstate::in, ppstate::out)
% is det.

do_link(Link, !S) :-
    register_link_parts(Link, LinkParts),
    (
        % Ordinary link with a description (aka link text), which can be empty.
        %
        % We need to work around a Mercury flaw. LinkParts2 gets a different instantiation than
        % LinkParts. This is needed for the call to make_link. See the thread "inst question" in
        % mercury-users of 2022-07-27 / 2022-07-28.

        LinkParts = link_parts(Target, LSubsection, LText, LTarget, LDoGet, LClass, LTitle),
        LText = left(_Desc),
        LinkParts2 = link_parts(Target, LSubsection, LText, LTarget, LDoGet, LClass, LTitle),
        make_link(LinkParts2, !S)
    ;
        % Link with an embedding as the link text (clickable image)
        lp_link_text(LinkParts) = right(EmbedParts),
        make_embed(EmbedParts, yes(LinkParts), !S)
    ).



% Output a link with textual description as the link text. In other words, an ordinary link,
% internal or external, with a textual label.

:- pred make_link(
    link_parts::in(lp_textlink),             % The link parts, as returend by register_link_parts/2
    ppstate::in, ppstate::out)
is det.

make_link(LinkParts, !S) :-
    LinkParts   = link_parts(Target, LSubsection, LText, LTarget, LDoGet, LClass, LTitle),
    LText       = left(Desc),
    ParsedPages = !.S ^ ppstate_parsed_pages,

    % Transformation of the class and title parameters
    (   LinkParts ^ lp_class = yes(Class),
        Div1 = ["class=" ++ quote_qm(Class)]
    ;   LinkParts ^ lp_class = no,
        Div1 = []
    ),
    (   LinkParts ^ lp_title = yes(Title),
        Div2 = ["title=" ++ quote_qm(Title)]
    ;   LinkParts ^ lp_title = no,
        Div2 = []
    ),

    ( if   Div1 \= [] ; Div2 \= []
      then DivBeginning = "{DIV(" ++ join_list(" ", Div1 ++ Div2) ++ ")}",
           DivEnd = "{DIV}"
      else DivBeginning = "",
           DivEnd = ""
    ),

    (
        %-------------------------------------------------------------------------------------------
        % A straight or dangling link to a wiki page (internal link). When the link is dangling, it
        % is translated to a Tiki link nevertheless, so the user can create a the page by clicking
        % on it.
        ( Target = t_wikipage(TikiPage) ; Target = t_dangling_wikipage(TikiPage) ),

        str(DivBeginning, !S),
        str("((" ++ tikipage(TikiPage), !S),

        (   LinkParts ^ lp_link_subsection = yes(Subsection),
            mutilate_subsection(ParsedPages, Target, Subsection, Subsection1),
            str("|#" ++ Subsection1, !S)
        ;   LinkParts ^ lp_link_subsection = no
        ),

        % We need to work around a Mercury flaw. LinkParts2 gets a different instantiation than
        % LinkParts. This is needed for the call to insert_description. See the thread "inst
        % question" in mercury-users of 2022-07-27 / 2022-07-28.
        LinkParts2 = link_parts(Target, LSubsection, LText, LTarget, LDoGet, LClass, LTitle),

        insert_description(LinkParts2, !S),
        str("))", !S),
        str(DivEnd, !S)
    ;
        %-------------------------------------------------------------------------------------------
        % A link to an external URL.
        Target = t_external(URL),

        str(DivBeginning, !S),
        M2T = !.S ^ ppstate_m2t,
        URLQuoted = pretty.tikitext_quote_text(M2T, c_external_link, no, string.to_char_list(URL)),
        str("[" ++ string.from_char_list(URLQuoted), !S),

        ( if LinkParts ^ lp_link_subsection = yes(Subsection)
          then str("#" ++ Subsection, !S)
          else true
        ),

        ( if   Desc = ""
          then true
          else str("|" ++ string.replace_all(Desc, "|", "~124~"), !S)
        ),

        % See https://doc.tiki.org/Wiki-Syntax-Links, search for "nocache".
        ( if   not(append(_ , ".html", URL) ; append(_ , ".htm", URL))
          then Paras1 = ["nocache"]
          else Paras1 = []
        ),

        % The "target=_blank" (MoinMoin) or "target" (Tiki, without parameter value) parameter
        ( LinkParts ^ lp_target = yes,
          Paras2 = Paras1 ++ ["target"]
        ;
          LinkParts ^ lp_target = no,
          Paras2 = Paras1
        ),

        ( if   Paras2 = []
          then true
          else str("|" ++ string.join_list(", ", Paras2), !S)
        ),

        str("]", !S),
        str(DivEnd, !S)

    ;
        %-------------------------------------------------------------------------------------------
        % A link to a non-dangling attachment. https://doc.tiki.org/PluginFile. Attachments
        % don't have subsections.
        Target = t_attachment(AttTikiPage, AttFilename),

        str(DivBeginning, !S),
        str("{file name=" ++ quote_qm(filename(AttFilename)) ++
            " page=" ++ quote_qm(tikipage(AttTikiPage))
            ++ " type=attachment", !S),
        ( if   Desc = ""
          then true
          else str(" desc=" ++ quote_qm(Desc), !S)
        ),
        str("}", !S),
        str(DivEnd, !S)

    ;
        %-------------------------------------------------------------------------------------------
        % A link to a dangling attachment.
        Target = t_dangling_attachment(AttTikiPage, AttFilename),

        str("''Moin2tiki error: Can't link to non-existing attachment "
            ++ quote_qm(tikipage(AttTikiPage)) ++ "/"
            ++ quote_qm(filename(AttFilename)) ++ "''", !S)
    ).


%--------------------------------------------------------------------------------
% Embeded objects. We only embed images at the moment. For other things, we create a link instead.


% An ordinary embedding.
:- pred do_embed(
    parser.embed(target(tikipage))::in,
    ppstate::in, ppstate::out)
is det.

do_embed(Embed, !S) :-
    register_embed_parts(Embed, EmbedParts),
    make_embed(EmbedParts, no, !S).


% Generate the Tiki code for an embedding. This is for an ordinary embedding (image), when the
% second argument is "no": It's an embedding that serves as the link text of a link (a clickable
% image), when the second argument is "yes(LinkParts)", where "LinkParts" being the data about the
% link.
:- pred make_embed(
    embed_parts::in,            % The data about the embedding
    maybe(link_parts)::in,      % If the embedding acts as a link, then the data about the link
                                % (see above)
    ppstate::in, ppstate::out)
is det.

make_embed(EmbedParts, MaybeLinkParts, !S) :-
    EmbedParts = embed_parts(Target, Text, Align, Width, Height, Class),
    (
        if
            (
                % An external object is being embedded. Take the filename from the URL.
                Target = t_external(URL),
                Filename = filename(URL)
            ;
                % An attachment is being embedded.
                Target = t_attachment(_, Filename)
            )
        then
             % Check for supported image file name extension.
             IsImg = (pred(Ext::in) is semidet :-
                 string.append(_,
                               string.to_lower(Ext),
                               string.to_lower(filename(Filename)))),

             ( if list.any_true(IsImg, img_exts)
                  then % It's an image which is being embedded. Make an invocation of the Img Plugin.

                       % We need to work around a Mercury flaw. EmbedParts2 gets a different
                       % instantiation than EmbedParts. This is needed for the call to
                       % make_pluginimg. See the thread "inst question" in mercury-users of
                       % 2022-07-27 / 2022-07-28.
                       EmbedParts2 = embed_parts(Target, Text, Align, Width, Height, Class),

                       make_pluginimg(EmbedParts2, MaybeLinkParts, !S)

                  else % It's not an image which is embedded in the MoinMoin page. Tiki doesn't
                       % support this. So make a link instead of an embedding.
                       (
                           % It's an ordiary embedding
                           MaybeLinkParts = no,
                           LinkParts1 =
                               (((((((null_link_parts
                                   ^ lp_link_target := ep_embed_target(EmbedParts))
                                   ^ lp_link_subsection := no)
                                   ^ lp_link_text := left(ep_embed_text(EmbedParts)))
                                   ^ lp_target := no)
                                   ^ lp_do_get := no)
                                   ^ lp_class := ep_class(EmbedParts))
                                   ^ lp_title := no),
                           make_link(LinkParts1, !S)
                       ;
                           % It's an embedding which serves as a link's link text (clickable image).
                           % Make an ordinary link instead.
                           MaybeLinkParts = yes(LinkParts),
                           make_link(LinkParts ^ lp_link_text := left(ep_embed_text(EmbedParts)), !S)
                       )
             )
        else
            (   % Error: A wiki page can't be embedded
                Target = t_wikipage(TikiPage),
                Error = "''Moin2tiki error: Can't embed a wiki page " ++ quote_qm(tikipage(TikiPage)) ++ "''"
            ;
                % Error: A dangling wiki page can't be embedded
                Target = t_dangling_wikipage(TikiPage),
                Error = "''Moin2tiki error: Can't embed non-existing wiki page " ++
                quote_qm(tikipage(TikiPage)) ++ "''"
            ;
                % Error: A non-existing attachment is being embedded.
                Target = t_dangling_attachment(TikiPage, Filename),
                Error = "''Moin2tiki error: Can't embed non-existing attachment " ++
                        quote_qm(tikipage(TikiPage)) ++
                        "/" ++ quote(filename(Filename)) ++ "''"
            ;
                % dealt with above
                Target = t_external(_),
                unexpected($pred, "Target=" ++ string.string(Target))
            ;
                % dealt with above
                Target = t_attachment(_, _),
                unexpected($pred, "Target=" ++ string.string(Target))
            ),
            str(Error, !S)
    ).



% Generate an invocation of the Tiki Img Plugin, for embedding an image. The second argument is only
% for the case when the image is to serve as a link. That is, when the MoinMoin equivalent has the form
% [[link target|{{embed target|embed text|embed parameters}}|link parameters]].
:- pred make_pluginimg(
    embed_parts::in(ep_extatt),    % The embed parts, of the embedded object, which serves as a link text.
    maybe(link_parts)::in,          % The link parts, as returend by register_link_parts/2
    ppstate::in, ppstate::out)
is det.

make_pluginimg(EmbedParts, MaybeLinkParts, !S) :-
    Target = ep_embed_target(EmbedParts),

    (   % An external object is being embedded. Take the filename from the URL.
        Target = t_external(URL),
        str("{img src=" ++ quote_qm(URL), !S),
        img_target(MaybeLinkParts, !S),
        img_parameters(EmbedParts, !S),
        str("}", !S)

    ;   % An attachment is being embedded.
        Target = t_attachment(AttTikiPage, AttFilename),
        str("{img attId=""", !S),
        attachment_placeholder(AttTikiPage, AttFilename, !S),
        str("""", !S),
        img_target(MaybeLinkParts, !S),
        img_parameters(EmbedParts, !S),
        str("}", !S)
    ).





% Insert a paceholder for a Tiki attachment ID. This is later detected by the MoinMoin-Tiki-bridge
% (moin-import.php) and replaced with the attachment-ID of a (new or existing) attachment. See
% "Attachments" in the design documentation.
%
% The attachment-ID-placeholde has the form "////TikiPage/Filename////".
:- pred attachment_placeholder(
    tikipage::in,
    filename::in,
    pretty.ppstate::in, pretty.ppstate::out       % Pretty printer state
) is det.

attachment_placeholder(TikiPage, Filename, !S) :-
    str("////" ++ tikipage(TikiPage) ++ "/" ++ filename(Filename) ++ "////", !S).




% If an image serves as a link, add the "link" parameter to the invocation of the Tiki Img Plugin.
% This makes the image clickable.

:- pred img_target(
    maybe(link_parts)::in,      % The parts of the link which the image is to link to. "no" when not
                                % clickable.
    ppstate::in, ppstate::out)
is det.

img_target(no, !S).

img_target(yes(LinkParts), !S) :-
    Target      = LinkParts ^ lp_link_target,
    MSubsection = LinkParts ^ lp_link_subsection,
    M2T         = ppstate_m2t(!.S),
    ParsedPages = !.S ^ ppstate_parsed_pages,
    (
        %--------------------------------------------------------------------------------------
        % Link to a (straight or dangling) wiki page.
        ( Target = t_wikipage(TikiPage) ; Target = t_dangling_wikipage(TikiPage) ),

        Link = "/" ++ M2T ^ m2t_params ^ p_tikidir ++ "/" ++ tikipage(TikiPage),
        (   MSubsection = yes(Subsection),
            mutilate_subsection(ParsedPages, Target, Subsection, Subsection1),
            str(" link=" ++ quote_qm(Link ++ "#" ++ Subsection1), !S)
        ;   MSubsection = no,
            str(" link=" ++ quote_qm(Link), !S)
        )
    ;
        %--------------------------------------------------------------------------------------
        % Link to an external page
        Target = t_external(URL),
        (   MSubsection = yes(Subsection),
            Subs = "#" ++ Subsection
        ;   MSubsection = no,
            Subs = ""
        ),
        str(" link=" ++ quote_qm(URL ++ Subs), !S)
    ;
        %--------------------------------------------------------------------------------------
        % Link to an attachment. Attachment references have no subsection.
        Target = t_attachment(AttPage, AttFilename),
        str(" link=""tiki-download_wiki_attachment.php?attId=", !S),
        attachment_placeholder(AttPage, AttFilename, !S),
        str("""", !S)
    ;
        %--------------------------------------------------------------------------------------
        % Here we have an embedded object that is to link to a dangling attachment. We don't make a
        % link here, this means the object doesn't link anything.
        Target = t_dangling_attachment(AttPage, AttFilename),
        pretty.message(
            "An embedded object is to link to a dangling attachment. This can't be done\n" ++
            "in Tiki, I skip the link. The dangling attachment reference was: "
            ++ quote_qm(tikipage(AttPage)) ++ "/" ++ quote_qm(filename(AttFilename)),
            !S)
    ).


% Translate and add the embeddment parameters to an invocation of the Tiki Img Plugin. This adds
% "width", "height", "align" and "class", when present.
:- pred img_parameters(
    embed_parts::in,
    ppstate::in, ppstate::out)
is det.

img_parameters(EmbedParts, !S) :-
    (
        EmbedParts ^ ep_width = yes(Width),
        str(" width=" ++ quote_qm(Width), !S)
    ;
        EmbedParts ^ ep_width = no
    ),
    (
        EmbedParts ^ ep_height = yes(Height),
        str(" height=" ++ quote_qm(Height), !S)
    ;
        EmbedParts ^ ep_height = no
    ),

    (
        EmbedParts ^ ep_align = yes(align_left),
        str(" align=left", !S)
    ;
        EmbedParts ^ ep_align = yes(align_right),
        str(" align=right", !S)
    ;
        EmbedParts ^ ep_align = yes(align_top),
        str(" stylebox=""vertical-align:top;""", !S)
    ;
        EmbedParts ^ ep_align = yes(align_middle),
        str(" stylebox=""vertical-align:middle;""", !S)
    ;
        EmbedParts ^ ep_align = yes(align_bottom),
        str(" stylebox=""vertical-align:bottom;""", !S)
    ;
        EmbedParts ^ ep_align = no
    ),

    (
        EmbedParts ^ ep_class = yes(Class),
        str(" class=" ++ quote_qm(Class), !S)
    ;
        EmbedParts ^ ep_class = no
    ),

    (
        if   EmbedParts ^ ep_embed_text = ""
        then true
        else str(" title=" ++ quote_qm(EmbedParts ^ ep_embed_text), !S)
    ).




%--------------------------------------------------------------------------------
% Making sense of the link and embedding parameters. This further parses the raw parameters, like
% they are returned by the main parser (see parser.m). Also include the other link/embed parts - the
% target and text (description). Store everything in a data structure for easy retrieval.


% The parsed link parts and parameters.
:- type link_parts ---> link_parts(
    lp_link_target      :: target(tikipage),            % the link target
    lp_link_subsection  :: maybe(string),               % the link subsection, if any
    lp_link_text        :: either(string, embed_parts), % the link text (for a normal link) or
                                                        % embedding information (for clickable images)
    lp_target           :: bool,                        % if the link has a "target=_blank" parameter
    lp_do_get           :: bool,                        % if the link has a "&dd=get" parameter
    lp_class            :: maybe(string),               % the class(es), when the link has one
    lp_title            :: maybe(string)).              % the tool tip, when the link has one

% The parsed embedding parts and parameters
:- type embed_parts ---> embed_parts(
    ep_embed_target     :: target(tikipage),            % the embed target (what is being embedded)
    ep_embed_text       :: string,                      % the embed title (displayed when the mouse
                                                        % hovers over the embedded object (picture)
    ep_align            :: maybe(align),                % the alignment, when the embedded object
                                                        % has one (that's the "align" parameter)
    ep_width            :: maybe(string),               % the width of the embedded object/image, if specified
    ep_height           :: maybe(string),               % the height of the embedded object/image, if specified
    ep_class            :: maybe(string)).              % the class(es), when the link has one

% types of alignment, which MoinMoin understands (see https://moinmo.in/HelpOnImages)
:- type align --->
      align_left ; align_right ; align_top ; align_middle ; align_bottom.


% Inst for the input of insert_description/3. This allows calls only with link_parts that have a
% wikipage (dangling or straight) as the target, and a regular text link (rather than an embedding).
:- inst lp_wikipage_textlink --->
        link_parts(
            wikipage,                   % the link target
            ground,                     % the link subsection, if any
            left(ground),               % the link text (for a normal link) or
            ground,                     % embedding information (for clickable images)
            ground,                     % if the link has a "target=_blank" parameter
            ground,                     % if the link has a "&dd=get" parameter
            ground).                    % the class(es), when the link has one

% Inst for the input of make_link/3. This allows calls only with link_parts that have a regular text
% link (rather than an embedding).
:- inst lp_textlink --->
        link_parts(
            ground,                     % the link target
            ground,                     % the link subsection, if any
            left(ground),               % the link text (for a normal link) or
            ground,                     % embedding information (for clickable images)
            ground,                     % if the link has a "target=_blank" parameter
            ground,                     % if the link has a "&dd=get" parameter
            ground).                    % the class(es), when the link has one

% Inst for the input of make_pluginimg/4. This allows calls only with link_parts that have a regular text
% link (rather than an embedding).
:- inst ep_extatt --->
        embed_parts(
            extatt,                      % the embed target (what is being embedded)
            ground,                      % the embed title (displayed when the mouse
                                         % hovers over the embedded object (picture)
            ground,                      % the alignment, when the embedded object
                                         % has one (that's the "align" parameter)
            ground,                      % the width of the embedded object/image, if specified
            ground,                      % the height of the embedded object/image, if specified
            ground).                     % the class(es), when the link has one

% Inst for the input of mutilate_subsection/4. This allows only internal targets, dangling or not.
:- inst targ_internallink --->
        t_wikipage(ground)
    ;   t_dangling_wikipage(ground).


% Empty values of the link and embed parts. This is just a starting record, with nothing recorded yet.
:- func null_link_parts  = link_parts.
:- func null_embed_parts = embed_parts.

null_link_parts  = link_parts(
    t_wikipage(tikipage("")),   % will always be replaced
    no, left(""), no, no, no, no).

null_embed_parts = embed_parts(
    t_wikipage(tikipage("")),   % will always be replaced
    "", no, no, no, no).


% Take a link data structure (which the parser returns) and record the information (link target,
% link text and link arguments) in a link_parts data structure.
:- pred register_link_parts(
    parser.link(target(tikipage))::in,   % The link as provided by the parser.
    link_parts::out)
is det.

register_link_parts(parser.link(Target, MSubsection, MDesc, Arguments),
                    LinkPartsRes) :-

    LinkParts1 = 'lp_link_target :='(null_link_parts, Target),
    LinkParts2 = 'lp_link_subsection :='(LinkParts1, MSubsection),
    (
        MDesc = yes(left(Desc)),
        LinkParts3 = 'lp_link_text :='(LinkParts2, left(Desc))
    ;
        MDesc = yes(right(Embed)),
        register_embed_parts(Embed, EmbedParts),
        LinkParts3 = 'lp_link_text :='(LinkParts2, right(EmbedParts))
    ;
        MDesc = no,
        LinkParts3 = 'lp_link_text :='(LinkParts2, left(""))
    ),
    foldl(register_one_link_part, Arguments, LinkParts3, LinkPartsRes).

% Used only by register_link_parts.
:- pred register_one_link_part(pair(string, string)::in, link_parts::in, link_parts::out) is det.

register_one_link_part(ArgName - ArgVal, PartsIn, PartsOut) :-
    (if
        (   ArgName = "target",
            ArgVal = "_blank",
            PartsOut0 = 'lp_target :='(PartsIn, yes)
        ;
            ArgName = "&do",
            ArgVal = "get",
            PartsOut0 = 'lp_do_get :='(PartsIn, yes)
        ;
            ArgName = "class",
            PartsOut0 = 'lp_class :='(PartsIn, yes(ArgVal))
        ;
            ArgName = "title",
            PartsOut0 = 'lp_title :='(PartsIn, yes(ArgVal))
        )
   then
       PartsOut = PartsOut0
   else
       PartsOut = PartsIn
   ).



% Take an embed data structure (which the parser returns) and record the information (embed target,
% embed text and embed arguments) in a embed_parts data structure.
:- pred register_embed_parts(
    parser.embed(target(tikipage))::in,       % The embedding as provided by the parser.
    embed_parts::out)
is det.

register_embed_parts(parser.embed(Target, MDesc, Arguments),
                     EmbedPartsRes) :-

    EmbedParts1 = 'ep_embed_target :='(null_embed_parts, Target),
    (
        MDesc = yes(Desc),
        EmbedParts2 = 'ep_embed_text :='(EmbedParts1, Desc)
    ;
        MDesc = no,
        EmbedParts2 = 'ep_embed_text :='(EmbedParts1, "")
    ),
    foldl(register_one_embed_part, Arguments, EmbedParts2, EmbedPartsRes).



% Used only by register_embed_parts
:- pred register_one_embed_part(pair(string, string)::in, embed_parts::in, embed_parts::out) is det.

register_one_embed_part(ArgName - ArgVal, PartsIn, PartsOut) :-
    (if
        (   ArgName = "align",
            ArgVal = "top",
            PartsOut0 = 'ep_align :='(PartsIn, yes(align_top))
        ;
            ArgName = "align",
            ArgVal = "middle",
            PartsOut0 = 'ep_align :='(PartsIn, yes(align_middle))
        ;
            ArgName = "align",
            ArgVal = "bottom",
            PartsOut0 = 'ep_align :='(PartsIn, yes(align_bottom))
        ;
            ArgName = "align",
            ArgVal = "right",
            PartsOut0 = 'ep_align :='(PartsIn, yes(align_right))
        ;
            ArgName = "align",
            ArgVal = "left",
            PartsOut0 = 'ep_align :='(PartsIn, yes(align_left))
        ;
            % The "center" alignment is a synonym for "middle". Strictly speaking, it's not valid
            % HTML (but browsers support it). Change this to "middle".
            ArgName = "align",
            ArgVal = "center",
            PartsOut0 = 'ep_align :='(PartsIn, yes(align_middle))
        ;
            ArgName = "width",
            PartsOut0 = 'ep_width :='(PartsIn, yes(ArgVal))
        ;
            ArgName = "height",
            PartsOut0 = 'ep_height :='(PartsIn, yes(ArgVal))
        ;
            ArgName = "class",
            PartsOut0 = 'ep_class :='(PartsIn, yes(ArgVal))
        )
   then
       PartsOut = PartsOut0
   else
       PartsOut = PartsIn
   ).



% Mutilate an anchor name, either the way the ANAME Tiki plugin does, or the way the generation of anchors from
% headings does, in Tiki. For this, we need to know if the subsection refers to an explicit anchor (made with
% ANAME) or an anchor automatically made from a heading.
%
% See the "Anchors in Tiki" design document.

:- pred mutilate_subsection(
    % The list of all parsed pages. That's the list of parse results of all SELECTED pages.
    assoc_list(tikipage, parser.page(parser.target(tikipage)))::in,

    % The link target.
    target(tikipage)::in(targ_internallink),

    % The subsection part
    string::in,

    % The Tiki-mutilated subsection part
    string::out)
is det.

mutilate_subsection(ParsedPages, Target, Subsection, Mutilated) :-
    % We can determine the right encoding only for a straight, internal link.
    ( Target = t_wikipage(TikiPage)
    ; Target = t_dangling_wikipage(TikiPage)
    ),
    (
        if  assoc_list.search(ParsedPages, TikiPage, Page),
            explicit_anchor_included(Subsection, Page)
        then
            mutilate_subsection_aname(Subsection, Mutilated)
        else
            mutilate_subsection_heading(Subsection, Mutilated)
    ).


% Mutilate anchor names the way the ANAME Tiki plugin does. Also see mutilate_subsection_heading/2.

% This replaces all sequences of characters in the input string, which aren't A-Z, a-z or 0-9, with one '_'. That's
% the way Tiki treats explicit anchors. So we need to do the same. The full details are outlined in the
% "Anchors in Tiki" design document.

:- pred mutilate_subsection_aname(
    string::in,         % subsection (without '#' at the beginning)
    string::out)        % mutilated subsection
is det.

mutilate_subsection_aname(In, Out) :-
    mutilate_subsection_aname_cl(string.to_char_list(In), Out1),
    Out = string.from_char_list(Out1).


% Only for mutilate_subsection.
:- pred mutilate_subsection_aname_cl(
    list(char)::in,         % subsection (without '#' at the beginning)
    list(char)::out)        % mutilated subsection
is det.

mutilate_subsection_aname_cl([], []).

mutilate_subsection_aname_cl(In@[_|_], Out) :-
    take_while(char.is_alnum, In, Good, Rest1),
    take_while_not(char.is_alnum, Rest1, Bad, Rest2),
    Out = Good ++ (if Bad = [] then [] else ['_']) ++ Out1,
    mutilate_subsection_aname_cl(Rest2, Out1).



% Mutilate anchor names the way the Tiki anchor generation from headings does. Also see
% mutilate_subsection_aname/2.

% This replaces all spaces in the anchor name with underscores, and removes all non-ASCII alphanumeric characters.
% That's the way Tiki treats anchors generated from headings. So we need to do the same. The full details are
% outlined in the "Anchors in Tiki" design document.

% THERE'S A MINOR BUG: All *sequences* of spaces must be replaced with a single underscore - not each space.

:- pred mutilate_subsection_heading(
    string::in,         % subsection (without '#' at the beginning)
    string::out)        % mutilated subsection
is det.

mutilate_subsection_heading(In, Out) :-
    string.replace_all(In, " ", "_", In1),
    mutilate_subsection_heading_cl(string.to_char_list(In1), Out1),
    Out = string.from_char_list(Out1).


% Only for mutilate_subsection.
:- pred mutilate_subsection_heading_cl(
    list(char)::in,         % subsection (without '#' at the beginning)
    list(char)::out)        % mutilated subsection
is det.

mutilate_subsection_heading_cl([], []).

mutilate_subsection_heading_cl(In@[_|_], Out) :-
    take_while(alnum_underscore, In, Good, Rest1),
    take_while_not(alnum_underscore, Rest1, _Bad, Rest2),
    Out = Good ++ Out1,
    mutilate_subsection_heading_cl(Rest2, Out1).


:- pred alnum_underscore(char::in) is semidet.

alnum_underscore(Chr) :-
    char.is_alnum(Chr) ; Chr = '_'.



% Insert the link description and replace a leading "#" with "~035~" if necessary.

:- pred insert_description(
    link_parts::in(lp_wikipage_textlink),
    ppstate::in, ppstate::out)
is det.

insert_description(LinkParts, !S) :-
    M2T = ppstate_m2t(!.S),


    lp_link_text(LinkParts) = left(Desc),

    (
        % When the link has a subsection, we don't need to quote a leading "#" in the description.
        lp_link_subsection(LinkParts) = yes(Subsection),
        ( if   Desc = ""
          then Target = lp_link_target(LinkParts),
               ( Target = t_wikipage(TikiPage)
               ; Target = t_dangling_wikipage(TikiPage)
               ),
               Desc1 = tikipage(TikiPage) ++ "#" ++ Subsection
          else Desc1 = Desc
        ),
        Quoted = tikitext_quote_text(M2T, c_link_text, no, string.to_char_list(Desc1)),
        str("|" ++ string.from_char_list(Quoted), !S)
    ;
        % When the link doesn't have a subsection, we need to quote a leading "#" in the
        % description, so it won't mistaken as the subsection.
        lp_link_subsection(LinkParts) = no,

        InChrs = string.to_char_list(Desc),
        (
            InChrs = [],
            Out = []
        ;
            InChrs = [Chr|Rest],
            ( if   Chr = '#'
              then Out0 = ['~','0','3','5','~'|Rest]
              else Out0 = InChrs
            ),
            Out = tikitext_quote_text(M2T, c_link_text, no, Out0)
        ),
        str("|" ++ string.from_char_list(Out), !S)
    ).

