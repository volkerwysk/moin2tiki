%================================================================================
% Translation of MoinMoin-Macros to Tiki
%
% MoinMoin macros are different to Tiki plugins in that they don't have a block of text on which
% they operate. There is syntax for only one tag, "<<...>>", and no closing tag.
%
% See http://moinmo.in/HelpOnMacros
%================================================================================

:- module macros.
:- interface.
:- import_module pretty, parser, moin2tiki.


% Translate a MoinMoin macro to Tiki and write it to the pretty printer state.

:- pred call_macro_pred(
    m2t::in,                            % Static data
    parser.macro::in,                   % Macro to be translated
    ppstate::in, ppstate::out)          % Pretty printer state
is det.


% Pretty printer for generic macros (used for error messages)

:- func write_macro_invoc(parser.macro) = string.


% An inst of the contexts in which macros can occur.

:- inst macro_context
   ---> c_list_item
   ;    c_text
   ;    c_table_cell
   ;    c_def_list_item.


%--------------------------------------------------------------------------------
% Implementation
%--------------------------------------------------------------------------------

:- implementation.
:- import_module assoc_list, string, pair, list, bool, require, exception, char, maybe.
:- import_module attachments, serialiser, tools, pagenames.


:- func macro_table =
    assoc_list(
        string,         % macro name in lower case
        macro_pred).    % packaged macro translation predicate


% MoinMoin macro names are case sensitive.

macro_table = [
    "BR"                  - macro_pred(macro_br),
    "Anchor"              - macro_pred(macro_anchor),
    "TableOfContents"     - macro_pred(macro_toc),
    "ListPages"           - macro_pred(macro_listpages)
    ].




:- type macro_pred --->
    macro_pred(
        (pred(
            moin2tiki.m2t::in,
            parser.macro::in,
            pretty.context::in(macro_context),
            ppstate::in, ppstate::out) is det)
    ).


call_macro_pred(M2T, Macro@parser.macro(Name, _Args), !S) :-
    (
        if   assoc_list.search(macro_table, Name, macro_pred(Pred))
        then check_for_macro_context(!.S ^ ppstate_context, Ctx),
             Pred(M2T, Macro, Ctx, !S)
        else pretty.message("Unknown macro " ++ write_macro_invoc(Macro) ++ ", skipping.", !S),
             write_unsupported_macro_call(Macro, !S)
    ).




% For MoinMoin macros which aren't suppored by Moin2Tiki, write the MoinMoin macro call instead.

:- pred write_unsupported_macro_call(
    parser.macro::in,                   % Macro to be translated
    ppstate::in, ppstate::out)          % Pretty printer state
is det.

write_unsupported_macro_call(parser.macro(Name, Args), !S) :-
    (
        Args = no,
        text("<<" ++ Name ++ ">>", !S)
    ;
        Args = yes(left(Str)),
        text("<<" ++ Name ++ "(" ++ quote_qm(Str) ++ ")>>", !S)
    ;
        Args = yes(right(Args1)),
        text("<<" ++ Name ++ "(" ++
            string.join_list(", ",
                map((func(Nam - Val) = Nam ++ "=" ++ quote_qm(Val)),
                    Args1))
             ++ ")>>", !S)
    ).



%--------------------------------------------------------------------------------
% Pretty printer for generic macros (used for error messages)

write_macro_invoc(macro(Name, no)) =
    "<<" ++ Name ++ ">>".

write_macro_invoc(macro(Name, yes(left(Str)))) =
    "<<" ++ Name ++ "(" ++ quote_qm(Str) ++ ")>>".

write_macro_invoc(macro(Name, yes(right(Args)))) =
    "<<" ++ Name ++ "(" ++
    string.join_list(
        ", ",
        map((func(Key - Val) = Key ++ "=" ++ quote_qm(Val)),
            Args))
    ++ ")>>".


%--------------------------------------------------------------------------------
% Checks for the macro arguments and context


% Check for macros that expect one string as argument.

:- pred check_args_str(
    parser.macro::in,           % Macro to be checked
    string::in,                 % Descriptive text about what is expected
    maybe(string)::out,         % String found, iff the call is like expected
    ppstate::in, ppstate::out)
is det.

check_args_str(Macro@macro(_, Args), Expect, MStr, !S) :-
    (
        Args = no,
        pretty.message(
            "Found invocation of macro """ ++ write_macro_invoc(Macro)
            ++ """, expecting " ++ Expect ++ ", skipping", !S),
        MStr = no
    ;
        Args = yes(left(Str)),
        MStr = yes(Str)
    ;
        Args = yes(right(_)),
        pretty.message(
            "Found invocation of macro """ ++ write_macro_invoc(Macro)
            ++ """ with name-value argument(s). Expecting " ++ Expect
            ++ ", skipping.", !S),
        MStr = no
    ).



% Check for macros that expect an arguments list.

:- pred check_args_args(
    parser.macro::in,                   % Macro to be checked
    maybe(parser.arguments)::out,       % Arguments list found, iff present
    ppstate::in, ppstate::out)
is det.

check_args_args(Macro@macro(_, Args), MArgs, !S) :-
    (
        Args = no,
        pretty.message(
            "Found invocation of macro """ ++ write_macro_invoc(Macro)
            ++ """, expecting arguments list. Skipping.", !S),
        MArgs = no
    ;
        Args = yes(right(MArgs0)),
        MArgs = yes(MArgs0)
    ;
        Args = yes(left(_)),
        pretty.message(
            "Found invocation of macro """ ++ write_macro_invoc(Macro)
            ++ """ with string as the argument. Expecting arguments list. Skipping.", !S),
        MArgs = no
    ).



% Check for macros that expect an optional string as argument. Return yes("") when it was invoced
% with no argument.

:- pred check_args_opt_str(
    parser.macro::in,           % Macro to be checked
    string::in,                 % Descriptive text about what is expected
    maybe(string)::out,         % String found, iff the call is like expected
    ppstate::in, ppstate::out)
is det.

check_args_opt_str(Macro@macro(_, Args), Expect, MStr, !S) :-
    (
        Args = no,
        MStr = yes("")
    ;
        Args = yes(left(Str)),
        MStr = yes(Str)
    ;
        Args = yes(right(_)),
        pretty.message(
            "Found invocation of macro """ ++ write_macro_invoc(Macro)
            ++ """ with name-value argument(s). Expecting " ++ Expect
            ++ ", skipping.", !S),
        MStr = no
    ).



% Check for macros that don't expect any argument.

:- pred check_args_none(
    parser.macro::in,           % Macro to be checked
    bool::out,                  % If the macro invocation has no arguments
    ppstate::in, ppstate::out)
is det.

check_args_none(Macro@macro(_, Args), None, !S) :-
    (
        Args = no,
        None = yes
    ;
        Args = yes(_),
        pretty.message(
            "Found invocation of macro """ ++ write_macro_invoc(Macro)
            ++ """ with argument(s), but expecting none. Skipping.", !S),
        None = no
    ).


% Check that the macro is called in a context which allows macros. This should be ensured by the
% parser. It's a bug when a macro occurs in the wrong context.

:- pred check_for_macro_context(
    pretty.context::in,
    pretty.context::out(macro_context))
is det.

check_for_macro_context(Ctx, Ctx1) :-
    (
       ( Ctx = c_list_item
       ; Ctx = c_text
       ; Ctx = c_table_cell
       ; Ctx = c_def_list_item
       ),
       Ctx1 = Ctx
   ;
       ( Ctx = c_heading
       ; Ctx = c_code_block
       ; Ctx = c_comment_block
       ; Ctx = c_link_text
       ; Ctx = c_external_link
       ),
       unexpected($pred, "Bug: Macro occurs in context " ++ string.string(Ctx)
                  ++ ", which allows no macros.")
   ).


%================================================================================
% Implementation of the macro translation predicates
%================================================================================

:- pred macro_br(        moin2tiki.m2t::in, parser.macro::in, pretty.context::in(macro_context),
                         ppstate::in, ppstate::out) is det.
:- pred macro_anchor(    moin2tiki.m2t::in, parser.macro::in, pretty.context::in(macro_context),
                         ppstate::in, ppstate::out) is det.
:- pred macro_toc(       moin2tiki.m2t::in, parser.macro::in, pretty.context::in(macro_context),
                         ppstate::in, ppstate::out) is det.
:- pred macro_listpages( moin2tiki.m2t::in, parser.macro::in, pretty.context::in(macro_context),
                         ppstate::in, ppstate::out) is det.


% The "<<br>>" macro.

macro_br(_M2T, Macro, Ctx, !S) :-
    check_args_none(Macro, None, !S),
    (
        if   None = yes
        then ( ( Ctx = c_list_item ; Ctx = c_table_cell ; Ctx = c_def_list_item ),
                 str(" %%% ", !S)
             ;
                 Ctx = c_text,
                 nl(!S)
             )
         else
             true
     ).


% The "<<anchor(Anchorname)>>" macro. Anchors in Tiki: see https://doc.tiki.org/PluginAname
% https://moinmo.in/HelpOnLinking#Anchors

macro_anchor(_M2T, Macro, _Ctx, !S) :-
    check_args_str(Macro, "anchor name", MStr, !S),
    (
        MStr = yes(Str),
        str("{ANAME()}", !S),
        str(Str, !S),
        str("{ANAME}", !S)
    ;
        MStr = no
    ).


% The "<<TableOfContents(...)>>" macro.
% https://doc.tiki.org/PluginMaketoc

macro_toc(_M2T, Macro, _Ctx, !S) :-
    check_args_opt_str(Macro, "one optional string argument", MStr, !S),
    (   MStr = yes(Str),
        str("{autotoc activity=yes align=page", !S),
        ( if   string.base_string_to_int(10, Str, Maxlevel)
          then str(" levels=""" ++ string.join_list(":", map(string.string, 1..Maxlevel)) ++ """", !S)
          else true
        ),
        str("}", !S)
    ;   MStr = no
    ).


% See https://doc.tiki.org/PluginListpages

macro_listpages(M2T, Macro, _Ctx, !S) :-
    check_args_args(Macro, MArgs, !S),
    (
        MArgs = no
    ;
        MArgs = yes(Args),
        ( if   assoc_list.search(Args, "search_term", Regexp)

          then listpages_convert_regex(M2T, Regexp, Search),
               str("{listpages find=" ++ quote_qm(Search) ++
                   " display=name sort=pageName_asc showCheckbox=n showNameOnly=y}", !S)

          else pretty.message("Found invocation of macro """ ++ write_macro_invoc(Macro)
                              ++ """ without ""search_term"" argument. Skipping.",
                              !S)
        )
    ).


% Limited support to convert a regular expession, which is used for the MoinMoin
% "<<ListPages(...)>>" macro, to a search string, which is used by the Tiki "Showpages" plugin.

:- pred listpages_convert_regex(
    moin2tiki.m2t::in,       % Static data
    string::in,         % Regular expression
    string::out)        % Page name Substring
is det.

listpages_convert_regex(M2T, Str1, Search) :-
    string.replace_all(Str1, ".*", "", Str2),
    string.replace_all(Str2, "[^/]*", "", Str3),
    string.replace_all(Str3, "$", "", Str4),
    string.replace_all(Str4, "/", M2T ^ m2t_params ^ p_pathsep, Search).
