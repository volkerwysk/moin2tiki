% This module isn't needed any longer. I've commented out all references to it in the other modules.

%================================================================================
% I had used MoinMoin pages for something like a database, of books and articles. Each page
% constituted one record. It consisted of a heading and a definition list. The definition names were
% used for the field names, and the definition texts for the values. Now with Tiki, we have read
% embedded databases, called "trackers". The MoinMoin pages can be migrated to tracker records.
%
% This module deals with that. The MoinMoin pages need to be interpreted and converted to records in
% a CSV file, which can then be imported in Tiki, into a tracker.
%================================================================================

:- module trackers.

:- interface.
:- import_module assoc_list, list, string.
:- import_module parser, pages, moin2tiki, io.


% Convert a MoinMoin "record page" to a record consisting of names and values. The values are
% in MoinMoin syntax. This means that MoinMoin markup in the data can be used.

:- pred page_to_record(
    moin2tiki.m2t::in,                          % Collected data
    parser.page(target(tikipage))::in,          % The page
    assoc_list(string, string)::out,            % The record, as an association list of item names
                                                % and item contents.
    list(string)::out)                          % Messages
is det.


% Take the collected records (without page names) and count the field names in them.

:- pred count_fields(
    list(assoc_list(string,string))::in,        % The records
    assoc_list(string,{int,int})::out)          % List of field names with number of occurrences and
                                                % non-empty occurrences
is det.



:- pred regalplaetze(
    assoc_list(tikipage, assoc_list(string,string))::in,
    io::di, io::uo)
is det.



% Parse the specified file as a tracker schema. 

:- pred schema(
    string::in,         % File name of the schema file.
    string::out,        % The schema in its CSV-file representation. This is made end with exactly
                        % one newline and no other white spaces.
    list(string)::out,  % The schema as a list of tracker field names.
    io::di, io::uo) is det.



%================================================================================
% Implementation
%================================================================================
:- implementation.
:- import_module require, pair, serialiser, maybe, bool, map, int, char, exception.
:- import_module pretty, tools, unicode, parsertools.


page_to_record(M2T, Paragraphs, Items, Messages) :-
    (
        if   Paragraphs = [comment_block(_) | Paras]
        then Paragraphs1 = Paras
        else Paragraphs1 = Paragraphs
    ),
    (
        if   Paragraphs1 = [heading(_,_) | Paragraphs2]
        then
             list.take_while(
                 (pred(def_list_item(_,_,_)::in) is semidet),
                 Paragraphs2,
                 Items1,
                 Rest),
             (
                 Items1 = [],
                 Items = [],
                 Messages = ["Error: No definition list directly after the heading found."]
             ;
                 Items1 = [_|_],
                 map_foldl(get_item(M2T), Items1, Items_orig, [], Mess),
                 (   Rest = [],
                     Messages = Mess
                 ;   Rest = [_|_],
                     append(Mess, ["Error: Found data after the end of the definition list."],
                            Messages)
                 ),

                 rectify_record(Items_orig, Items)
             )
        else
             Messages = ["Error: Expecting a heading at the beginning."],
             Items = []
    ).


% Convert one def_list_item to a Name-Text pair.

:- pred get_item(
    moin2tiki.m2t::in,                          % Collected data
    parser.paragraph(target(tikipage))::in,     % The def_list_item
    pair(string, string)::out,                  % The list "item name - list item contents" pair
    list(string)::in,                           % Messages in
    list(string)::out)                          % Messages out
is det.

get_item(M2T, Para, ItemName - ItemText, MessagesIn, MessagesOut) :-
    (
        if   Para = def_list_item(_, ItemName0, Inlines1)

        then ItemName = ItemName0,
             skip_unwanted(Inlines1, Inlines2),
             serialiser.inlines1(Inlines2, M2T, pretty.c_def_list_item, ItemText, Messages),
             append(MessagesIn, Messages, MessagesOut)

        else unexpected($pred, "def_list_item expected")
    ).


% This removes any "<<BR>>" macros from the beginning and the end of a list of inlines.
:- pred skip_unwanted(
    list(parser.inline(target(tikipage)))::in,
    list(parser.inline(target(tikipage)))::out)
is det.

skip_unwanted(Inlines1, Inlines4) :-
    take_while(is_unwanted, Inlines1, _, Inlines2),
    take_while(is_unwanted, list.reverse(Inlines2), _, Inlines3),
    Inlines4 = reverse(Inlines3).


:- pred is_unwanted(parser.inline(target(tikipage))::in) is semidet.
is_unwanted(macro(macro("BR", no))).
is_unwanted(space).




count_fields(Records, Fields) :-
    foldl(count_fields_1, Records, map.init, Map),
    map.to_assoc_list(Map, Fields).


:- pred count_fields_1(
    assoc_list(string, string)::in,

    % Map from field name to the number of occurences of the field and the number on non-empty
    % occurences.
    map(string, {int,int})::in,
    map(string, {int,int})::out)
is det.

count_fields_1(Record, MapIn, MapOut) :-
    foldl((pred((Name - Contents)::in, MapIn1::in, MapOut1::out) is det :-
              (
                  if   map.search(MapIn1, Name, {Occ,Nonempty})
                  then Occ1 = Occ + 1,
                       NE = Nonempty
                  else Occ1 = 1,
                       NE = 0
              ),
              (
                  if   Contents = ""
                  then NE1 = NE
                  else NE1 = NE + 1
              ),
              map.set(Name, {Occ1,NE1}, MapIn1, MapOut1)
          ),
          Record,
          MapIn,
          MapOut).


%--------------------------------------------------------------------------------


% :- pred regalplaetze(
%     assoc_list(tikipage, assoc_list(string,string))::in,
%     io::di, io::uo)
% is det.

regalplaetze(Records : assoc_list(tikipage, assoc_list(string, string)),
             !IO) :-
    io.write_string("\nRegalplätze:\n", !IO),

    foldl((pred((_ - Fields)::in,
                (RPin  : map(string, int))::in,
                (RPout : map(string, int))::out) is det :-
            ( if   assoc_list.search(Fields, "Regalplatz", Regalplatz)
              then tools.transform_add_value(
                        (pred(Anz0::in, Anz1::out) is det :- Anz1 = Anz0 + 1),
                        1,
                        Regalplatz,
                        RPin, RPout)
              else RPout = RPin
            )),
          Records,
          map.init,
          RegplMap),

    map.to_assoc_list(RegplMap, AL1),
    list.sort(compare_fst, AL1, AL2),

    foldl((pred((Reg - Anz)::in, IOin::di, IOout::uo) is det :-
              io.format("%s: %i\n", [s(Reg), i(Anz)], IOin, IOout)),
          AL2,
          !IO).


:- pred compare_fst(
    pair(string,T)::in,
    pair(string,T)::in,
    comparison_result::out)
is det.

compare_fst(A - _, B - _, Comp) :-
    glib_utf8_collate(A, B, Comp).



% Split a title in the form "Title; Subtitle" in title ("Title") and subtitle ("Subtitle"). If there
% isn't any "; " in the string, retur an empty subtitle.

:- pred titel_untertitel(string::in, string::out, string::out) is det.

titel_untertitel(S, Links, Rechts) :-
    (
        if   string.sub_string_search(S, "; ", St)
        then string.split(S, St, Links, _),
             string.split(S, St+2, _, Rechts)
        else Links = S,
             Rechts = ""
    ).



% Split a "Datum erhalten" field in "Erhalten" and "Quelle".

:- pred erhalten_quelle(string::in, string::out, string::out) is det.

erhalten_quelle(D, Erhalten, Quelle) :-
    (
        if   D = ""
        then Erhalten = "",
             Quelle = ""
        else e_q(E, Q, string.to_char_list(D), _),
             Erhalten = string.from_char_list(E),
             Quelle = string.from_char_list(Q)
    ).


% Parser for a "Datum erhalten" field.

:- pred e_q(list(char)::out, list(char)::out, list(char)::in, list(char)::out) is det.

e_q(Erhalten, Quelle) -->
    (
       if
           spaces0,
           digits(Day),
           [ '.' ],
           digits(Month),
           [ '.' ],
           digits(Year),
           { Erhalten0 = Year ++ ['-'] ++ pad(Month) ++ ['-'] ++ pad(Day) },
           spaces0,
           (
               if   ['(']
               then repeatmax(quelle_chr, Quelle0),
                    [')'],
                    end
               else { Quelle0 = [] }
           )
       then
           { Erhalten = Erhalten0,
             Quelle = Quelle0 }
       else
           { Erhalten = [] },
           repeatmax0(nonnl, Quelle)
    ).


:- func pad(list(char)) = list(char).

pad([])        = [].
pad([X])       = ['0',X].
pad([X,Y])     = [X,Y].
pad([X,Y,_|_]) = [X,Y].


:- pred quelle_chr(char::out, list(char)::in, list(char)::out) is semidet.

quelle_chr(Ch) -->
    [ Ch ],
    { Ch \= ')' }.


% Do the necessary transformations of a record for my record-MoinMoin pages

:- pred rectify_record(
    assoc_list(string, string)::in,     % Original record as found in the MoinMoin page
    assoc_list(string, string)::out)    % Rectified record
is det.

rectify_record(Items1, Res) :-
    rectify_record_1(Items1, Items2),

    % Deal with "Jahr"/"Erscheinungsjahr"
    (
        if   assoc_list.search(Items2, "Jahr", J),
             list.all_true(is_digit, string.to_char_list(J)),
             length(J) = 4
        then Jahr = yes(J)
        else Jahr = no
    ),
    (
        if   assoc_list.search(Items2, "Erscheinungsjahr", E),
             list.all_true(is_digit, string.to_char_list(E)),
             length(E) = 4
        then Erscheinungsjahr = yes(E)
        else Erscheinungsjahr = no
    ),
    (
        Jahr = no, Erscheinungsjahr = no,
        Res = Items2
    ;
        Jahr = no, Erscheinungsjahr = yes(E1),
        rem(Items2, "Erscheinungsjahr", Items3),
        Res = [ "Jahr" - E1 | Items3 ]
    ;
        Jahr = yes(Jahr1), Erscheinungsjahr = no,
        rem(Items2, "Jahr", Items3),
        Res = [ "Jahr" - Jahr1 | Items3 ]
    ;
        Jahr = yes(Jahr1), Erscheinungsjahr = yes(_),
        rem(Items2, "Jahr", Items3),
        rem(Items3, "Erscheinungsjahr", Items4),
        Res = [ "Jahr" - Jahr1 | Items4 ]
    ).


:- pred rem(assoc_list(K, V)::in, K::in, assoc_list(K, V)::out) is det.

rem(L0, K, L1) :-
    (   if   remove(L0, K, _, X)
        then L1 = X
        else unexpected($pred, "Removal from assoc_list failed")
    ).

        

:- pred rectify_record_1(
    assoc_list(string, string)::in,     % Original record as found in the MoinMoin page
    assoc_list(string, string)::out)    % Rectified record
is det.

rectify_record_1([], []).

rectify_record_1( [ ItemName - ItemVal | Rest], Items) :-
    (
        % Split title into title and subtitle
        if   ItemName = "Titel"
        then titel_untertitel(ItemVal, Titel, Untertitel),
             Items = [ "Titel" - Titel
                     , "Untertitel" - Untertitel
                     | Items1 ],
             rectify_record_1(Rest, Items1)

        % Split "Datum erhalten" to "Datum" and "Quelle"
        else if ItemName = "Datum erhalten"
        then erhalten_quelle(ItemVal, Erhalten, Quelle),
             Items = [ "Erhalten" - Erhalten
                     , "Quelle" - Quelle
                     | Items1 ],
             rectify_record_1(Rest, Items1)

        % Delete these fields: Datei, Webabzug
        else if ( ItemName = "Datei"
                ; ItemName = "Webabzug" 
                ; ItemName = "Regal", ItemVal = "?"
                ; ItemName = "Regal", ItemVal = "Belletristik (?)"
                )
        then rectify_record_1(Rest, Items)

        % Rename "Regal" to "Regalplatz"
        else if   ItemName = "Regal"
        then Items = [ "Regalplatz" - (if ItemVal = "Im Arbeitszimmer" then "im Arbeitszimmer" else ItemVal)
                     | Items1],
             rectify_record_1(Rest, Items1)

        % Rename "Ort" to "Orte"
        else if   ItemName = "Ort"
        then Items = [ "Orte" - ItemVal | Items1],
             rectify_record_1(Rest, Items1)

        % Enforce correct page number
        else if   ItemName = "Seite"
        then Items = [ "Seite" - ItemVal1 | Items1],
             take_while(is_digit, string.to_char_list(ItemVal), Digits, _),
             ItemVal1 = string.from_char_list(Digits),
             rectify_record_1(Rest, Items1)
            
        % No change
        else Items = [ ItemName - ItemVal | Items1],
             rectify_record_1(Rest, Items1)
    ).


schema(Filename, Txt, Schema, !IO) :-
    contents_ex(Filename, Txt0, !IO),
    ( if   schema(S, string.to_char_list(Txt0), [])
      then Schema = S,
           Txt = string.rstrip(Txt0) ++ "\n"
      else throw("Error: Can't parse tracker tabular schema file " ++ quote_qm(Filename))
    ).


% Parser for tracker schema, as exported by "Export full" of the Tracker Tabular Format

:- pred schema(list(string)::out, list(char)::in, list(char)::out) is semidet.

schema(Fields) -->
    repeatmax(schema_field, Fields),
    eol.



% Parser for field in a tracker schema

:- pred schema_field(string::out, list(char)::in, list(char)::out) is semidet.

schema_field(Fieldname) -->
    [ '"' ],
    repeatmax0(fieldname_char, Fieldnamechars),
    str(" ["),
    field_rest,
    { from_char_list(Fieldnamechars, Fieldname) }.



% One character in a field name.

:- pred fieldname_char(char::out, list(char)::in, list(char)::out) is semidet.

fieldname_char(Char) -->
    ( if str(" [") then { fail }
                   else [ Char ]
    ).



% The part in a field schema after the field name.

:- pred field_rest(list(char)::in, list(char)::out) is semidet.

field_rest -->
    repeatmax0(rest_char),
    str("]"""),
    maybe(comma).


% One character of the rest of the field schema

:- pred rest_char(list(char)::in, list(char)::out) is semidet.

rest_char -->
    [ Ch ],
    { Ch \= ']' }.


% Parser for a comma.

:- pred comma(list(char)::in, list(char)::out) is semidet.

comma --> [ (',') ].
