Links occur in two places in MoinMoin: in regular links in "[[...]]" and in links to embedded objects in "{{...}}".
The link target can be an external link (an URL), a wiki page or reference to an attachment (specified by
"attachment:..."). Links to a wikipage reference and an attachment can dangle.

Except for the link target, there is more information about a link: the subsection (the part after
"#", if present), the link description (after the first "|") and the link arguments (after the second
"|, if present). 

Regular links can have a subsection, a description and arguments. Links to embedded objects can have
a description and arguments. A subsection doesn't make sense for embedded links. The link
description can be an embeded object, such as an image. This way, for example, clickable images can
be generated.

A regular link target can be external, being an URL (when the link target begins with "https://" or
"http://"). External links can't dangle (for Moin2Tiki). It can also be a reference to a regular
wiki page or a reference to an attachment.

Wiki pages can't (directly) be embeded in one another. So an embeding link target is either external
or a reference to an attachment.

The parser creates the parsed representation of a MoinMoin page. The data type of this
representation ("parser.page/1") is paramerised over a link target type. The parser creates "raw"
link targets (so the type is "parser.page(raw)"). Those are replaced by MoinMoin link targets in
postprocessing (see postprocess.m).

In the "raw" state, the link targets aren't interpreted, except for the distinction between
attachment references and other references (wiki pages or external links). 

Raw link targets have:

- The actual target. 
- A flag which says if the target is an attachment.

The raw link targets are transformed to moin link targets in a transformation run (see
"transform.m"). The page is traversed and the targets are mapped to the new type. 

The type of the MoinMoin link targets is "target(P)". It is parameterised over the type P of
the page names. Those are defined like this:

```
:- type moinpage ---> moinpage(moinpage :: string).
:- type tikipage ---> tikipage(tikipage :: string).
```

They encapsulate a page name, so they are checked by the Mercury type checker.

The "cooked" link can be:

- wikipage link (internal link)
  - pagename
- extrenal link
  - URL
- attachment reference
  - pagename
  - filename
- dangling wikipage link
  - dangling pagename
- dangling attachment reference
  - pagename
  - filename

For a danglich attachment refernce, either the pagename or the filename can dangle.

