% Unicode support
%
% This module imports some needed Unicode related functions from the GLib library, because the
% Unicode support in Mercury is still incomplete (as of 2021-04-18).
%
% See https://developer.gnome.org/glib/stable/

:- module unicode.

:- interface.
:- import_module char, io.

:- pred glib_toupper(char::in, char::out) is det.
:- pred glib_tolower(char::in, char::out) is det.
:- pred glib_isalnum(char::in) is semidet.
:- pred glib_isupper(char::in) is semidet.
:- pred glib_islower(char::in) is semidet.
:- pred glib_isalpha(char::in) is semidet.

:- pred glib_utf8_collate(string::in, string::in, builtin.comparison_result::out) is det.
:- func glib_utf8_collate(string::in, string::in) = (builtin.comparison_result::out) is det.
:- pred setlocale(io::di, io::uo) is det.


:- implementation.
:- import_module bool, int.


:- pragma foreign_decl("C", "
#include <glib.h>
#include <locale.h>
").


:- pragma foreign_proc("C",
    glib_toupper(Ch::in, Upch::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Upch = g_unichar_toupper(Ch);
").



:- pragma foreign_proc("C",
    glib_tolower(Ch::in, Upch::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Upch = g_unichar_tolower(Ch);
").


:- pred glib_isalnum0(char::in, int::out) is det.

:- pragma foreign_proc("C",
    glib_isalnum0(Ch::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Res = g_unichar_isalnum(Ch);
").


glib_isalnum(Ch) :-
    glib_isalnum0(Ch, Is),
    ( if   Is = 0
      then fail
      else true
    ).



:- pred glib_isupper0(char::in, int::out) is det.

:- pragma foreign_proc("C",
    glib_isupper0(Ch::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Res = g_unichar_isupper(Ch);
").


glib_isupper(Ch) :-
    glib_isupper0(Ch, Is),
    ( if   Is = 0
      then fail
      else true
    ).



:- pred glib_islower0(char::in, int::out) is det.

:- pragma foreign_proc("C",
    glib_islower0(Ch::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Res = g_unichar_islower(Ch);
").


glib_islower(Ch) :-
    glib_islower0(Ch, Is),
    ( if   Is = 0
      then fail
      else true
    ).



:- pred glib_isalpha0(char::in, int::out) is det.

:- pragma foreign_proc("C",
    glib_isalpha0(Ch::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
  Res = g_unichar_isalpha(Ch);
").


glib_isalpha(Ch) :-
    glib_isalpha0(Ch, Is),
    ( if   Is = 0
      then fail
      else true
    ).



:- pred c_glib_utf8_collate(string::in, string::in, int::out) is det.

:- pragma foreign_proc("C",
    c_glib_utf8_collate(Str1::in, Str2::in, Res::out),
    [promise_pure, will_not_call_mercury, thread_safe],
"
    setlocale(LC_ALL, """");
    Res = g_utf8_collate(Str1, Str2);
").



glib_utf8_collate(Str1, Str2, Compres) :-
    c_glib_utf8_collate(Str1, Str2, Res),
    (
        if   Res < 0
        then Compres = (<)

        else if Res = 0
        then Compres = (=)

        else Compres = (>)
    ).


glib_utf8_collate(A, B) = Res :-
    glib_utf8_collate(A, B, Res).


:- pragma foreign_proc("C",
    setlocale(IO0::di, IO1::uo),
    [promise_pure, will_not_call_mercury, tabled_for_io],
"    
    setlocale(LC_ALL, """");
    IO1 = IO0;
").

% :- pragma foreign_proc("C",
%     setlocale(IO0::di, IO1::uo),
%     [promise_pure, will_not_call_mercury, tabled_for_io],
% "    
%     setlocale(LC_ALL, """");
% ").
