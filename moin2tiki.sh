#! /bin/bash

# This script assumes that everything (for instance the "moin2tiki" program) is in 
# the directory which the script is in.

PAGESDIR=/usr/local/lib/moin-1.9.11/wiki/data/pages
USER=v
LANGUAGE=de

if [ "$1" == "" ] ; then
    echo "Specify an action and select pages. Call \"./moin2tiki help\" for help."
    exit 1
fi

DIR="`dirname $0`"

sudo -u www-data "$DIR"/moin2tiki \
    "$@" \
    --pagesdir="$PAGESDIR" \
    --tmpdir="$DIR"/intermediate.tmp \
    --user="$USER" --language="$LANGUAGE" \
    --pathsep=:: \
    --pnrepl="$DIR"/pnrepl \
    || exit 1


#    --ignore-list-read="$DIR"/ignore-list \
#    --ignore-list-write="$DIR"/ignore-list \
